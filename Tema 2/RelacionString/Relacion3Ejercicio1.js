function invierteCadena(cad_arg)
{
	for (var i=cad_arg.length-1; i >= 0; i--)
	{
	  document.write(cad_arg[i]);
	};
	document.write("</br>");
}

function inviertePalabras(cad_arg)
{
  var cadena = cad_arg.split(" ");
  
  for (var i=cadena.length-1; i >= 0; i--)
  {
    document.write(cadena[i]+" ");
  };
  document.write("</br>");
}

function encuentraPalabraMasLarga(cad_arg)
{
  var c = 0;
  var cadena = cad_arg.split(" ");
  var mayor = cadena[c].length;
  var cad_mayor = Array();
  cad_mayor[c] = cadena[0];
  c++;
  
  for (var i=1; i < cadena.length; i++)
  {
    if(mayor < (cadena[i].length))
    {
    	mayor = cadena[i].length;
    	cad_mayor = cadena[i];
    }
    if(mayor == cadena[i].length)
    {
    	cad_mayor[c] = cadena[i];
    	c++;
    }
  }
  
  if(cad_mayor.length > 1)
  {
  	var string_final = "La palabras ";
  	for (var i=0; i < cad_mayor.length; i++) {
  		if(i == 0)
  		{
  			string_final = string_final +" "+cad_mayor[i];
  		}
  		else if(i == cad_mayor.length-1)
	  		 {
	  			string_final = string_final +" y "+cad_mayor[i];
	  		 }
	  		 else
	  		 {
	  			 string_final = string_final +", "+cad_mayor[i];
	  		 }
		
	  };
  	document.write(string_final+" son las mayores con una longitud de "+mayor+"</br>");
  }
  else
  {
  	document.write("La palabra "+cad_mayor+" es la mayor con una longitud de "+mayor+"</br>");
  }
}

function fltraPalabrasMasLargas(cad_arg, i)
{
	var cadena = cad_arg.split(" ");
	var c = 0;
	for (var x=0; x < cadena.length; x++) {
	  if(cadena[x].length >= i)
	  {
	  	c++;
	  }
	};
	document.write("Hay "+c+" palabras con "+i+" caracteres de longuitud o superior.</br>");
}

function cadenaBienFormada(cad_arg)
{
	var bien = cad_arg.substr(0,1).toUpperCase() + cad_arg.substr(1,cad_arg.length).toLowerCase();
	document.write(bien);
}
