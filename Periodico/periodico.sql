-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-03-2016 a las 15:23:42
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `periodico`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`u867906833_admin`@`localhost` PROCEDURE `sp_usuarios` ()  begin
			select * from usuario order by idusuario;
		end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE `comentario` (
  `idcomentario` int(11) NOT NULL,
  `idusuario` int(11) DEFAULT NULL,
  `idnoticia` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `texto` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `infonoticia`
--

CREATE TABLE `infonoticia` (
  `idnoticia` int(11) DEFAULT NULL,
  `idtiponoticia` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `pos` int(11) DEFAULT NULL,
  `neg` int(11) DEFAULT NULL,
  `activa` tinyint(1) DEFAULT NULL,
  `idautor` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticia`
--

CREATE TABLE `noticia` (
  `idnoticia` int(11) NOT NULL,
  `idioma` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto` text COLLATE utf8_unicode_ci,
  `imagen` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `noticia`
--

INSERT INTO `noticia` (`idnoticia`, `idioma`, `titulo`, `texto`, `imagen`) VALUES
(19, 'en', 'card 3', 'la tarjeta 3 mola mas', ''),
(18, 'en', 'cards 2', 'Hola soy la tarjeta 2', ''),
(16, 'en', 'cards', 'Content types Cards support a wide variety of content, including images, text, list groups, links, and more. Mix and match multiple content types to create the card you need.', ''),
(20, 'en', 'Hola', '<p><span class="marker">Hola</span> <s>tio </s>que<strong> pasa?</strong></p>\r\n', ''),
(21, 'en', '9yfoiqweifqw', '<h1>gklsdabfkj &ntilde; KLVNSD</h1>\r\n', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `palabra`
--

CREATE TABLE `palabra` (
  `idpalabra` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idioma` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `traduccion` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `palabra`
--

INSERT INTO `palabra` (`idpalabra`, `idioma`, `traduccion`) VALUES
('menu_lenguaje', 'es', 'Idioma'),
('menu_lenguaje', 'en', 'Language'),
('menu_lenguaje', 'fr', 'Langue'),
('titulo_iniciar', 'es', 'Iniciar Sesión'),
('titulo_iniciar', 'en', 'Login'),
('titulo_iniciar', 'fr', 'S''identifier'),
('menu_correo', 'es', 'Correo'),
('menu_correo', 'en', 'Email'),
('menu_correo', 'fr', 'Courrier'),
('menu_clave', 'es', 'Contraseña'),
('menu_clave', 'en', 'Password'),
('menu_clave', 'fr', 'Passe'),
('error_login', 'es', 'Error al iniciar (clave incorrecta o usuario bloqueado)'),
('error_login', 'en', 'Failed login ( incorrect password or user locked)'),
('error_login', 'fr', 'Impossible de démarrer ( mot de passe incorrect ou l''utilisateur verrouillés )'),
('log_out', 'en', 'Log out'),
('log_out', 'es', 'Cerrar sesion'),
('log_out', 'fr', 'Se déconnecter'),
('add_new', 'en', 'Add new'),
('add_new', 'es', 'Añadir noticia'),
('add_new', 'fr', 'Ajouter de nouvelles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiponoticia`
--

CREATE TABLE `tiponoticia` (
  `idtiponoticia` int(11) NOT NULL,
  `idpalabra` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activa` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nombre` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nick` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecharegistro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `clave` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `correo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipousuario` int(11) DEFAULT NULL,
  `idiomapref` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre`, `nick`, `fecharegistro`, `clave`, `activo`, `correo`, `tipousuario`, `idiomapref`) VALUES
(3, 'Aldo Guerrero', 'Aldo', '2015-11-24 19:28:15', '*C39DEFC5473C2FEDE90FF436C69465631E05CD42', 1, 'aldo@hotmail.com', 2, NULL),
(4, 'Carlos', 'carlitos', '2015-11-24 19:32:35', '*AF4534DF01F9A919AD1CF6D94DD5B0D9273DB325', 1, 'carlos@gmail.com', 2, NULL),
(7, 'iban jesus romera', 'snakemk', '2015-11-24 19:48:36', '*E857FDD16C6768D376D3CD585FAEB373205EF4A1', 1, 'snakemk91@gmail.com', 2, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`idcomentario`);

--
-- Indices de la tabla `noticia`
--
ALTER TABLE `noticia`
  ADD PRIMARY KEY (`idnoticia`);

--
-- Indices de la tabla `tiponoticia`
--
ALTER TABLE `tiponoticia`
  ADD PRIMARY KEY (`idtiponoticia`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comentario`
--
ALTER TABLE `comentario`
  MODIFY `idcomentario` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `noticia`
--
ALTER TABLE `noticia`
  MODIFY `idnoticia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `tiponoticia`
--
ALTER TABLE `tiponoticia`
  MODIFY `idtiponoticia` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
