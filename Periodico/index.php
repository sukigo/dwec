<?php

	require_once './clases/form.php';
	require_once './clases/BD.php';
	session_start();
	$bd=BD::getInstance();
	$arr_palabras = $bd->listarPalabras("en");
	$idioma = "en";
	if(isset($_POST) && $_POST != NULL)
	{
		if(isset($_POST['es']) || isset($_POST['login_es']))
		{
			$arr_palabras = $bd->listarPalabras("es");
			$idioma = "es";
		}
		elseif(isset($_POST['fr']) || isset($_POST['login_fr']))
		{
			$arr_palabras = $bd->listarPalabras("fr");
			$idioma = "fr";
		}
	}
	
	$lista=$bd->listarNoticias($idioma);
	if($lista != null)
	{
		$noticias = '<div class="row">';
		foreach ($lista as $key => $value) 
		{
				$noticias .= '
				<form method="POST">
		            <div class="col-md-4 card card-block" style="background:white;border­radius: 20px;text­align: justify;overflow: hidden;height:350px; width:400px;padding:15px;margin:25px;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
		            	<input type="text" name="pedido" value="'.$value['id'].'" style="display: none"/>
		                <h3 class="card-title">
		                   <a href="Producto.php?producto='.$value['id'].'">'.$value['titulo'].'</a>
		                </h3>
		                <p class="card-text">'.$value['texto'].'</p>
		            </div>
	            </form>';
		
		}
	    $noticias .= '</div>';
	}
	else
	{
		$noticias = "No hay noticias disponibles.";
	}
	    
			
		$arr_estructura = array(
			"div_nav" => array(
				"etiqueta" => "div",
				"id" => "div_nav",
				"class" => "navbar navbar-default navbar-fixed-top",
				"name" => "div_nav",
				"style" => "background: black;background-image: linear-gradient(to bottom, #222, #111);border-bottom-color: black; box-shadow: -2px 6px 5px #000;border-bottom-left-radius: 5px;border-bottom-right-radius: 5px;",
				"role" => "navigation",
				"elementos" => array(

					"div_header" => array(
						"etiqueta" => "div",
						"id" => "div_header",
						"class" => "navbar-header",
						"name" => "div_header",
						"style" => ""
					),
					"button" => array(
						"etiqueta" => "button",
						"id" => "button_desplegable",
						"class" => "navbar-toggle collapsed",
						"name" => "button_desplegable",
						"data-toggle" => "collapse",
						"data-target" => ".navbar-collapse",
						"type" => "button",
						"value" => "<span class='sr-only'>Toggle navegación</span><span class='icon-bar'></span><span class='icon-bar'></span><span class='icon-bar'></span>",
						"style" => ""
					),
					"Inicio" => array(
						"etiqueta" => "a",
						"id" => "titulo",
						"class" => "navbar-brand",
						"name" => "titulo",
						"href" => "#",
						"value" => "Inicio",
						"style" => "color: white;"
					),
					"/div1" => array(
						"etiqueta" => "/div"
					),
					"div_collapse" => array(
						"etiqueta" => "div",
						"id" => "div_collapse",
						"class" => "navbar-collapse collapse",
						"name" => "div_collapse",
						"style" => ""
					),
					"lista_menu" => array(
						"etiqueta" => "lista",
						"id" => "lista_menu",
						"class" => "nav navbar-nav",
						"role" => "",
						"name" => "lista_menu",
						"style" => "background-color:black;background-image: linear-gradient(to bottom, #222, #111);",
						"ordenada" => false,
						"elementos" => array(
							"add_noticia" => '<a href="javascript:void(0)" class="btn" style="color:white;">'.'Meter categorias'."</a>",
						)
					),
					"/div2" => array(
						"etiqueta" => "/div"
					),
					"/div3" => array(
						"etiqueta" => "/div"
					)
				)
			),
			"div3" => array(
				"etiqueta" => "div",
				"id" => "div",
				"class" => "btn-group",
				"name" => "div",
				"style" => "position:fixed; top:5px; right:5px; font-weight: bold; margin-top: auto;z-index: 9999;background-color:black;background-image: linear-gradient(to bottom, #222, #111);",
				"elementos" => array(
					"button_language" => array(
						"etiqueta" => "button",
						"id" => "language",
						"class" => "btn btn-primary",
						"name" => "languaje",
						"value" => $arr_palabras['menu_lenguaje'],
						"type" => "button",
						"style" => "background-color: black;cursor: not-allowed;"
					),
					"button_drop_down" => array(
						"etiqueta" => "button",
						"id" => "desplegable",
						"class" => "btn btn-primary dropdown-toggle",
						"data-toggle" => "dropdown",
						"name" => "desplegable",
						"value" => '<span class="caret"></span>',
						"type" => "button",
						"style" => "background-color: black;"
					),
					"lista_idiomas" => array(
						"etiqueta" => "lista",
						"id" => "div",
						"class" => "dropdown-menu",
						"role" => "menu",
						"name" => "div",
						"style" => "background-color:black;background-image: linear-gradient(to bottom, #222, #111);color:white;",
						"ordenada" => false,
						"elementos" => array(
							"en" => "<input style ='background-color:black;background-image: linear-gradient(to bottom, #222, #111);border:hidden;' type='submit' name='en' value='English'/>",
							"es" => "<input style ='background-color:black;background-image: linear-gradient(to bottom, #222, #111);border:hidden;' type='submit' name='es' value='Español'/>",
							"fr" => "<input style ='background-color:black;background-image: linear-gradient(to bottom, #222, #111);border:hidden;' type='submit' name='fr' value='Français'/>"
						)
					)
				)
			),
			"div_content" => array(
				"etiqueta" => "div",
				"id" => "div_content",
				"class" => "card",
				"name" => "div_content",
				"style" => "margin:50px; margin-top: 100px;padding: 15px;",
				"role" => "",
				"elementos" => array(
					"titulo_noticias" => array(
						"etiqueta" => "h2",
						"id" => "titulo_noticia",
						"class" => "card-title",
						"name" => "titulo_noticias",
						"href" => "javascript:void(0)",
						"value" => "Lista noticias",
						"style" => "text-align: center;"
					),
					"tabla_noticias" => array(
						"etiqueta" => "p",
						"id" => "noticias",
						"class" => "card-block",
						"name" => "noticias",
						"href" => "javascript:void(0)",
						"value" => $noticias,
						"style" => ""
					)
				)
			)
		);
	
	$form = new form("post","form1","form-signin");
	$form->estructura($arr_estructura);
		
?>

<!DOCTYPE html>
<html>
	<head>
	  	<title>Periodico</title>
	  	<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
	  	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	  	<link rel="stylesheet" href="css/active_nav.css">
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	  	<script src="js/active_nav.js"></script>
	</head>
	<body style="background-color: #3D72A4;">
		<?php 
			$form->mostrar(); 
		?>
	</body>
</html>