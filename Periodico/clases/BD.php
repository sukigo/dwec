<?php
Class BD extends mysqli{

   private $servidor='localhost';
   private $usuario='root';
   private $password='';
   private $base_datos='periodico';
   private $link;
   private $stmt;
   private $array;
   static $_instance;

   private function BD(){
      $this->conectar();
   }

   private function __clone(){ }

   public static function getInstance(){
      if (!(self::$_instance instanceof self)){
         self::$_instance=new self();
      }
      return self::$_instance;
   }

	
   private function conectar(){
      $this->link = mysqli_connect($this->servidor, $this->usuario, $this->password);
      mysqli_select_db($this->link,$this->base_datos);
      @mysqli_query("SET NAMES 'utf8'");
   }
   
    public function iniciaSesion($correo, $clave){
		$this->stmt=mysqli_query($this->link,"SELECT tipousuario, activo FROM usuario where correo='$correo' and clave=PASSWORD('$clave')");
		$contador=mysqli_num_rows($this->stmt);	
		if($contador != 0){	
			if($ro = mysqli_fetch_array($this->stmt)){ 
	  			if ($ro['activo']){
					if ($ro['tipousuario']==0){
						return 'usuario';
					}
					if ($ro['tipousuario']==1){
						return 'autor';
					}	
					if ($ro['tipousuario']==2){
						return 'administrador';
					}			
				}else{
					return false;
				}
			}
		}
   }
   public function insertarUsuario($nombre, $nick, $clave, $correo, $tipo){
		$this->stmt=mysqli_query($this->link,"SELECT * FROM usuario where nick='$nick'");  
		$cuenta= mysqli_num_rows($this->stmt);	
		if($cuenta != 0){
			return false;
		}else{ 
			mysqli_query ($this->link,"INSERT INTO usuario (nombre, nick, clave, activo, correo, tipousuario)VALUES('$nombre', '$nick', PASSWORD('$clave'), true, '$correo', $tipo);");
			return true;		
		}
  

   }
 
public function listarPalabras($idioma){
		$this->stmt=mysqli_query($this->link,"SELECT idpalabra, traduccion FROM palabra where idioma='$idioma'");
		$contador=mysqli_num_rows($this->stmt);	
		$arra = array();
		if($contador != 0){	
			while($ro = mysqli_fetch_array($this->stmt)){ 
				$cas=mysqli_num_rows($this->stmt);
				if ($cas !=0){
					for ($a=0; $a<$contador;$a++){								
						$arra[$ro['idpalabra']] = $ro['traduccion'];
					}
					
	  			}
			}
			return $arra;
		}
   }
   public function borrarUsuario($nick){
		$this->stmt=mysqli_query($this->link,"SELECT * FROM usuario where nick='$nick'");  
		if(mysqli_num_rows($this->stmt) != 0){
			mysqli_query ($this->link,"delete from usuario where nick='$nick'");
			return true;
			
		}else{
			return false;		
		}
   }
 
   public function listarUsuario(){
		$this->stmt=mysqli_query($this->link,"SELECT * FROM usuario");
		$contador=mysqli_num_rows($this->stmt);	
		$arra = array();
		$cuenta=0;
		if($contador != 0){	
			while($ro = mysqli_fetch_array($this->stmt)){ 
				$cas=mysqli_num_rows($this->stmt);
				if ($cas !=0){
					$arra[$cuenta] = array();
					$arra[$cuenta]['idusuario'] = $ro['idusuario'];
					$arra[$cuenta]['nombre'] = $ro['nombre'];
					$arra[$cuenta]['nick'] = $ro['nick'];
					$arra[$cuenta]['fechanac'] = $ro['fechanac'];
					$arra[$cuenta]['fecharegistro'] = $ro['fecharegistro'];
					$arra[$cuenta]['clave'] = $ro['clave'];
					$arra[$cuenta]['activo'] = $ro['activo'];
					$arra[$cuenta]['correo'] = $ro['correo'];
					$arra[$cuenta]['tipousuario'] = $ro['tipousuario'];
					$arra[$cuenta]['idiomapref'] = $ro['idiomapref'];
					$cuenta++;
	  			}
			}
			return $arra;
		}
   }
   
   public function listarNoticias($idioma)
   {
   		$this->stmt=mysqli_query($this->link,"select idnoticia, titulo, texto from noticia where idioma = '$idioma';");
		if($this->stmt)
		{
			$contador=mysqli_num_rows($this->stmt);
			$arra = array($contador);
			$cuenta=0;
			if($contador != 0){
				while($ro = mysqli_fetch_array($this->stmt)){
					$cas=mysqli_num_rows($this->stmt);
					if ($cas !=0){
						$arra[$cuenta]= array(
							"id" => $ro['idnoticia'],
							"titulo" => $ro['titulo'],
							"texto" => $ro['texto']
						);		
						$cuenta++;
		  			}
				}
				return $arra;
			}
		}
		else
		{
			return false;
		}
		
   }

	public function borrarNoticia($id)
	{
		$this->stmt = mysqli_prepare($this->link,"Delete from noticia where idnoticia = ?;");
		$this->stmt->bind_param("i", $id);
		if($this->stmt->execute())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function insertarNoticia($titulo, $texto, $idioma)
   	{
   		$this->stmt = mysqli_prepare($this->link,"Insert Into noticia (titulo,texto,idioma) values (?,?,?);");
		$this->stmt->bind_param("sss",$titulo, $texto, $idioma);
		if($this->stmt->execute())
		{
			return true;
		}
		else
		{
			return false;
		}
   	}
	
	public function modificarNoticia($titulo, $texto, $idioma, $id)
   	{
   		$this->stmt = mysqli_prepare($this->link,"Update noticia set titulo = ? , texto = ?, idioma = ? where idnoticia = ?;");
		$this->stmt->bind_param("sssi",$titulo, $texto, $idioma, $id);
		if($this->stmt->execute())
		{
			return true;
		}
		else
		{
			return false;
		}
   	}
}
?>