<?php
	class input
	{
		var $str_id;
		var $str_class;
		var $str_name;
		var $str_value;
		var $str_type;
		var $str_style;
		var $str_placeholder;
		
		function input($id = '', $class = '', $name = '',$value = '',$type = '',$placeholder = ' ',$style = '')
		{
			$this->str_id = $id;
			$this->str_class = $class;
			$this->str_name = $name;
			$this->str_value = $value;
			$this->str_type = $type;
			$this->str_style = $style;
			$this->str_placeholder = $placeholder;
		}
		
		function setId($id)
		{
			$this->str_id = $id;
		}
		
		function setPlaceholder($placeholder)
		{
			$this->str_placeholder = $placeholder;
		}
		
		function setClass($class)
		{
			$this->str_class = $class;
		}
		
		function setName($name)
		{
			$this->str_name = $name;
		}
		
		function setValue($value)
		{
			$this->str_value = $value;
		}
		
		function setType($type)
		{
			$this->str_type = $type;
		}
		
		function setStyle($style)
		{
			$this->str_style = $style;
		}
		
		function getId()
		{
			return $this->str_id;
		}
		
		function getPlaceholder()
		{
			return $this->str_placeholder;
		}
		
		function getClass()
		{
			return $this->str_class;
		}
		
		function getName()
		{
			return $this->str_name;
		}
		
		function getValue()
		{
			return $this->str_value;
		}
		
		function getType()
		{
			return $this->str_type;
		}
		
		function getStyle()
		{
			return $this->str_style;
		}
		
		function generar()
		{
			return "<input id='".$this->str_id."' class='".$this->str_class."' name='".$this->str_name."' value='".$this->str_value."' type='".$this->str_type."' style='".$this->str_style."' placeholder='".$this->str_placeholder."' />";
		}
	}
?>