<?php
	require_once 'input.php';
	require_once 'div.php';
	require_once 'lista.php';
	
	class form
	{
		var $str_final;
		var $arr_post;
		var $arr_estructura;
		var $str_metodo;
		var $str_id;
		var $str_class;
		var $str_style;
		
		function form($metodo = '', $id = '', $class = '', $style = '')
		{
			$this->str_metodo = $metodo;
			$this->str_id = $id;
			$this->str_class = $class;
			$this->str_style = $style;
		}
		
		function setMetodo($metodo)
		{
			$this->str_metodo = $metodo;
		}
		
		function getMetodo()
		{
			return $this->str_metodo;
		}
		
		function setId($id)
		{
			$this->str_id = $id;
		}
		
		function getId()
		{
			return $this->str_id;
		}
		
		function setClass($class)
		{
			$this->str_class = $class;
		}
		
		function getClass()
		{
			return $this->str_class;
		}
		
		function procesar($arr_post)
		{
			
		}
		
		function estructura($arr_estructura)
		{
			if(is_array($arr_estructura))
			{
				$this->arr_estructura = $arr_estructura;
			}
			else
			{
				echo "Debes pasar un array como estructura.";
			}
		}
		
		function mostrar()
		{
			$this->str_final = "<form method='".$this->str_metodo."' id='".$this->str_id."' class='".$this->str_class."' style='".$this->str_style."'>";
			foreach($this->arr_estructura as $type => $arr_detalles)
			{
				switch ($arr_detalles['etiqueta'])
				{
					case 'input':
						$input = new input();
						foreach($arr_detalles as $key => $value)
						{
							switch ($key)
							{
								case 'id':
									$input->setId($value);
								break;
								case 'placeholder':
									$input->setPlaceholder($value);
								break;
								case 'class':
									$input->setClass($value);
								break;
								case 'name':
									$input->setName($value);
								break;
								case 'value':
									$input->setValue($value);
								break;
								case 'type':
									$input->setType($value);
								break;
								case 'style':
									$input->setStyle($value);
								break;
							}
						}
						$this->str_final .= $input->generar();
					break;
					
					case 'div':
						$div = new div();
						foreach($arr_detalles as $key => $value)
						{
							switch ($key)
							{
								case 'id':
									$div->setId($value);
								break;
								case 'class':
									$div->setClass($value);
								break;
								case 'name':
									$div->setName($value);
								break;
								case 'style':
									$div->setStyle($value);
								break;
								case 'hidden':
									$div->setHidden($value);
								break;
								case 'elementos':
									foreach ($value as $nombre=> $detalles)
									{
										switch ($detalles['etiqueta'])
										{
											case 'input':
												$input = new input();
												foreach($detalles as $key => $value)
												{
													switch ($key)
													{
														case 'id':
															$input->setId($value);
														break;
														case 'placeholder':
															$input->setPlaceholder($value);
														break;
														case 'class':
															$input->setClass($value);
														break;
														case 'name':
															$input->setName($value);
														break;
														case 'value':
															$input->setValue($value);
														break;
														case 'type':
															$input->setType($value);
														break;
														case 'style':
															$input->setStyle($value);
														break;
													}
												}
												$div->insert($input->generar());
											break;
											case 'salto':
												$div->insert('</br>');
											break;
											case 'lista':
											$lista = new lista();
											foreach($detalles as $key => $value)
											{
												switch ($key)
												{
													case 'id':
														$lista->setId($value);
													break;
													case 'class':
														$lista->setClass($value);
													break;
													case 'name':
														$lista->setName($value);
													break;
													case 'value':
														$lista->setValue($value);
													break;
													case 'type':
														$lista->setType($value);
													break;
													case 'style':
														$lista->setStyle($value);
													break;
													case 'ordenada':
														$lista->setOrden($value);
													break;
													case 'role':
														$lista->setRole($value);
													break;
													case 'elementos':
														foreach($value as $index => $dato)
														{
															$lista->insert($index,$dato);
														}
													break;
												}
											}
											$div->insert($lista->generar());
										break;
										default:
											if($detalles['etiqueta'] == 'div')
											{
												$div->insert("<".$detalles['etiqueta']." id='".$detalles['id']."' class='".$detalles['class']."' name='".$detalles['name']."' style='".$detalles['style']."' >");
											}
											elseif ($detalles['etiqueta'] == '/div')
											{
												$div->insert("</div>");
											}
											elseif(isset($detalles['href']) && isset($detalles['data_dismiss']))
											{
												$div->insert("<".$detalles['etiqueta']." href='".$detalles['href']."' id='".$detalles['id']."' aria-label='".$detalles['aria_label']."' data-dismiss='".$detalles['data_dismiss']."' class='".$detalles['class']."' name='".$detalles['name']."' style='".$detalles['style']."' >".$detalles['value']."</".$detalles['etiqueta'].'>');
											}
											elseif(isset($detalles['data-toggle']) && isset($detalles['data-target']))
											{
												$div->insert("<".$detalles['etiqueta']." id='".$detalles['id']."' type='".$detalles['type']."' class='".$detalles['class']."' data-target='".$detalles['data-target']."' data-toggle='".$detalles['data-toggle']."' name='".$detalles['name']."' style='".$detalles['style']."' >".$detalles['value']."</".$detalles['etiqueta'].'>');												
											}
											elseif(isset($detalles['data-toggle']))
											{
												$div->insert("<".$detalles['etiqueta']." id='".$detalles['id']."' type='".$detalles['type']."' class='".$detalles['class']."' data-toggle='".$detalles['data-toggle']."' name='".$detalles['name']."' style='".$detalles['style']."' >".$detalles['value']."</".$detalles['etiqueta'].'>');
											}
											elseif(isset($detalles['type']))
											{
												$div->insert("<".$detalles['etiqueta']." id='".$detalles['id']."' type='".$detalles['type']."' class='".$detalles['class']."' name='".$detalles['name']."' style='".$detalles['style']."' >".$detalles['value']."</".$detalles['etiqueta'].'>');
											}
											else
											{
												$div->insert("<".$detalles['etiqueta']." id='".$detalles['id']."' class='".$detalles['class']."' name='".$detalles['name']."' style='".$detalles['style']."' >".$detalles['value']."</".$detalles['etiqueta'].'>');
											}
										break;
										}
									}
								break;
							}
						}
						$this->str_final .= $div->generar();
					break;
					
					case 'lista':
						$lista = new lista();
						foreach($arr_detalles as $key => $value)
						{
							switch ($key)
							{
								case 'id':
									$lista->setId($value);
								break;
								case 'class':
									$lista->setClass($value);
								break;
								case 'name':
									$lista->setName($value);
								break;
								case 'value':
									$lista->setValue($value);
								break;
								case 'type':
									$lista->setType($value);
								break;
								case 'style':
									$lista->setStyle($value);
								break;
								case 'ordenada':
									$lista->setOrden($value);
								break;
								case 'role':
									$lista->setRole($value);
								break;
								case 'elementos':
									foreach($value as $index => $dato)
									{
										$lista->insert($dato);
									}
								break;
							}
						}
						$this->str_final .= $lista->generar();
					break;
					case 'salto':
						$this->str_final .='</br>';
					break;
					default:
						if(isset($arr_detalles['type']) && isset($arr_detalles['data-toggle']))
						{
							$this->str_final .= "<".$arr_detalles['etiqueta']." id='".$arr_detalles['id']."' type='".$arr_detalles['type']."' class='".$arr_detalles['class']."'  data-toggle='".$arr_detalles['data-toggle']."' name='".$arr_detalles['name']."' style='".$arr_detalles['style']."' >".$arr_detalles['value']."</".$arr_detalles['etiqueta'].'>';
						}
						elseif(isset($arr_detalles['type']))
						{
							$this->str_final .= "<".$arr_detalles['etiqueta']." id='".$arr_detalles['id']."' type='".$arr_detalles['type']."' class='".$arr_detalles['class']."' name='".$arr_detalles['name']."' style='".$arr_detalles['style']."' >".$arr_detalles['value']."</".$arr_detalles['etiqueta'].'>';
						}
						else
						{
							$this->str_final .= "<".$arr_detalles['etiqueta']." id='".$arr_detalles['id']."' class='".$arr_detalles['class']."' name='".$arr_detalles['name']."' style='".$arr_detalles['style']."' >".$arr_detalles['value']."</".$arr_detalles['etiqueta'].'>';
						}
					break;
				}
			}
			$this->str_final .= "</form>";
			
			echo $this->str_final;
		}
	}
?>
