<?php
	class div
	{
		var $str_final;
		var $str_id;
		var $str_class;
		var $str_style;
		var $str_name;
		var $boo_hidden;
		var $str_role;
		var $arr_elements = array();
		
		function div($id = '', $class = '', $name = '', $arr_elements = '', $style = '', $str_role = '', $boo_hidden = false)
		{
			$this->str_id = $id;
			$this->str_class = $class;
			$this->str_style = $style;
			$this->str_name = $name;
			$this->arr_elements = $arr_elements;
			$this->str_role = $str_role;
			$this->boo_hidden = $boo_hidden;
		}
		
		function setId($id)
		{
			$this->str_id = $id;
		}
		
		function setRole($role)
		{
			$this->str_role = $role;
		}
		
		function setHidden($hidden)
		{
			$this->boo_hidden = $hidden;
		}
		
		function setClass($class)
		{
			$this->str_class = $class;
		}
		
		function setName($name)
		{
			$this->str_name = $name;
		}
		
		function setStyle($style)
		{
			$this->str_style = $style;
		}
		
		function getId()
		{
			return $this->str_id;
		}
		
		function getRole()
		{
			return $this->str_role;
		}
		
		function getHidden()
		{
			return $this->boo_hidden;
		}
		
		function getClass()
		{
			return $this->str_class;
		}
		
		function getName()
		{
			return $this->str_name;
		}
		
		function getStyle()
		{
			return $this->str_style;
		}
		
		function insert($elemento)
		{
			$this->arr_elements[] = $elemento;
		}
		
		function generar()
		{
			$this->str_final = "<div id='".$this->str_id."' role='".$this->str_role."' class='".$this->str_class."' name='".$this->str_name."' style='".$this->str_style."' aria-hidden='".$this->boo_hidden."'>";
			foreach($this->arr_elements as $key => $elemento)
			{
				$this->str_final .= $elemento;
			}
			$this->str_final .= "</div>";
			return $this->str_final;
		}
	}
?>