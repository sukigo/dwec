<?php
	class lista
	{
		var $str_final;
		var $str_id;
		var $str_class;
		var $str_style;
		var $str_name;
		var $boo_ordenada;
		var $str_role;
		var $arr_elements = array();
		
		function lista($id = '', $class = '', $name = '', $arr_elements = '', $style = '', $role = '', $ordenada = false)
		{
			$this->str_id = $id;
			$this->str_class = $class;
			$this->str_style = $style;
			$this->str_name = $name;
			$this->arr_elements = $arr_elements;
			$this->boo_ordenada = $ordenada;
			$this->str_role = $role;
		}
		
		function setId($id)
		{
			$this->str_id = $id;
		}
		
		function setRole($role)
		{
			$this->str_role = $role;
		}
		
		function setClass($class)
		{
			$this->str_class = $class;
		}
		
		function setName($name)
		{
			$this->str_name = $name;
		}
		
		function setStyle($style)
		{
			$this->str_style = $style;
		}
		
		function setOrden($orden)
		{
			$this->boo_ordenada = $orden;
		}
		
		function getId()
		{
			return $this->str_id;
		}
		
		function getRole()
		{
			return $this->str_role;
		}
		
		function getClass()
		{
			return $this->str_class;
		}
		
		function getName()
		{
			return $this->str_name;
		}
		
		function getStyle()
		{
			return $this->str_style;
		}
		
		function getOrden()
		{
			return $this->boo_ordenada;
		}
		
		function insert($id,$elemento)
		{
			$this->arr_elements[] = '<li id="'.$id.'">'.$elemento.'</li>';
		}
		
		function generar()
		{
			if($this->boo_ordenada)
			{
				$this->str_final = "<ol id='".$this->str_id."' class='".$this->str_class."' role='".$this->str_role."' name='".$this->str_name."' style='".$this->str_style."'>";
				foreach($this->arr_elements as $key => $elemento)
				{
					$this->str_final .= $elemento;
				}
				$this->str_final .= "</ol>";
			}
			else
			{
				$this->str_final = "<ul id='".$this->str_id."' class='".$this->str_class."' role='".$this->str_role."' name='".$this->str_name."' style='".$this->str_style."'>";
				foreach($this->arr_elements as $key => $elemento)
				{
					$this->str_final .= $elemento;
				}
				$this->str_final .= "</ul>";
			}
			return $this->str_final;
		}
	}
?>