<?php

	require_once './clases/form.php';
	require_once './clases/BD.php';
	session_start();
	$bd=BD::getInstance();
	$arr_palabras = $bd->listarPalabras($_SESSION['idioma']);
	if(!isset($_SESSION['arr_noticias']))
	{
		$_SESSION['arr_noticias'] = $bd->listarNoticias($_SESSION['idioma']);
	}
	if($tipo_user = $bd->iniciaSesion($_SESSION['user'],$_SESSION['pass']))
	{
		if($_SESSION['tipo_user'] == "administrador")
		{
			$tabla = "";
			if($_SESSION['arr_noticias'])
			{
				$tabla = "<table class='table' style='background:white;'>";
				//cambiar mensaje
				$tabla .= "<tr><th>ID</th><th>Titulo</th><th>Accion</th></tr>";
				foreach ($_SESSION['arr_noticias'] as $key => $notica)
				{
					$tabla .= "<tr><td>".$notica['id']."</td><td>".$notica['titulo']."</td><td style='padding:5px;><form method='POST'><input type='text' name='linea' value='".$notica['id']."' style='display: none'/><button class='btn btn-primary' name='eliminar' type='submit' ><span class='glyphicon glyphicon-trash'></span></button></form><a class='btn btn-primary' data-toggle='modal' data-target='#modificar_noticia' ><span class='glyphicon glyphicon-edit'></span></a></form></td></tr>";
				}
				$tabla .= "</table>";
			}
			else
			{
				//cambiar mensaje
				$tabla = "<h4 style='text-align: center;'>No hay noticias disponibles</h4>";
			}
			if(isset($_POST) && $_POST != NULL)
			{
				if(isset($_POST['eliminar']))
				{
					if($bd->borrarNoticia($_POST['linea']))
					{
						$_SESSION['arr_noticias'] = $bd->listarNoticias($_SESSION['idioma']);
						$tabla = "";
						if($_SESSION['arr_noticias'])
						{
							$tabla = "<table class='table' style='background:white;'>";
							//cambiar mensaje
							$tabla .= "<tr><th>ID</th><th>Titulo</th><th>Accion</th></tr>";
							foreach ($_SESSION['arr_noticias'] as $key => $notica)
							{
								$tabla .= "<tr><td>".$notica['id']."</td><td>".$notica['titulo']."</td><td style='padding:5px;><form method='POST'><input type='text' name='linea' value='".$notica['id']."' style='display: none'/><button class='btn btn-primary' name='eliminar' type='submit' ><span class='glyphicon glyphicon-trash'></span></button></form><a class='btn btn-primary' data-toggle='modal' data-target='#modificar_noticia' ><span class='glyphicon glyphicon-edit'></span></a></form></td></tr>";
							}
							$tabla .= "</table>";
						}
						else
						{
							//cambiar mensaje
							$tabla = "<h4 style='text-align: center;'>No hay noticias disponibles</h4>";
						}
						//cambiar mensaje
						echo '<script language="javascript">alert("Noticia eliminada");</script>';
					}
				}
				
				if(isset($_POST['modificar']))
				{
					$titulo = $_POST['titulo'];
					$texto = $_POST['texto'];
					$idioma = $_POST['idioma'];
					$id = $_POST['id'];
					if($titulo != "" && $texto != "" && $idioma != "" && $id != "")
					{
						if($bd->modificarNoticia($titulo,$texto,$_SESSION['idioma'],$id))
						{
							$_SESSION['arr_noticias'] = $bd->listarNoticias($_SESSION['idioma']);
							$tabla = "";
							if($_SESSION['arr_noticias'])
							{
								$tabla = "<table class='table' style='background:white;'>";
								//cambiar mensaje
								$tabla .= "<tr><th>ID</th><th>Titulo</th><th>Accion</th></tr>";
								foreach ($_SESSION['arr_noticias'] as $key => $notica)
								{
									$tabla .= "<tr><td>".$notica['id']."</td><td>".$notica['titulo']."</td><td style='padding:5px;><form method='POST'><input type='text' name='linea' value='".$notica['id']."' style='display: none'/><button class='btn btn-primary' name='eliminar' type='submit' ><span class='glyphicon glyphicon-trash'></span></button></form><a class='btn btn-primary' data-toggle='modal' data-target='#modificar_noticia' ><span class='glyphicon glyphicon-edit'></span></a></form></td></tr>";
								}
								$tabla .= "</table>";
							}
							else
							{
								//cambiar mensaje
								$tabla = "<h4 style='text-align: center;'>No hay noticias disponibles</h4>";
							}
							//cambiar mensaje
							echo '<script language="javascript">alert("Noticia Modificada");</script>';
						}
						else
						{
							//cambiar mensaje
							echo '<script language="javascript">alert("Error al modificar Noticia");</script>';
						}
					}
				}
				
				if(isset($_POST['insertar']))
				{
					$titulo = $_POST['titulo'];
					$texto = $_POST['texto'];
					
					if($titulo != "" && $texto != "")
					{
						if($bd->insertarNoticia($titulo,$texto,$_SESSION['idioma']))
						{
							$_SESSION['arr_noticias'] = $bd->listarNoticias($_SESSION['idioma']);
							$tabla = "";
							if($_SESSION['arr_noticias'])
							{
								$tabla = "<table class='table' style='background:white;'>";
								//cambiar mensaje
								$tabla .= "<tr><th>ID</th><th>Titulo</th><th>Accion</th></tr>";
								foreach ($_SESSION['arr_noticias'] as $key => $notica)
								{
									$tabla .= "<tr><td>".$notica['id']."</td><td>".$notica['titulo']."</td><td><form method='POST'><input type='text' name='linea' value='".$notica['id']."' style='display: none'/><button class='btn btn-primary' name='eliminar' type='submit' ><span class='glyphicon glyphicon-trash'></span></button></form><a class='btn btn-primary' data-toggle='modal' data-target='#modificar_noticia' ><span class='glyphicon glyphicon-edit'></span></a></td></tr>";
								}
								$tabla .= "</table>";
							}
							else
							{
								//cambiar mensaje
								$tabla = "<h4 style='text-align: center;'>No hay noticias disponibles</h4>";
							}
							//cambiar mensaje
							echo '<script language="javascript">alert("Noticia insertada correctamente");</script>';
						}
						else
						{
							//cambiar mensaje
							echo '<script language="javascript">alert("Error al insertar Noticia");</script>';
						}
					}
					else
					{
						//cambiar mensaje
						echo '<script language="javascript">alert("Rellene correctamente los campos");</script>';
					}
				}
			}
			
			if(isset($_REQUEST['sesion']))
			{
				session_destroy();
				header('Location: administrator.php');
			}
			
			$arr_estructura = array(
				"div_nav" => array(
					"etiqueta" => "div",
					"id" => "div_nav",
					"class" => "navbar navbar-default navbar-fixed-top",
					"name" => "div_nav",
					"style" => "background: black;background-image: linear-gradient(to bottom, #222, #111);border-bottom-color: black; box-shadow: -2px 6px 5px #000;border-bottom-left-radius: 5px;border-bottom-right-radius: 5px;",
					"role" => "navigation",
					"elementos" => array(

						"div_header" => array(
							"etiqueta" => "div",
							"id" => "div_header",
							"class" => "navbar-header",
							"name" => "div_header",
							"style" => ""
						),
						"button" => array(
							"etiqueta" => "button",
							"id" => "button_desplegable",
							"class" => "navbar-toggle collapsed",
							"name" => "button_desplegable",
							"data-toggle" => "collapse",
							"data-target" => ".navbar-collapse",
							"type" => "button",
							"value" => "<span class='sr-only'>Toggle navegación</span><span class='icon-bar'></span><span class='icon-bar'></span><span class='icon-bar'></span>",
							"style" => ""
						),
						"Inicio" => array(
							"etiqueta" => "a",
							"id" => "titulo",
							"class" => "navbar-brand",
							"name" => "titulo",
							"href" => "#",
							"value" => "Inicio",
							"style" => "color: white;"
						),
						"/div1" => array(
							"etiqueta" => "/div"
						),
						"div_collapse" => array(
							"etiqueta" => "div",
							"id" => "div_collapse",
							"class" => "navbar-collapse collapse",
							"name" => "div_collapse",
							"style" => ""
						),
						"lista_menu" => array(
							"etiqueta" => "lista",
							"id" => "lista_menu",
							"class" => "nav navbar-nav",
							"role" => "",
							"name" => "lista_menu",
							"style" => "background-color:black;background-image: linear-gradient(to bottom, #222, #111);",
							"ordenada" => false,
							"elementos" => array(
								"add_noticia" => '<a href="javascript:void(0)" class="btn" data-toggle="modal" data-target="#insert_noticia" style="color:white;">'.$arr_palabras['add_new']."</a>",
							)
						),
						"lista_menu2" => array(
							"etiqueta" => "lista",
							"id" => "lista_menu2",
							"class" => "nav navbar-nav navbar-right",
							"role" => "",
							"name" => "lista_menu2",
							"style" => "background-color:black;background-image: linear-gradient(to bottom, #222, #111);",
							"ordenada" => false,
							"elementos" => array(
								"log_out" => "<a style='color:white;margin-right:10px;'><span class='glyphicon glyphicon-log-out'></span> ".$arr_palabras['log_out']."</a>"					
							)
						),
						"/div2" => array(
							"etiqueta" => "/div"
						),
						"/div3" => array(
							"etiqueta" => "/div"
						)
					)
				),
				"div_content" => array(
					"etiqueta" => "div",
					"id" => "div_content",
					"class" => "card",
					"name" => "div_content",
					"style" => "margin:50px; margin-top: 100px;background: white;padding: 15px;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);",
					"role" => "",
					"elementos" => array(
						"titulo_noticias" => array(
							"etiqueta" => "h3",
							"id" => "titulo_noticia",
							"class" => "card-title",
							"name" => "titulo_noticias",
							"href" => "javascript:void(0)",
							"value" => "Lista noticias",
							"style" => "text-align: center;"
						),
						"tabla_noticias" => array(
							"etiqueta" => "p",
							"id" => "noticias",
							"class" => "card-block",
							"name" => "noticias",
							"href" => "javascript:void(0)",
							"value" => $tabla,
							"style" => ""
						)
					)
				)
			);
			
		}
		
		if($_SESSION['tipo_user'] == "autor")
		{
			echo "hola ".$_SESSION['tipo_user'];
		}
		
		$form = new form("post","form1","form-signin");
		$form->estructura($arr_estructura);
	}
	else
	{
		header('Location: administrator.php');
	}
		
?>

<!DOCTYPE html>
<html>
	<head>
	  	<title>Menú Usuario</title>
	  	<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
	  	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	  	<link rel="stylesheet" href="css/active_nav.css">
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	  	<script src="js/active_nav.js"></script>
	  	<script src="//cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
	</head>
	<body style="background-color: #3D72A4;">
		<?php 
			$form->mostrar(); 
		?>
		
		<div class="modal fade" id="insert_noticia" role="dialog">
		    <div class="modal-dialog">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title"><?php /*cambiar mensaje*/ echo "Añadir Noticia"; ?></h4>
		        </div>
		        <div class="modal-body">
		          <form class="form-horizontal" method="POST" style="padding: 10px;">
		          	<?php
		          	//cambiar textos por variables con los idiomas
		          		echo 	'<div class="col-xs-6 col-sm-6 col-md-6">
		    					<div class="form-group">
		    						<input type="text" name="titulo" id="titulo" class="form-control input-sm" placeholder="Titulo">
		    					</div>
		    				</div>
	          				
		    				<div class="col-xs-6 col-sm-6 col-md-6">
		    					<div class="form-group">
		    						<input type="file" name="imagen" id="imagen" class="form-control input-sm" value="imagen">
		    					</div>
		    				</div>
		    				<div class="col-xs-12 col-sm-12 col-md-12">
		    					<div class="form-group">
		    						<textarea name="texto" id="texto" class="form-control input-sm"></textarea>
		    					</div>
		    				</div>
	                <div class="form-group">
	                	<div class="col-sm-12 controls">
	                        <button type="submit" name="insertar" class="btn btn-primary pull-center">Insertar</button>                        
	                    </div>';
	                ?>
	                </div>
		          </form>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		      
		    </div>
		  </div>
		  
		  <div class="modal fade" id="modificar_noticia" role="dialog">
		    <div class="modal-dialog">
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title"><?php /*Cambiar mensaje */ echo "Modificar Noticia"; ?></h4>
		        </div>
		        <div class="modal-body">
		          <form class="form-horizontal" method="POST" style="padding: 10px;">
		          	<?php
		          	//cambiar textos por variables con los idiomas
		          	 echo 	'<div class="col-xs-6 col-sm-6 col-md-6">
		    					<div class="form-group">
		    						<input type="text" name="id" id="id" class="form-control input-sm" placeholder="Id">
		    					</div>
		    				</div>
		          	 		<div class="col-xs-6 col-sm-6 col-md-6">
		    					<div class="form-group">
		    						<input type="text" name="titulo" id="titulo" class="form-control input-sm" placeholder="Titulo">
		    					</div>
		    				</div>
		    				<div class="col-xs-6 col-sm-6 col-md-6">
		    					<div class="form-group">
		    						<input type="text" name="idioma" id="idioma" class="form-control input-sm" placeholder="Idioma">
		    					</div>
		    				</div>
		    				</br>
		    				<div class="col-xs-12 col-sm-12 col-md-12">
		    					<div class="form-group">
		    						<textarea name="texto" id="texto2" class="form-control input-sm"></textarea>
		    					</div>
		    				</div>
	                <div class="form-group">
	                	<div class="col-sm-12 controls">
	                        <button type="submit" name="modificar" class="btn btn-primary pull-center">Modificar</button>                        
	                    </div>';
	                ?>
	                </div>
		          </form>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		      
		    </div>
		  </div>
	</body>
</html>