<?php
	require_once './clases/form.php';
	require_once './clases/BD.php';
	$bd=BD::getInstance();
	$arr_palabras = $bd->listarPalabras("en");
	$user = "";
	$id_login = "en";
	$error = array(
				"etiqueta" => "label",
				"id" => "",
				"class" => "",
				"name" => "",
				"style" => "",
				"value" => "",
			);
	if(isset($_POST) && $_POST != NULL)
	{
		if(isset($_POST['es']) || isset($_POST['login_es']))
		{
			$arr_palabras = $bd->listarPalabras("es");
			$id_login = "es";
		}
		elseif(isset($_POST['fr']) || isset($_POST['login_fr']))
		{
			$arr_palabras = $bd->listarPalabras("fr");
			$id_login = "fr";
		}
		
		if(isset($_POST['user']) && isset($_POST['pass']) && $_POST['user'] != "" && $_POST['pass'] != "" && !isset($_POST['es']) && !isset($_POST['en']) && !isset($_POST['fr']))
		{
			$user = $_POST['user'];
			if($tipo_user = $bd->iniciaSesion($_POST['user'],$_POST['pass']))
			{
				session_start();
				$_SESSION['user'] = $_POST['user'];
				$_SESSION['pass'] = $_POST['pass'];
				$_SESSION['tipo_user'] = $tipo_user;
				$_SESSION['idioma'] = $id_login;
				header('Location: menu_user.php');
			}
			else
			{
				 $error = array(
							"etiqueta" => "div",
							"id" => "error_login",
							"class" => "modal fade",
							"name" => "div",
							"hidden" => "true",
							"style" => "margin:150px auto;float:none;width:35%;",
							"elementos" => array(
								"div_error" => array(
									"etiqueta" => "div",
									"id" => "error",
									"class" => "modal-dialog",
									"name" => "error",
									"value" => "",
									"type" => "",
									"style" => ""
								),
								"div_error" => array(
									"etiqueta" => "div",
									"id" => "error",
									"class" => "alert alert-danger",
									"name" => "error",
									"value" => $arr_palabras['error_login'],
									"type" => "",
									"style" => ""
								),
								"btn_close" => array(
									"etiqueta" => "button",
									"href" => "#",
									"class" => "close",
									"data_dismiss" => "modal",
									"aria_label" => "close",
									"value" => "&times;",
									"id" => "",
									"name" => "",
									"style" => ""
								),
								"label_div1" => array(
									"etiqueta" => "label",
									"id" => "",
									"class" => "",
									"name" => "",
									"value" => $arr_palabras['error_login'],
									"type" => "",
									"style" => ""
								),
								"/div1" => array(
									"etiqueta" => "/div"
								),
								"/div2" => array(
									"etiqueta" => "/div"
								)
							)
				);
			}
		}
		
			$arr_estructura = array(
				"div3" => array(
					"etiqueta" => "div",
					"id" => "div",
					"class" => "btn-group",
					"name" => "div",
					"style" => "position:fixed; top:5px; right:5px; font-weight: bold; margin-top: auto;",
					"elementos" => array(
						"button_language" => array(
							"etiqueta" => "button",
							"id" => "language",
							"class" => "btn btn-primary",
							"name" => "languaje",
							"value" => $arr_palabras['menu_lenguaje'],
							"type" => "button",
							"style" => "background-color: black;cursor: not-allowed;"
						),
						"button_drop_down" => array(
							"etiqueta" => "button",
							"id" => "desplegable",
							"class" => "btn btn-primary dropdown-toggle",
							"data-toggle" => "dropdown",
							"name" => "desplegable",
							"value" => '<span class="caret"></span>',
							"type" => "button",
							"style" => "background-color: black;"
						),
						"lista_idiomas" => array(
							"etiqueta" => "lista",
							"id" => "div",
							"class" => "dropdown-menu",
							"role" => "menu",
							"name" => "div",
							"style" => "background-color:black;color:white;",
							"ordenada" => false,
							"elementos" => array(
								"en" => "<input style ='background-color: black;border:hidden;' type='submit' name='en' value='English'/>",
								"es" => "<input style ='background-color: black;border:hidden;' type='submit' name='es' value='Español'/>",
								"fr" => "<input style ='background-color: black;border:hidden;' type='submit' name='fr' value='Français'/>"
							)
						)
					)
				),
				"div4" => $error,
				"div1" => array(
					"etiqueta" => "div",
					"id" => "div",
					"class" => "form-signin",
					"name" => "div",
					"style" => "text-align: center;",
					"elementos" => array(
						"input_user" => array(
							"etiqueta" => "input",
							"id" => "user",
							"class" => "form-control",
							"placeholder" => $arr_palabras['menu_correo'],
							"name" => "user",
							"value" => $user,
							"type" => "email",
							"style" => ""
						),
						"input_pass" => array(
							"etiqueta" => "input",
							"id" => "pass",
							"class" => "form-control",
							"placeholder" => $arr_palabras['menu_clave'],
							"name" => "pass",
							"value" => "",
							"type" => "password",
							"style" => ""
						),
						"button_login" => array(
							"etiqueta" => "button",
							"id" => "login",
							"class" => "btn btn-lg btn-primary btn-block",
							"name" => "login_".$id_login,
							"value" => $arr_palabras['titulo_iniciar'],
							"type" => "submit",
							"style" => "background-color: black;",
						)
					)
				)
			);
	}
	else
	{
		$arr_estructura = array(
			"div3" => array(
				"etiqueta" => "div",
				"id" => "div",
				"class" => "btn-group",
				"name" => "div",
				"style" => "position:fixed; top:5px; right:5px; font-weight: bold; margin-top: auto;",
				"elementos" => array(
					"button_language" => array(
						"etiqueta" => "button",
						"id" => "language",
						"class" => "btn btn-primary",
						"name" => "languaje",
						"value" => $arr_palabras['menu_lenguaje'],
						"type" => "button",
						"style" => "background-color: black;cursor: not-allowed;"
					),
					"button_drop_down" => array(
						"etiqueta" => "button",
						"id" => "desplegable",
						"class" => "btn btn-primary dropdown-toggle",
						"data-toggle" => "dropdown",
						"name" => "desplegable",
						"value" => '<span class="caret"></span>',
						"type" => "button",
						"style" => "background-color: black;"
					),
					"lista_idiomas" => array(
						"etiqueta" => "lista",
						"id" => "div",
						"class" => "dropdown-menu",
						"role" => "menu",
						"name" => "div",
						"style" => "background-color: black;color: white;",
						"ordenada" => false,
						"elementos" => array(
							"en" => "<input style ='background-color: black;border:hidden;' type='submit' name='en' value='English'/>",
							"es" => "<input style ='background-color: black;border:hidden;' type='submit' name='es' value='Español'/>",
							"fr" => "<input style ='background-color: black;border:hidden;' type='submit' name='fr' value='Français'/>"
						)
					)
				)
			),
			"div4" => $error,
			"div1" => array(
				"etiqueta" => "div",
				"id" => "div",
				"class" => "form-signin",
				"name" => "div",
				"style" => "text-align: center;",
				"elementos" => array(
					"input_user" => array(
						"etiqueta" => "input",
						"id" => "user",
						"class" => "form-control",
						"placeholder" => $arr_palabras['menu_correo'],
						"name" => "user",
						"value" => "",
						"type" => "email",
						"style" => ""
					),
					"input_pass" => array(
						"etiqueta" => "input",
						"id" => "pass",
						"class" => "form-control",
						"placeholder" => $arr_palabras['menu_clave'],
						"name" => "pass",
						"value" => "",
						"type" => "password",
						"style" => ""
					),
					"button_login" => array(
						"etiqueta" => "button",
						"id" => "login",
						"class" => "btn btn-lg btn-primary btn-block",
						"name" => "login_".$id_login,
						"value" => $arr_palabras['titulo_iniciar'],
						"type" => "submit",
						"style" => "background-color: black;"
					)
				)
			)
		);
	}
	
	$form = new form("post","form1","form-signin");
	$form->estructura($arr_estructura);
?>

<!DOCTYPE html>
<html>
	<head>
	  	<title>Panel Administrador</title>
	  	<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
	  	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	  	<script src="js/modal_login.js"></script>
        <link rel="stylesheet" href="css/signin.css" type="text/css"/>
	</head>
	<body style="background-color: #3D72A4;">
		<?php $form->mostrar(); ?>
	</body>
</html>