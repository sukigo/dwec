$(document).ready(function()
{
	letras = 0;
	$("#pass").on("keypress",function(event)
	{
		if(event.key == "Backspace")
		{
			if(letras > 0)
			{
				letras--;
			}
		}
		else
		{
			letras++;
		}
		
		if(letras < 5)
		{
			$("label").html("No segura");
		}
		else if(letras > 5 && letras < 8)
		{
			$("label").html("Medianamente segura");
		}
		else
		{
			$("label").html("Segura");
		}
	});
});
