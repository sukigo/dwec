function dibujarCanvas()
{
	var str_tabla = "<table style='border: none;border-spacing: 0px;'>";
	for (var i=0; i < 100; i++)
	{
		str_tabla += "<tr>";
	  	for (var x=0; x < 100; x++)
	  	{
				str_tabla += "<td id='"+i+","+x+"' onmousemove='colorear(event)'></td>"
		};
		str_tabla += "</tr>";
	};
	str_tabla +="</table>";
	
	document.getElementById("tabla").innerHTML = str_tabla;
}

function colorear(x)
{
	if(x.ctrlKey)
	{
		x.target.style.backgroundColor = "red";
	}
	if(x.shiftKey)
	{
		x.target.style.backgroundColor = "blue";
	}
	if(x.altKey)
	{
		x.target.style.backgroundColor = "white";
	}
}

window.onload = dibujarCanvas;

