function aumentarLetra()
{
	for (var i=0; i < document.getElementsByTagName("P").length; i++)
	{
		document.getElementsByTagName("P")[i].style.fontSize = "25px";
	};
}

function disminuirLetra()
{
	for (var i=0; i < document.getElementsByTagName("P").length; i++)
	{
		document.getElementsByTagName("P")[i].style.fontSize = "15px";
	};
}

function izquierdaLetra()
{
	for (var i=0; i < document.getElementsByTagName("P").length; i++)
	{
		document.getElementsByTagName("P")[i].style.textAlign = "left";
	};
}

function justificarLetra()
{
	for (var i=0; i < document.getElementsByTagName("P").length; i++)
	{
		document.getElementsByTagName("P")[i].style.textAlign = "justify";
	};
}