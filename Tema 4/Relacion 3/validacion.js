function validar()
{
	var error = "";
	if(document.getElementById("user_id").value != "")
	{
		if(document.getElementById("user_id").value.length < 5 || document.getElementById("user_id").value.length >12)
		{
			error += "*El campo User Id debe contener entre 5 y 12 caracteres.\n";
		}
	}
	else
	{
		error += "*Debes rellenar el campo User Id.\n";
	}
	
	if(document.getElementById("password").value != "")
	{
		if(document.getElementById("password").value.length < 7 || document.getElementById("password").value.length>12)
		{
			error += "*El campo Password debe contener entre 7 y 12 caracteres.\n";
		}
	}
	else
	{
		error += "*Debes rellenar el campo Password.\n";
	}
	
	if(document.getElementById("name").value != "")
	{
		if(/^([0-9])*$/.test(document.getElementById("name").value))
		{
			error += "*El campo Name solo debe tener letras.\n";
		}
	}
	else
	{
		error += "*Debes rellenar el campo Name.\n";
	}
	
	if(document.getElementById("country").value == "select")
	{
		error += "*Debes seleccionar un pais.\n";
	}
	
	if(document.getElementById("zip").value != "")
	{
		if(!/^([0-9])*$/.test(document.getElementById("zip").value))
		{
			error += "*El campo ZIP Code solo debe tener numeros.\n";
		}
	}
	else
	{
		error += "*Debes rellenar el campo ZIP Code.\n";
	}
	
	if(document.getElementById("email").value != "")
	{
		if(!/^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(document.getElementById("email").value))
		{
			error += "*Introduzca el email correctamente.\n";
		}
	}
	else
	{
		error += "*Debes rellenar el campo Email.\n";
	}
	
	if(document.getElementById("male").checked == true || document.getElementById("female").checked == true)
	{
		if(document.getElementById("male").checked == true && document.getElementById("female").checked == true)
		{
			error += "*Debe seleccionar solo 1 sexo.\n";
		}
	}
	else
	{
		error += "*Debes seleccionar un sexo.\n";
	}
	
	if(document.getElementById("english").checked == true || document.getElementById("non_english").checked == true)
	{
		if(document.getElementById("english").checked == true && document.getElementById("non_english").checked == true)
		{
			error += "*Debe seleccionar solo 1 idioma.\n";
		}
	}
	else
	{
		error += "*Debes seleccionar un idioma.\n";
	}
	
	if(error == "")
	{
		error = "No hay errores.";
	}
	alert(error);
}
