function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function cerrarSesion()
{
	var nombre = getCookie("nombre");
	if(nombre)
	{
		setCookie("nombre", nombre, 0);
		alert("Hasta pronto, "+nombre);
		location.reload();
	}
	else
	{
		document.getElementById("saludo").innerHTML = "No hay niguna sesion abierta.";
	}
}

function fondo()
{
	var fondo = prompt("Escriba el color de fondo:");
	document.cookie = "fondo="+fondo+";";
	document.getElementById("saludo").style.backgroundColor = fondo;
}

function texto()
{
	var texto = prompt("Escriba el tama\u00f1o de las letras:");
	document.cookie = "texto="+texto+";";
	document.getElementById("saludo").style.fontSize = texto+"px";
}

function parrafo()
{
	var parrafo = prompt("Escriba la alineacion del texto:");
	document.cookie = "parrafo="+parrafo+";";
	document.getElementById("saludo").style.textAlign = parrafo;
}

function checkCookies()
{
	var nombre = getCookie("nombre");
	var fondo = getCookie("fondo");
	var texto = getCookie("texto");
	var parrafo = getCookie("parrafo");
	if(nombre)
	{
		document.getElementById("saludo").innerHTML = "Hola "+nombre+'</br><input type="button" value="Parrafo" onclick="parrafo();"/><input type="button" value="Texto" onclick="texto();"/><input type="button" value="Fondo" onclick="fondo();"/><input type="button" value="Cerrar Sesion" onclick="cerrarSesion();"/>';
		if(fondo)
		{
			document.getElementById("saludo").style.backgroundColor = fondo;
		}
		if(texto)
		{
			document.getElementById("saludo").style.fontSize = texto;
		}
		if(parrafo)
		{
			document.getElementById("saludo").style.textAlign = parrafo;
		}
	}
	else
	{
		nombre = prompt("Escriba su nombre:");
		if (nombre != "" && nombre != null) 
		{
            setCookie("nombre", nombre, 0.00347222);
            document.getElementById("saludo").innerHTML = "Hola "+nombre+'</br><input type="button" value="Parrafo" onclick="parrafo();"/><input type="button" value="Texto" onclick="texto();"/><input type="button" value="Fondo" onclick="fondo();"/><input type="button" value="Cerrar Sesion" onclick="cerrarSesion();"/>';
        }
	}
}



window.onload = checkCookies;