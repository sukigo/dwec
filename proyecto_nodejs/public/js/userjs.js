$(document).ready(function()
{
    $('.modal-trigger').leanModal();

	function sendIcono(id)
	{
		socket.emit('sendIcono', id);
	}

	$("#b_nick").on("click",function()
	  {
	  	if($('#i_nick').val() != "")
	  	{
	  		var socket = io();
	  		var nick = $('#i_nick').val();
		  	
	  		socket.emit('nick', $('#i_nick').val());
	  		socket.emit('iconos', true);
	  		socket.on('exist', function(msg)
	  		{
	  			if(msg)
	  			{
					if(!$('#name_err').html())
			  		{
			  			Materialize.toast('Usuario ya en uso!', 3000, 'rounded');
			  		}	  			
			  	}
	  			else
	  			{
	  				$("#d_nick").hide();
			  		$('#name_err').remove();
				  	$("#chat").fadeIn();
	  			}
	  		});

	  		$('#form').submit(function()
	  		{
	  			if($('#form > input').val().lenght == 1)
	  			{
	  				socket.emit('mensaje', $('#form > input').val());
		    		$('#form > input').val('');
		    		return false;
	  			}
	  			else
	  			{
	  				socket.emit('mensaje_privado', $("#form > input").attr("id"),$("#form > input").val());
	  				$("#form > input").val('');
	  			}
	    		
	  		});
	  		
	  		$('#enviar').click(function()
	  		{
	  			if($('#chat_global').css("display") != "none")
	  			{
	  				socket.emit('mensaje', $('#form > input').val());
		    		$('#form > input').val('');
		    		return false;
	  			}
	  			else
	  			{
	  				socket.emit('mensaje_privado', $("#form > input").attr("id"),$("#form > input").val());
	  				if($("#form > input").val().trim() != "")
	  				{
	  					$('#chat_'+$("#form > input").attr("id")).append($('<li>').css({"text-align":"right","max-width ":" 50%;"}).text("Tú : "+$("#form > input").val()));
			    		$('#form > input').val('');
	  				}
		    		return false;
	  			}
	    		
	  		});
	  		
	  		$('#form > input').on("keydown",function()
	  		{
	  			if($('#chat_global').css("display") != "none")
	  			{
		  			if($('#form > input').val() != "")
		  			{
		  				socket.emit('escribiendo','');
		  			}
		  			else
		  			{
		  				socket.emit('no_escribiendo','');
		  			}
		  		}
		  		else
		  		{
		  			if($('#form > input').val() != "")
		  			{
		  				socket.emit('escribiendo_privado',$('#form > input').attr("id"));
		  			}
		  			else
		  			{
		  				socket.emit('no_escribiendo_privado',$('#form > input').attr("id"));
		  			}
		  		}
	  			
	  		});
	  
	  		socket.on('iconos',function(msg)
	  		{
	  			var str_final = '<div id="modalIconos" class="modal bottom-sheet"><div class="modal-content">';
	  			$.each(msg, function(key, val)
	            {
	            	str_final += "<a href='javascript:void(0)' id='"+key+"' >"+val+"</a>";
	            });
	            str_final += "</div></div>";
	            $("body").append(str_final);
	            
	            $('.modal-content > a').on("click",function()
		  		{
		  			if($('#chat_global').css("display") != "none")
	  				{
		  				socket.emit('sendIcono', $(this).attr("id"));
		  			}
		  			else
		  			{
		  				socket.emit('sendIconoPrivado',$("#form > input").attr("id"), $(this).attr("id"));
		  				$('#chat_'+$("#form > input").attr("id")).append($('<li>').css("text-align","right").html("Tú : "+$(this).html()));
		  			}
		  		});
	  		});
	  		
	  		socket.on('mensaje', function(msg)
	  		{
	  			$('#chat_global').append($('<li>').text(msg));	    		
	  		});
	  		
	  		socket.on('mensajeIcono', function(msg)
	  		{
	  			$('#chat_global').append($('<li>').html(msg));	    		
	  		});
	  		
	  		socket.on('mensajeIconoPrivado', function(emisor,msg)
	  		{
	  			$('#chat_'+emisor).append($('<li>').html(msg));	    		
	  		});
	  
	  		socket.on('saludo', function(msg)
	  		{
	    		$('#chat_global').append($('<li>').text(msg));
	  		});
	  
	  		socket.on('new', function(msg)
	  		{
	    		$('#chat_global').append($('<li>').text(msg));
	  		});

			socket.on('users_privado', function(msg)
	  		{
	  			if($('#chat_global').css("display") == "none")
	  			{
	  				$('#users').html("");
		  			$.each(msg, function(key, val)
		            {
		            	if(key != nick)
		            	{
		            		if(/[\^$.<>*+?=!:|\\()\[\]{}]/.test(val))
		            		{
		            			$("#users").append($('<li>').append($("<a>").attr({"id":key,"href":"javascript:void(0)"}).text(val)));
		            		}
		            		else
		            		{
		            			$("#users").append($('<li>').html("<a id='"+key+"' href='javascript:void(0)'>"+val+"</a>"));
		            		}
		            		
		            	}
		            	else
		            	{
		            		$("#users").append($('<li>').attr("id",key).text("Tú"));
		            	}
		            
		            });
	  			}
			});
		
	  		socket.on('users', function(msg)
	  		{
	  			$('#users').html("");
	  			$.each(msg, function(key, val)
	            {
	            	if(key != nick)
	            	{
	            		if(/[\^$.<>*+?=!:|\\()\[\]{}]/.test(val))
	            		{
	            			$("#users").append($('<li>').append($("<a>").attr({"id":key,"href":"javascript:void(0)"}).text(val)));
	            		}
	            		else
	            		{
	            			$("#users").append($('<li>').html("<a id='"+key+"' href='javascript:void(0)'>"+val+"</a>"));
	            		}
	            		
	            	}
	            	else
	            	{
	            		$("#users").append($('<li>').attr("id",key).text("Tú"));
	            	}
	                
	                $('#'+val).on("click",function()
			  		{
			  			if($("#chat_"+val).length == 0)
			  			{
			  				$("#rooms > ul").append($("<li>").append($("<button>").attr({"id":val,"class":"btn waves-effect waves-light"}).text(val)));
				  			$("#d_mensajes").append("<ul id='chat_"+val+"'></ul>");
				  			$("#d_mensajes > ul").hide();
				  			$("#chat_"+val).fadeIn();
				  			$("#form > input").attr("id",val);
				  			
				  			$("#rooms > ul > li > button").on("click",function()
				  			{
				  				if($(this).attr("id") == "global")
				  				{
				  					$("#d_mensajes > ul").hide();
					  				$("#chat_"+$(this).attr("id")).fadeIn();
					  				$("#form > input").attr("id",$(this).attr("id"));
					  				socket.emit('refresh_user');
				  				}
				  				else
				  				{
				  					$("#d_mensajes > ul").hide();
					  				$("#chat_"+$(this).attr("id")).fadeIn();
					  				$("#form > input").attr("id",$(this).attr("id"));
					  				socket.emit('refresh_user_privado',$(this).attr("id"));
				  				}
				  				
				  			});
				  			
				  			socket.emit('chatPrivado', nick ,key );
			  			}
			  		});
	            });
	  		});
	  		
	  		socket.on("mensaje_privado",function(emisor,val)
	  		{
	  			$('#chat_'+emisor).append($('<li>').text(val));
	  		});
	  		
	  		socket.on("chatPrivado",function(val)
	  		{
	  			$("#rooms > ul").append($("<li>").append($("<button>").attr({"id":val,"class":"btn waves-effect waves-light"}).text(val)));
	  			$("#d_mensajes").append("<ul id='chat_"+val+"'></ul>");
	  			$("#d_mensajes > ul").hide();
	  			$("#chat_"+val).fadeIn();
	  			$("#form > input").attr("id",val);
	  			
	  			$("#rooms > ul > li > button").on("click",function()
	  			{
	  				$("#d_mensajes > ul").hide();
	  				$("#chat_"+$(this).attr("id")).fadeIn();
	  				$("#form > input").attr("id",$(this).attr("id"));
	  			});
	  		});
	  		
	  		socket.on("user_disconect",function(msg)
	  		{
	  			$('#chat_global').append($('<li>').text(msg));
	  		});
	  	}
	  	else
	  	{
	  		if(!$('#name_err').html())
	  		{
				Materialize.toast('Usuario no valido!', 3000, 'rounded');	  		
			}
	  	}
	  });
});
