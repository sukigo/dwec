var express= require('express');
var app = express();
var path = require('path');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var puerto = process.env.PORT || 3000;


var users = {};
var id = {};
var iconos = new Array("<img src='/img/alegre.jpg' style='height: 20px;' />","<img src='/img/cabreo.jpg' style='height: 20px;'/>","<img src='/img/dormir.jpg' style='height: 20px;'/>","<img src='/img/risa.jpg' style='height: 20px;'/>");

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

app.use(express.static('public'));
app.get('/', function(req, res)
{
  	res.sendFile(path.join(__dirname+'public/index.html'));
});

io.on('connection', function(socket)
{ 
  socket.on("nick",function(msg)
  {
  	if(users[msg])
  	{
  		socket.emit('exist',true);
  	}
  	else
  	{
  		users[msg] = msg;
  		id[msg] = socket.id;
	  	socket.username = msg;
	  	socket.broadcast.emit("new","Se ha conectado "+msg);
	  	socket.emit("saludo","Bienvenido "+msg);
	  	socket.emit("users",users);
	  	socket.broadcast.emit("users",users);
	  	socket.emit('exist',false);
  	}
	  	
  });
  
  socket.on('iconos', function(msg)
  {
  	if(msg)
  	{
  		socket.emit('iconos',iconos);
  	}
  });
  
  socket.on('sendIcono', function(msg)
  {
  	io.emit('mensajeIcono', socket.username + ": " + iconos[msg]);
  });
  
  socket.on('sendIconoPrivado', function(receptor,msg)
  {
  	io.to(id[receptor]).emit('mensajeIconoPrivado',socket.username,socket.username+" : "+ iconos[msg]);
  });
  
  socket.on('chatPrivado', function(emisor, receptor)
  {
  	io.to(id[receptor]).emit('chatPrivado',emisor);
  });
  
  socket.on('mensaje', function(msg)
  {
  	var mensaje = msg;
  	var res = msg.trim();
	if(res != "")
	{
	    io.emit('mensaje', socket.username + ": " + mensaje);
	    users[socket.username] = socket.username;
	  	socket.broadcast.emit("users",users);
	}
  });
  
  
  
  socket.on('mensaje_privado', function(receptor,msg)
  {
  	var mensaje = msg;
  	var res = msg.trim();
	if(res != "")
	{
	    io.to(id[receptor]).emit('mensaje_privado',socket.username,socket.username+" : "+ mensaje);
	    users[socket.username] = socket.username;
	  	socket.broadcast.emit("users",users);
	}
  });
  
  socket.on('escribiendo_privado', function(receptor)
  {
  	users[socket.username] = socket.username +" esta escribiendo...";
  	var privado = {};
  	privado[receptor] = receptor;
  	privado[socket.username] = socket.username+" escribiendo...";
  	io.to(id[receptor]).emit("users_privado",privado);
  });
  
  socket.on('no_escribiendo_privado', function(receptor)
  {
  	users[socket.username] = socket.username;
  	var privado = {};
  	privado[receptor] = receptor;
  	privado[socket.username] = socket.username;
  	socket.broadcast.emit("users_privado",privado);
  });
  
  socket.on('refresh_user', function()
  {
  	socket.broadcast.emit("users",users);
  });
  
  socket.on('refresh_user_privado', function(receptor)
  {
  	var privado = {};
  	privado[receptor] = receptor;
  	privado[socket.username] = socket.username;
  	socket.broadcast.emit("users",privado);
  });
  
  socket.on('escribiendo', function()
  {
  	users[socket.username] = socket.username +" esta escribiendo...";
  	socket.broadcast.emit("users",users);
  });
  
  socket.on('no_escribiendo', function()
  {
  	users[socket.username] = socket.username;
  	socket.broadcast.emit("users",users);
  });
			
  socket.on('disconnect', function()
  {
  	if(typeof(socket.username) == "undefined")
	{
		return;
	}
	delete users[socket.username];
	socket.broadcast.emit("users",users);
    socket.broadcast.emit('user_disconect', socket.username+" se ha desconectado.");
  });
});

http.listen(puerto, function()
{
  console.log('listening on *:3000');
});