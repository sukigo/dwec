var nomUno = new Array("Juan Jimenez","Paco Martinez","Jose Dominguez");
var nomDos = new Array("Pepe Botella","Rocio Jurado","Marta Jerez");

function apellidoConJ(value)
{
	var nombre = value.split(" ");
	if(nombre[1].indexOf("J")>-1)
	{
		return nombre;
	}
}

var filtradoUno = nomUno.filter(apellidoConJ);
var filtradoDos = nomDos.filter(apellidoConJ);

var concat = filtradoUno.concat(filtradoDos);
var iniciales = concat.map(function(cad)
{
	return cad.substr(0,1);
})
iniciales.sort();
document.write(iniciales);
