function potenciaRecursiva(base, exp)
{
	if(exp == 0)
	{
		return 1;
	}
	else
	{
		return base * potenciaRecursiva(base,(exp-1))
	}
}

var a = prompt("Introduzca la base:");
var b = prompt("Introduzca el exponente:");

a = parseInt(a);
b = parseInt(b);

var x = potenciaRecursiva(a,b);

document.write("El resultado es: "+x);


