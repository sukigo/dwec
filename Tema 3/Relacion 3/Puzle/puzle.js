function pieza(URL,posX,posY,altura,anchura)
{
	esta_pieza = this;
	var url = URL;
	var posX = posX;
	var posY = posY;
	var altura = altura || 0;
	var anchura = anchura || 0;
	
	esta_pieza.showResult = function()
	{
		if(altura == 0 && anchura ==0)
		{
			return "<img src='"+url+"'/>";
		}
		else
		{
			if(altura == 0)
			{
				return "<img src='"+url+"' width='"+anchura+"'/>";
			}
			else if(anchura == 0)
			{
				return "<img src='"+url+"' height = '"+altura+"' />";
			}
			else
			{
				return "<img src='"+url+"' height = '"+altura+"' width='"+anchura+"'/>";
			}
		}
	}
	
	esta_pieza.show = function()
	{
		if(altura == 0 && anchura ==0)
		{
			return "<a href='javascript:este_puzle.moverPieza("+posY+","+posX+")' ><img src='"+url+"'/></a>";
		}
		else
		{
			if(altura == 0)
			{
				return "<a href='javascript:este_puzle.moverPieza("+posY+","+posX+")' ><img src='"+url+"' width='"+anchura+"'/></a>";
			}
			else if(anchura == 0)
			{
				return "<a href='javascript:este_puzle.moverPieza("+posY+","+posX+")' ><img src='"+url+"' height = '"+altura+"' /></a>";
			}
			else
			{
				return "<a href='javascript:este_puzle.moverPieza("+posY+","+posX+")'><img src='"+url+"' height = '"+altura+"' width='"+anchura+"'/></a>";
			}
		}
	}
	
	esta_pieza.getUrl = function()
	{
		return url;
	}
	
	esta_pieza.setPosX = function(PosX)
	{
		posX = PosX;
	}
	
	esta_pieza.setPosY = function(PosY)
	{
		posY = PosY;
	}
	
	esta_pieza.getPosX = function()
	{
		return posX;
	}
	
	esta_pieza.getPosY = function()
	{
		return posY
	}
}

function puzle(enlace_html,array_piezas,filas,columnas)
{
	este_puzle = this;
	var etiqueta = enlace_html;
	var piezas_ordenadas = array_piezas;
	var piezas_desordenadas = new Array();
	var y = filas;
	var x = columnas;
	var str_tabla = "";
	var num_mov = 0;
	var finalizado = false;
	var time_in = new Date();
	
	este_puzle.desordenar = function()
	{
		var piezas_usadas = new Array();
		var usada = false;
		for (var i=0; i < y ; i++)
		{
			piezas_desordenadas[i] = new Array();
			for (var j=0; j < x; j++)
			{
			  if(piezas_usadas.length == 0)
			  {
			  	var rand_x = Math.floor(Math.random()*x);
			  	var rand_y = Math.floor(Math.random()*y);
			  	piezas_usadas.push(piezas_ordenadas[rand_y][rand_x].getUrl());
			  }
			  else
			  {
			  	while(usada)
			  	{
			  		usada = false;
			  		rand_x = Math.floor(Math.random()*x);
				  	rand_y = Math.floor(Math.random()*y);
				  	for (var h=0; h < piezas_usadas.length; h++)
				  	{
						if(piezas_usadas[h] == piezas_ordenadas[rand_y][rand_x].getUrl())
			  			{
			  				usada = true;
			  			}
					};
				 }
				 piezas_usadas.push(piezas_ordenadas[rand_y][rand_x].getUrl());
			  }
			  
			  if(!usada)
			  {
			  	piezas_desordenadas[i][j] = piezas_ordenadas[rand_y][rand_x];
			  	piezas_desordenadas[i][j].setPosY(i);
			  	piezas_desordenadas[i][j].setPosX(j);
			  	usada = true;
			  }
			};
		};
	}
	
	este_puzle.moverPieza = function(posY,posX)
	{
		if(posY == 0 || posX == 0)
		{
			if(posY == 0 && posX == 0)
			{
				if(piezas_desordenadas[posY+1][posX].getUrl() == "blanco.jpg")
				{
					var aux = piezas_desordenadas[posY][posX];
					piezas_desordenadas[posY][posX] = piezas_desordenadas[posY+1][posX];
					piezas_desordenadas[posY][posX].setPosY(posY);
					piezas_desordenadas[posY+1][posX] = aux;
					piezas_desordenadas[posY+1][posX].setPosY(posY+1);
					num_mov++;
					este_puzle.show();
					
				}
				else if(piezas_desordenadas[posY][posX+1].getUrl() == "blanco.jpg")
				{
					var aux = piezas_desordenadas[posY][posX];
					piezas_desordenadas[posY][posX] = piezas_desordenadas[posY][posX+1];
					piezas_desordenadas[posY][posX].setPosX(posX);
					piezas_desordenadas[posY][posX+1] = aux;
					piezas_desordenadas[posY][posX+1].setPosX(posX+1);
					num_mov++;
					este_puzle.show();
				}
			}
			if(posY == 0)
			{
				if(posX != columnas-1)
				{
					if(piezas_desordenadas[posY+1][posX].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY+1][posX];
						piezas_desordenadas[posY][posX].setPosY(posY);
						piezas_desordenadas[posY+1][posX] = aux;
						piezas_desordenadas[posY+1][posX].setPosY(posY+1);
						num_mov++;
						este_puzle.show();
					}
					else if(piezas_desordenadas[posY][posX+1].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY][posX+1];
						piezas_desordenadas[posY][posX].setPosX(posX);
						piezas_desordenadas[posY][posX+1] = aux;
						piezas_desordenadas[posY][posX+1].setPosX(posX+1);
						num_mov++;
						este_puzle.show();
					}
					else if(piezas_desordenadas[posY][posX-1].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY][posX-1];
						piezas_desordenadas[posY][posX].setPosX(posX);
						piezas_desordenadas[posY][posX-1] = aux;
						piezas_desordenadas[posY][posX-1].setPosX(posX-1);
						num_mov++;
						este_puzle.show();
					}
				}
				else
				{
					if(piezas_desordenadas[posY+1][posX].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY+1][posX];
						piezas_desordenadas[posY][posX].setPosY(posY);
						piezas_desordenadas[posY+1][posX] = aux;
						piezas_desordenadas[posY+1][posX].setPosY(posY+1);
						num_mov++;
						este_puzle.show();
					}
					else if(piezas_desordenadas[posY][posX-1].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY][posX-1];
						piezas_desordenadas[posY][posX].setPosX(posX);
						piezas_desordenadas[posY][posX-1] = aux;
						piezas_desordenadas[posY][posX-1].setPosX(posX-1);
						num_mov++;
						este_puzle.show();
					}
				}
			}
			if(posX == 0)
			{
				if(posY != filas-1)
				{
					if(piezas_desordenadas[posY+1][posX].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY+1][posX];
						piezas_desordenadas[posY][posX].setPosY(posY);
						piezas_desordenadas[posY+1][posX] = aux;
						piezas_desordenadas[posY+1][posX].setPosY(posY+1);
						num_mov++;
						este_puzle.show();
					}
					else if(piezas_desordenadas[posY][posX+1].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY][posX+1];
						piezas_desordenadas[posY][posX].setPosX(posX);
						piezas_desordenadas[posY][posX+1] = aux;
						piezas_desordenadas[posY][posX+1].setPosX(posX+1);
						num_mov++;
						este_puzle.show();
					}
					else if(piezas_desordenadas[posY-1][posX].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY-1][posX];
						piezas_desordenadas[posY][posX].setPosY(posY);
						piezas_desordenadas[posY-1][posX] = aux;
						piezas_desordenadas[posY-1][posX].setPosY(posY-1);
						num_mov++;
						este_puzle.show();
					}
				}
				else
				{
					if(piezas_desordenadas[posY][posX+1].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY][posX+1];
						piezas_desordenadas[posY][posX].setPosX(posX);
						piezas_desordenadas[posY][posX+1] = aux;
						piezas_desordenadas[posY][posX+1].setPosX(posX+1);
						num_mov++;
						este_puzle.show();
					}
					else if(piezas_desordenadas[posY-1][posX].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY-1][posX];
						piezas_desordenadas[posY][posX].setPosY(posY);
						piezas_desordenadas[posY-1][posX] = aux;
						piezas_desordenadas[posY-1][posX].setPosY(posY-1);
						num_mov++;
						este_puzle.show();
					}
				}	
			}
		}
		else if(posY>0 && posY<filas-1 && posX>0 && posX<columnas-1)
		{
			if(piezas_desordenadas[posY-1][posX].getUrl() == "blanco.jpg")
			{
				var aux = piezas_desordenadas[posY][posX];
				piezas_desordenadas[posY][posX] = piezas_desordenadas[posY-1][posX];
				piezas_desordenadas[posY][posX].setPosY(posY);
				piezas_desordenadas[posY-1][posX] = aux;
				piezas_desordenadas[posY-1][posX].setPosY(posY-1);
				num_mov++;
				este_puzle.show();
			}
			else if(piezas_desordenadas[posY][posX-1].getUrl() == "blanco.jpg")
			{
				var aux = piezas_desordenadas[posY][posX];
				piezas_desordenadas[posY][posX] = piezas_desordenadas[posY][posX-1];
				piezas_desordenadas[posY][posX].setPosX(posX);
				piezas_desordenadas[posY][posX-1] = aux;
				piezas_desordenadas[posY][posX-1].setPosX(posX-1);
				num_mov++;
				este_puzle.show();
			}
			else if(piezas_desordenadas[posY+1][posX].getUrl() == "blanco.jpg")
			{
				var aux = piezas_desordenadas[posY][posX];
				piezas_desordenadas[posY][posX] = piezas_desordenadas[posY+1][posX];
				piezas_desordenadas[posY][posX].setPosY(posY);
				piezas_desordenadas[posY+1][posX] = aux;
				piezas_desordenadas[posY+1][posX].setPosY(posY+1);
				num_mov++;
				este_puzle.show();
			}
			else if(piezas_desordenadas[posY][posX+1].getUrl() == "blanco.jpg")
			{
				var aux = piezas_desordenadas[posY][posX];
				piezas_desordenadas[posY][posX] = piezas_desordenadas[posY][posX+1];
				piezas_desordenadas[posY][posX].setPosX(posX);
				piezas_desordenadas[posY][posX+1] = aux;
				piezas_desordenadas[posY][posX+1].setPosX(posX+1);
				num_mov++;
				este_puzle.show();
			}
		}
		else if(posY == filas-1 || posX == columnas-1)
		{
			if(posY == filas-1 && posX == columnas-1)
			{
				if(piezas_desordenadas[posY-1][posX].getUrl() == "blanco.jpg")
				{
					var aux = piezas_desordenadas[posY][posX];
					piezas_desordenadas[posY][posX] = piezas_desordenadas[posY-1][posX];
					piezas_desordenadas[posY][posX].setPosY(posY);
					piezas_desordenadas[posY-1][posX] = aux;
					piezas_desordenadas[posY-1][posX].setPosY(posY-1);
					num_mov++;
					este_puzle.show();
				}
				else if(piezas_desordenadas[posY][posX-1].getUrl() == "blanco.jpg")
				{
					var aux = piezas_desordenadas[posY][posX];
					piezas_desordenadas[posY][posX] = piezas_desordenadas[posY][posX-1];
					piezas_desordenadas[posY][posX].setPosX(posX);
					piezas_desordenadas[posY][posX-1] = aux;
					piezas_desordenadas[posY][posX-1].setPosX(posX-1);
					num_mov++;
					este_puzle.show();
				}
			}
			if(posY == filas-1)
			{
				if(posX != 0)
				{
					if(piezas_desordenadas[posY-1][posX].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY-1][posX];
						piezas_desordenadas[posY][posX].setPosY(posY);
						piezas_desordenadas[posY-1][posX] = aux;
						piezas_desordenadas[posY-1][posX].setPosY(posY-1);
						num_mov++;
						este_puzle.show();
					}
					else if(piezas_desordenadas[posY][posX-1].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY][posX-1];
						piezas_desordenadas[posY][posX].setPosX(posX);
						piezas_desordenadas[posY][posX-1] = aux;
						piezas_desordenadas[posY][posX-1].setPosX(posX-1);
						num_mov++;
						este_puzle.show();
					}
					else if(piezas_desordenadas[posY][posX+1].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY][posX+1];
						piezas_desordenadas[posY][posX].setPosX(posX);
						piezas_desordenadas[posY][posX+1] = aux;
						piezas_desordenadas[posY][posX+1].setPosX(posX+1);
						num_mov++;
						este_puzle.show();
					}
				}
				else
				{
					if(piezas_desordenadas[posY-1][posX].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY-1][posX];
						piezas_desordenadas[posY][posX].setPosY(posY);
						piezas_desordenadas[posY-1][posX] = aux;
						piezas_desordenadas[posY-1][posX].setPosY(posY-1);
						num_mov++;
						este_puzle.show();
					}
					else if(piezas_desordenadas[posY][posX+1].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY][posX+1];
						piezas_desordenadas[posY][posX].setPosX(posX);
						piezas_desordenadas[posY][posX+1] = aux;
						piezas_desordenadas[posY][posX+1].setPosX(posX+1);
						num_mov++;
						este_puzle.show();
					}
				}
			}
			if(posX == columnas-1)
			{
				if(posY != 0)
				{
					if(piezas_desordenadas[posY-1][posX].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY-1][posX];
						piezas_desordenadas[posY][posX].setPosY(posY);
						piezas_desordenadas[posY-1][posX] = aux;
						piezas_desordenadas[posY-1][posX].setPosY(posY-1);
						num_mov++;
						este_puzle.show();
					}
					else if(piezas_desordenadas[posY][posX-1].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY][posX-1];
						piezas_desordenadas[posY][posX].setPosX(posX);
						piezas_desordenadas[posY][posX-1] = aux;
						piezas_desordenadas[posY][posX-1].setPosX(posX-1);
						num_mov++;
						este_puzle.show();
					}
					else if(piezas_desordenadas[posY+1][posX].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY+1][posX];
						piezas_desordenadas[posY][posX].setPosY(posY);
						piezas_desordenadas[posY+1][posX] = aux;
						piezas_desordenadas[posY+1][posX].setPosY(posY+1);
						num_mov++;
						este_puzle.show();
					}
				}
				else
				{
					if(piezas_desordenadas[posY][posX-1].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY][posX-1];
						piezas_desordenadas[posY][posX].setPosX(posX);
						piezas_desordenadas[posY][posX-1] = aux;
						piezas_desordenadas[posY][posX-1].setPosX(posX-1);
						num_mov++;
						este_puzle.show();
					}
					else if(piezas_desordenadas[posY+1][posX].getUrl() == "blanco.jpg")
					{
						var aux = piezas_desordenadas[posY][posX];
						piezas_desordenadas[posY][posX] = piezas_desordenadas[posY+1][posX];
						piezas_desordenadas[posY][posX].setPosY(posY);
						piezas_desordenadas[posY+1][posX] = aux;
						piezas_desordenadas[posY+1][posX].setPosY(posY+1);
						num_mov++;
						este_puzle.show();
					}
				}
			}
		}
		
	}
	
	este_puzle.showResult = function()
	{
		str_tabla = "<table border ='1px solid black'>";
		for (var i=0; i < y; i++)
		{
		  str_tabla += "<tr>";
		  for (var j=0; j < x; j++)
		  {
		  	var img = piezas_ordenadas[i][j].showResult();
			str_tabla += "<td>";
			str_tabla += img;
			str_tabla += "</td>";
		  }
		  str_tabla += "</tr>";
		}
		str_tabla += "</table>";
		document.getElementById(etiqueta).innerHTML = str_tabla;
	}
	
	este_puzle.time = function()
	{
		var time_act = new Date();
		var aux = time_act - time_in;
		var time  = new Date();
		time.setTime(aux); 
		var min = time.getMinutes();
		var sec = time.getSeconds();
	    if (min<10) {min="0"+min;} 
	    if (sec<10) {sec="0"+sec;}
		
		var result = min+":"+sec;
		return result;
	}
	
	este_puzle.comprobar = function()
	{
		finalizado = true;
		for (var i=0; i < piezas_ordenadas.length; i++)
		{
		  for (var j=0; j < piezas_ordenadas[i].length; j++)
		  {
			if(piezas_ordenadas[i][j].getUrl() != piezas_desordenadas[i][j].getUrl())
			{
				finalizado = false;
			}
		  };
		};
	}
	
	este_puzle.getMov = function()
	{
		return num_mov;
	}
	
	este_puzle.showPunt = function(etiqueta)
	{
		var str_tabla_pun = "<table border ='1px solid black'>";
		str_tabla_pun += "<tr><td>"+este_puzle.getMov()+"</td><td>"+este_puzle.time()+"</td></tr>";
		str_tabla_pun += "</table>";
		document.getElementById(etiqueta).innerHTML = str_tabla_pun;
		setTimeout("este_puzle.showPunt('puntuaciones')",1000);
		
	}
	
	este_puzle.show = function()
	{
		este_puzle.comprobar();
		if(finalizado == false)
		{
			este_puzle.showPunt("puntuaciones");
			str_tabla = "<table border ='1px solid black'>";
			for (var i=0; i < y; i++)
			{
			  str_tabla += "<tr>";
			  for (var j=0; j < x; j++)
			  {
			  	var img = piezas_desordenadas[i][j].show();
				str_tabla += "<td>";
				str_tabla += img;
				str_tabla += "</td>";
			  }
			  str_tabla += "</tr>";
			}
			str_tabla += "</table>";
			document.getElementById(etiqueta).innerHTML = str_tabla;
		}
		else
		{
			este_puzle.showResult();
		}
	}
}

var pi = new Array();
var c = 0;
for (var i=0; i < 4; i++)
{
	pi[i] = new Array();
  for (var j=0; j < 4; j++) 
  {
  	if(i==3 && j==3)
  	{
  		var url = "blanco.jpg";
	    pi[i][j] = new pieza(url,j,i,100,100);
  	}
  	else
  	{
  		var url = c+".jpg";
	    pi[i][j] = new pieza(url,j,i,100,100);
	    c++;
  	}
  	
  };
};

var puzle = new puzle("puzle",pi,4,4);
puzle.desordenar();
var buttonstate=0;
function comprobarSol(element)
{
	buttonstate= 1 - buttonstate;
	if(buttonstate)
  	{
		puzle.showResult();
	}
	else
	{
		puzle.show();
	}
}

window.onload = puzle.show;