var radius = 45;
var circumference = 2 * Math.PI * radius;

document.writeln("The circumference is "+circumference+".</br>");

var area = Math.PI * (radius * radius);

document.writeln("The area is "+area+".");
