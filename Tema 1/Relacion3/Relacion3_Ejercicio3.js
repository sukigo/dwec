function calculateSupply (age, snack) {
  var max_age = 85;
  var consumed = ((max_age - age) * 365) * snack;
  
  var result = Math.round(consumed);
  document.writeln("You will need "+result+" to last you until the ripe old age of "+max_age+" .</br>");
}

var a = calculateSupply(22,1.5);
var b = calculateSupply(25,2);
var c = calculateSupply(14,6.3);
