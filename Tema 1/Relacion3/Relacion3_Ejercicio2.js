function calculateAge (birth_year, current_year) {
  var age = current_year - birth_year;
  var age2 = age - 1;
  
  document.writeln("You are either "+age+" or "+age2+".</br>");
}

var result1 = calculateAge(1992,2015);
var result2 = calculateAge(1995,2015);
var result3 = calculateAge(1999,2015);

function calculateAge2 (birth_year) {
  var currentTime = new Date();
  var current_year = currentTime.getFullYear();
  var age = current_year - birth_year;
  var age2 = age - 1;
  
  document.writeln("You are either "+age+" or "+age2+".</br>");
}

var bonus1 = calculateAge2(1992);
var bonus2 = calculateAge2(1995);
var bonus3 = calculateAge2(1999);

