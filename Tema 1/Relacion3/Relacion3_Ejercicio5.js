function celsiusToFahrenheit (celsius) {
  var result = ((celsius * 9) / 5) + 32;
  
  document.writeln(celsius+"&deg;C is "+result+"&deg;F. </br>");
}

function fahrenheitToCelsius (fahrenheit) {
  var result = ((fahrenheit - 32) * 5) / 9 ;

  document.writeln(fahrenheit+"&deg;F is "+result+"&deg;C.");
}

var a = celsiusToFahrenheit(26);
var b = fahrenheitToCelsius(98.6);
