function tellFortune (children, partner, location, job) {
  document.writeln("You will be a "+job+" in "+location+", and married to "+partner+" with "+children+" kids.</br>");
}

var a = tellFortune(2,"Marta","Granada","Fisher");
var b = tellFortune(3,"Lucia","Granada","Chef");
var c = tellFortune(5,"Graciela","Granada","Programmer");

