function helloWorld (language) {
  if(language == "es")
  {
  		return "Hola mundo!";
  }
  else if (language == "en")
  	   {
  			return "Hello world!";
  	   }
  	   else if (language == "de")
  	   {
  	   		return "Hallo welt!";
  	   }
}

var a = helloWorld("es");
var b = helloWorld("de");
var c = helloWorld("en");

document.write(a+"</br>");
document.write(b+"</br>");
document.write(c+"</br>");
