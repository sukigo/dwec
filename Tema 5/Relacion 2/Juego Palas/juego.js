/*Objeto Pala*/

function pala(apariencia,X,Y,width,height)
{
	esta_pala = this;
	esta_pala.pala = document.createElementNS("http://www.w3.org/2000/svg","rect");
	esta_pala.apariencia = apariencia;
	esta_pala.x = X;
	esta_pala.y = Y;
	esta_pala.height = height;
	esta_pala.width = width;
	
	esta_pala.draw = function(svg)
	{
		this.pala.setAttribute("x",this.x);
		this.pala.setAttribute("y",this.y);
		this.pala.setAttribute("rx",10);
		this.pala.setAttribute("ry",10);
		this.pala.setAttribute("width",this.width);
		this.pala.setAttribute("height",this.height);
		this.pala.setAttribute("fill",this.apariencia);
		
		svg.appendChild(this.pala);
	}
	
	esta_pala.moverUp = function(svg)
	{
		var height = svg.getAttribute("height");
		if(this.y >= 0)
		{
			this.y = this.y-5;
		}
		
	}
	
	esta_pala.moverDown = function(svg)
	{
		var height = svg.getAttribute("height");
		if(this.y<=(height - this.height))
		{
			this.y = this.y+5;
		}
	}
}

/*Objeto Bola*/

function bola(apariencia,radio,X,Y,velX,velY)
{
	esta_bola = this;
	this.bola = document.createElementNS("http://www.w3.org/2000/svg","circle");
	this.apariencia = apariencia;
	this.radio = radio;
	this.x = X;
	this.y = Y;
	this.velX = velX;
	this.velY = velY;
	
	esta_bola.getX = function()
	{
		return this.x;
	}
	
	esta_bola.getY = function()
	{
		return this.y;
	}
	
	esta_bola.getRadio = function()
	{
		return this.radio;
	}
	
	esta_bola.mover = function(svg)
	{
		var height = svg.getAttribute("height");
		var width = svg.getAttribute("width");
		if(this.x>=(width-(this.radio+1)))
		{
			this.velX = -this.velX;
		}
		
		if(this.x<= this.radio)
		{
			this.velX = -this.velX;
		}
		
		if(this.y>=(height-(this.radio+1)))
		{
			this.velY = -this.velY;
		}
		
		if(this.y<= this.radio)
		{
			this.velY = -this.velY;
		}
		
		this.x = this.x+this.velX;
		this.y = this.y+this.velY;
		
	}
	
	esta_bola.cambiarVel= function()
	{
			this.velX = -this.velX;
			this.velY = -this.velY;
	}
	
	esta_bola.draw = function(svg)
	{
		this.bola.setAttribute("r",this.radio);
		this.bola.setAttribute("cx",this.x);
		this.bola.setAttribute("cy",this.y);
		this.bola.setAttribute("stroke","black");
		this.bola.setAttribute("fill",this.apariencia);
		svg.appendChild(this.bola);
	}
	
	esta_bola.collision = function(bola2)
	{
		if(Math.sqrt( ( bola2.getX()-this.x ) * ( bola2.getX()-this.x )  + ( bola2.getY()-this.y ) * ( bola2.getY()-this.y ) ) < ( this.radio + bola2.getRadio()))
		{
			return true;
		}
	}
}

/*Objeto BolaAleatoria*/

function bolaAleatoria(apariencia)
{
	this.bola = document.createElementNS("http://www.w3.org/2000/svg","circle");
	this.apariencia = apariencia;
	this.radio = Math.floor(Math.random()*50+10);
	this.x = Math.floor(Math.random()*500+this.radio);
	this.y = Math.floor(Math.random()*500+this.radio);
	this.velX = Math.floor(Math.random()*10+1);
	this.velY = Math.floor(Math.random()*10+1);
}

bolaAleatoria.prototype = new bola();
bolaAleatoria.prototype.constructor = bolaAleatoria;

/*Objeto Juego*/

function juego(etiqueta,fps)
{
	este_juego = this;
	var etiqueta = etiqueta;
	var FPS = fps;
	var up = 38;
	var down = 40;
	var bolas = new Array();
	var palas = new Array();
	var intervalo;
	var bolas_col = new Array();
	var svg = document.createElementNS("http://www.w3.org/2000/svg","svg");
	var key = [];
	
	este_juego.start = function()
	{
		document.addEventListener("keydown",function(evt)
		{
			key[evt.keyCode] = true;
		});
		
		document.addEventListener("keyup",function(evt)
		{
			delete key[evt.keyCode];
		});
		
		if(intervalo == undefined)
		{
			svg.setAttribute("height",600);
			svg.setAttribute("width",1320);
			svg.setAttribute("style","border: 1px solid black;");
			document.getElementById(etiqueta).appendChild(svg);
			intervalo = setInterval(function()
			{
				bolas_col = new Array();
				for (var i=0; i < bolas.length; i++)
				{
				  	bolas[i].mover(svg);
				}
				
				for (var i=0; i < bolas.length; i++)
				{
				  	for (var x=0 ; x<bolas.length;x++)
					{
						if(x!=i)
						{
							if(bolas[i].collision(bolas[x]))
							{
								bolas_col[i] = bolas[i];
								bolas_col[x] = bolas[x];
							}
						}
						
					}
				}
				
				bolas_col.forEach(function(element, index, array) {element.cambiarVel();});
				
				if(key[up])
				{
					palas[0].moverUp(svg);
				}
				
				if(key[down])
				{
					palas[0].moverDown(svg);
				}
				
				for (var i=0; i < palas.length; i++)
				{
				  	palas[i].draw(svg);
				}
				
				for (var i=0; i < bolas.length; i++)
				{
				  	bolas[i].draw(svg);
				}
			}
			,1000/FPS);
		}
			
	}
	
	este_juego.addBola = function(apariencia,r,x,y,velx,vely)
	{
		bolas.push(new bola(apariencia,r,x,y,velx,vely));
	}
	
	este_juego.addPala = function(apariencia,x,y,width,height)
	{
		palas.push(new pala(apariencia,x,y,width,height));
	}
	
	este_juego.addBolaAleatoria = function(apariencia)
	{
		var bola = new bolaAleatoria(apariencia);
		bolas.push(bola);
	}
	
	este_juego.stop = function()
	{
		clearInterval(intervalo);
		intervalo = undefined;
	}
	
}

var juego = new juego("juego",60);
juego.addPala("black",0,250,20,100);
juego.addBolaAleatoria("red");

