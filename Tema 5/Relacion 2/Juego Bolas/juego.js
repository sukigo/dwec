/*Objeto Bola*/

function bola(apariencia,radio,X,Y,velX,velY)
{
	esta_bola = this;
	this.bola = document.createElementNS("http://www.w3.org/2000/svg","circle");
	this.apariencia = apariencia;
	this.radio = radio;
	this.x = X;
	this.y = Y;
	this.velX = velX;
	this.velY = velY;
	
	esta_bola.getX = function()
	{
		return this.x;
	}
	
	esta_bola.getY = function()
	{
		return this.y;
	}
	
	esta_bola.getVX = function()
	{
		return this.velX;
	}
	
	esta_bola.getVY = function()
	{
		return this.velY;
	}
	
	esta_bola.getRadio = function()
	{
		return this.radio;
	}
	
	esta_bola.mover = function(svg)
	{
		var height = svg.getAttribute("height");
		var width = svg.getAttribute("width");
		if(this.x>=(width-(this.radio+1)))
		{
			this.velX = -this.velX;
		}
		
		if(this.x<= this.radio)
		{
			this.velX = -this.velX;
		}
		
		if(this.y>=(height-(this.radio+1)))
		{
			this.velY = -this.velY;
		}
		
		if(this.y<= this.radio)
		{
			this.velY = -this.velY;
		}
		
		this.x = this.x+this.velX;
		this.y = this.y+this.velY;
		
	}
	
	esta_bola.cambiarVel= function(vx,vy,r)
	{
			this.velX = (this.velX*(this.radio-r)+(2*r*vx))/(this.radio+r);
			this.velY = (this.velY*(this.radio-r)+(2*r*vy))/(this.radio+r);
	}
	
	esta_bola.draw = function(svg)
	{
		this.bola.setAttribute("r",this.radio);
		this.bola.setAttribute("cx",this.x);
		this.bola.setAttribute("cy",this.y);
		this.bola.setAttribute("stroke","black");
		this.bola.setAttribute("fill",this.apariencia);
		svg.appendChild(this.bola);
	}
	
	esta_bola.collision = function(bola2)
	{
		if(Math.sqrt( ( bola2.getX()-this.x ) * ( bola2.getX()-this.x )  + ( bola2.getY()-this.y ) * ( bola2.getY()-this.y ) ) < ( this.radio + bola2.getRadio()))
		{
			return true;
		}
	}
}

/*Objeto BolaAleatoria*/

function bolaAleatoria(apariencia)
{
	this.bola = document.createElementNS("http://www.w3.org/2000/svg","circle");
	this.apariencia = apariencia;
	this.radio = Math.floor(Math.random()*50+10);
	this.x = Math.floor(Math.random()*500+this.radio);
	this.y = Math.floor(Math.random()*500+this.radio);
	this.velX = Math.floor(Math.random()*10+1);
	this.velY = Math.floor(Math.random()*10+1);
}

bolaAleatoria.prototype = new bola();
bolaAleatoria.prototype.constructor = bolaAleatoria;

/*Objeto Juego*/

function juego(etiqueta,fps)
{
	este_juego = this;
	var etiqueta = etiqueta;
	var FPS = fps;
	var bolas = new Array();
	var intervalo;
	var bolas_col = new Array();
	var svg = document.createElementNS("http://www.w3.org/2000/svg","svg");
	
	este_juego.start = function()
	{
		if(intervalo == undefined)
		{
			svg.setAttribute("height",600);
			svg.setAttribute("width",600);
			svg.setAttribute("style","border: 1px solid black;");
			document.getElementById(etiqueta).appendChild(svg);
			intervalo = setInterval(function()
			{
				bolas_col = new Array();
				for (var i=0; i < bolas.length; i++)
				{
				  	bolas[i].mover(svg);
				}
				
				for (var i=0; i < bolas.length; i++)
				{
				  	for (var x=0 ; x<bolas.length;x++)
					{
						if(x!=i)
						{
							if(bolas[i].collision(bolas[x]))
							{
								bolas_col[i] = new Array(bolas[i],bolas[x].getVX(),bolas[x].getVY(),bolas[x].getRadio())
								bolas_col[x] = new Array(bolas[x],bolas[i].getVX(),bolas[i].getVY(),bolas[i].getRadio())
							}
						}
						
					}
				}
				
				bolas_col.forEach(
					function(element, index, array)
					{
						element[0].cambiarVel(element[1],element[2],element[3]);
					}
				);
				
				for (var i=0; i < bolas.length; i++)
				{
				  	bolas[i].draw(svg);
				}
			}
			,1000/FPS);
		}
			
	}
	
	este_juego.addBola = function(apariencia,r,x,y,velx,vely)
	{
		bolas.push(new bola(apariencia,r,x,y,velx,vely));
	}
	
	este_juego.addBolaAleatoria = function(apariencia)
	{
		var bola = new bolaAleatoria(apariencia);
		bolas.push(bola);
	}
	
	este_juego.stop = function()
	{
		clearInterval(intervalo);
		intervalo = undefined;
	}
}

var juego = new juego("juego",60);
juego.addBolaAleatoria("red");
juego.addBolaAleatoria("blue");
juego.addBolaAleatoria("bronw");

