function controlador()
{
	var modelo = new bd();
	var datos = modelo.listar();
	var vi = new vista("lista",datos);
	
	this.init = function()
	{
		vi.iniciar();
	}
	
	this.insert = function()
	{
		var txt = document.getElementById("text_field").value;
		modelo.add(txt);
		datos = modelo.listar();
		vi.setDatos(datos);
		vi.iniciar();
	}
	
}

var control= new controlador();
window.onload = control.init;