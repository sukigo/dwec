function vista(etiqueta,datos)
{
	this.datos = datos;
	this.elementoPadre = document.getElementById(etiqueta);
	
	this.setDatos = function(datos)
	{
		this.datos = datos
	}
	
	this.iniciar = function()
	{
		if(this.datos.length != 0)
		{
			this.elementoPadre.innerHTML = "";
			for (var i=0; i < this.datos.length; i++)
			{
			  	var item = document.createElement("div");
				item.style.backgroundColor = "gray";
				item.style.width = "50px";
				item.style.margin = "10px";
				var texto = document.createTextNode(this.datos[i]);
				item.appendChild(texto);
				this.elementoPadre.appendChild(item);
			}
			
		}
			
	}
}
