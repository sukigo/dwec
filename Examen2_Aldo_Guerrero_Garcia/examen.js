//Ejercicio 2

function creaCookies(valor)
{
	var d = new Date();
	d.setTime(d.getTime() + (5*60*1000));
	var expires = "expires="+d.toUTCString();
	document.cookie = "examen=" + valor + ";"+expires;
}

function valor()
{
	 var ca = document.cookie.split(';');
	 for(var i=0; i<ca.length; i++)
	 {
        var c = ca[i];
        
     	while (c.charAt(0)==' ')
     	{
     		c = c.substring(1);
     	} 
     	
        if (c.indexOf("examen=") == 0)
        {
        	alert(c.substring("examen=".length, c.length));
        }
        else
        {
        	alert("No existe esa cookie.");
        }
     }
}

function borrarCookie()
{
	var d = new Date();
	d.setTime(d.getTime());
	var expires = "expires="+d.toUTCString();
	document.cookie = "examen=0;"+expires;
}

creaCookies("10");

//Ejercicio 3

function creaImagen(etiqueta, url)
{
	var padre = document.getElementById(etiqueta);
	var img = document.createElement("img");
	img.setAttribute("src",url);
	padre.appendChild(img);
}

function lista(array)
{
	var lista = document.createElement("ul");
	for (var i=0; i < array.length; i++)
	{
		var item = document.createElement("li");
		if (i%2 == 0)
		{
			item.setAttribute("id","pares");
		}
		else
		{
			item.setAttribute("id","impares");
		}
		var texto = document.createTextNode(array[i]);
		item.appendChild(texto);
		lista.appendChild(item);
	}
	
	return lista;
}

function aplicaEstilos(id)
{
	var listaPadre = document.getElementById(id);
	if(listaPadre != null)
	{
		var lis = listaPadre.getElementsByTagName("li");
		
		for (var i=0; i < lis.length; i++)
		{
			var id = lis[i].getAttribute("id");
			
			if(id == "pares")
			{
				lis[i].style.backgroundColor = "blue";
			}
			else if(id == "impares")
			{
				lis[i].style.backgroundColor = "green";
			}
		}
	}
}

function creaRectangulo(etiqueta)
{
	var padre = document.getElementById(etiqueta);
	var svg = document.createElementNS("http://www.w3.org/2000/svg","svg");
	var rect = document.createElementNS("http://www.w3.org/2000/svg","rect");
	rect.setAttribute("id","rectangulo");
	rect.setAttribute("fill","red");
	rect.setAttribute("height","50px");
	rect.setAttribute("width","50px");
	svg.appendChild(rect);
	padre.appendChild(svg);
}

function animaRectangulo()
{
	var rect = document.getElementById("rectangulo");
	var width = 50;
	setInterval(function()
	{
		if(width < 350)
		{
			width ++;
			rect.setAttribute("width",width+"px");
		}
	},1000/60);
}

function chart(etiqueta)
{
	var datos = new Array();
		datos['PP'] = 40;
		datos['PSOE'] = 25;
		datos['Izquierda'] = 16;
		datos['Ciudadanos'] = 4;
		datos['Podemos'] = 15;
		
      google.load('visualization', '1.0', {'packages':['corechart']});

      google.setOnLoadCallback(drawChart);

      function drawChart() 
      {
      	
      	var data = google.visualization.arrayToDataTable([
	        ['Año', 'Población', { role: 'style' } ],
	        ['2012', 3452, 'color: red'],
	        ['2013', 3600, 'color: blue; opacity: 0.2'],
	        ['2014', 4021, 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF;opacity: 0.5'],
	        ['2015', 3800, 'stroke-color: #871B47; stroke-opacity: 0.6; stroke-width: 8; fill-color: #BC5679; opacity: 0.8']
	      ]);

        var options = {'title':'Evolución de la población de mi pueblo',
                       'width':500,
                       'height':300,
                       backgroundColor: 'gray',
                       'is3D':true,
                       animation:{
				        duration: 2000,
				        easing: 'inAndOut',
				        startup: true
      				   },
			};

        var chart2 = new google.visualization.BarChart(document.getElementById(etiqueta));
        chart2.draw(data, options);
      }
}

creaImagen("img","img.jpg");

var arr = new Array("hola","cara","cola");
var ul = lista(arr);
ul.setAttribute("id","lista1");
document.getElementById("lista").appendChild(ul);
aplicaEstilos("lista1");

creaRectangulo("svg");

chart("chart");


