﻿var app = angular.module('E-Ducar', ['ngRoute','ngAnimate']);

//Configuracion de los controladores
app.controller('principal', function($scope, $http)
{
	$('#index-banner').css('display','block');
	
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
	
});

app.controller('resultados', function($scope, $http)
{
	$('#index-banner').css('display','none');
	
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
	
	$http.get("https://fct2016daw.no-ip.org/Proyecto/islogin.php?login=true").then(function (response)
  	{
  		if(!response.data.user)
  		{
  			window.location="https://fct2016daw.no-ip.org/Proyecto/";
  		}
  	});
  	
  	
  	$http.get("https://fct2016daw.no-ip.org/Proyecto/checkear_examen.php?mostrar=true").then(function (response)
  	{
  		$scope.resultados = response.data;
  		
  		Highcharts.chart('container', {
		      chart: {
		        type: 'pie'
		      },
		      title: {
		        text: 'Resultados'
		      },
		
		      series: [{
		        data:[{
		                        name: "Correctas",
		                        y: $scope.resultados.correctas,
		                        color: '#01DF01'
		                    }, {
		                        name: "Incorrectas",
		                        y: $scope.resultados.incorrectas,
		                        color: '#FF0000'
		                }]
		      }]
	    });
  	});
  	
});

app.controller('gestionar_cursos_detalle', ['$scope','$http','$routeParams',function($scope, $http, $routeParams)
{
	$http.get("https://fct2016daw.no-ip.org/Proyecto/islogin.php?login=true").then(function (response)
  	{
  		if(!response.data.user)
  		{
  			window.location="https://fct2016daw.no-ip.org/Proyecto/";
  		}
  	});
  	
  	$http.get("https://fct2016daw.no-ip.org/Proyecto/listaCursoDetalle.php?idcurso="+$routeParams.idCurso).then(function (response)
  	{
  	   	$scope.curso = response.data;
  	   	$scope.temas = response.data.tema; 
  	   	 	   	
  	   	if($scope.temas != null)
		{
  			$scope.hayTemas = false;
  		}
  		else
  		{
  			$scope.hayTemas = true;
  		}
  	});
  	
  	$scope.addTema = function($event)
  	{
  		window.location="https://fct2016daw.no-ip.org/Proyecto/#/crear_temas_curso/"+$event.target.id;
  	}
	
	$scope.checked = function($event)
	{
		var checkbox = $('#temas .checkbox');
		var c = 0;
		var action = "";
		
		if($event.currentTarget.previousElementSibling.checked)
		{
			action = "desactivar"
		}
		else
		{
			action = "activar"
		}
		
		
		$.each(checkbox,function(index,value)
		{
			if($(this).is( ":checked" ))
			{
				c++;
			}
		});
		
		if(c > 0)
		{
			if(c == 1 && action == "activar")
			{
				$('#borrar_temas').css("background-color","");
				$('#borrar_temas').removeAttr("disabled");
			}
			else if(c == 1 && action == "desactivar")
			{
				$('#borrar_temas').css("background-color","#A9BCF5");
				$('#borrar_temas').attr("disabled","disabled");
			}
			else
			{
				$('#borrar_temas').css("background-color","");
				$('#borrar_temas').removeAttr("disabled");
			}
			
		}
		else
		{
			if(c == 0 && action == "activar")
			{
				$('#borrar_temas').css("background-color","");
				$('#borrar_temas').removeAttr("disabled");
			}
			else
			{
				$('#borrar_temas').css("background-color","#A9BCF5");
				$('#borrar_temas').attr("disabled","disabled");
			}
		}
		
		if(action == "desactivar")
		{
			$event.currentTarget.previousElementSibling.removeAttr("checked");
		}
		else
		{
			$event.currentTarget.previousElementSibling.attr('checked','checked');
		}
	}
	
	$('#cambiar_datos').on('click', function()
	{
		$('#modal_datos').css('display','block');
		$('#modal_datos').css('width','65%');
		$('#modal_datos').css('max-height','80%');
		$('#modal_datos').css('min-height','235px');
		$('#modal_datos').css('background-color','rgb(234, 234, 234);');
		$('#modal_datos p').css('text-align','center');
		$('#modal_datos').css('border-radius','5px');
		$('<div class="modal-backdrop" style="opacity: 0.5 !important;"></div>').appendTo(document.body);
	});

	$('#cancelar_datos').on('click', function()
	{
		$('#modal_datos').css('display','none');
		$(".modal-backdrop").remove();
	});
	$('#cambiar_imagen').on('click', function()
	{
		$('#modal_imagen').css('display','block');
		$('#modal_imagen').css('max-height','265px');
		$('#modal_imagen').css('min-height','235px');
		$('#modal_imagen').css('background-color','rgb(234, 234, 234);');
		$('#modal_imagen p').css('text-align','center');
		$('#modal_imagen').css('border-radius','5px');
		$('<div class="modal-backdrop" style="opacity: 0.5 !important;"></div>').appendTo(document.body);
	});

	$('#cancelar_imagen').on('click', function()
	{
		$('#modal_imagen').css('display','none');
		$(".modal-backdrop").remove();
	});
	
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove();
		},4000);

	}
	
}]);

app.controller('cursos', function($scope, $http)
{
	$('#index-banner').css('display','none');
	
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
	
	$http.get("https://fct2016daw.no-ip.org/Proyecto/listaTodosCursos.php").then(function (response)
  	{
  		$scope.cursos = response.data;
  		
  		if(!Array.isArray($scope.cursos))
  		{
  			$scope.noCursos = true;
  		}
  	});
	
});

app.controller('gestionar_cursos', function($scope, $http)
{
	$scope.desplegar = function($event)
	{
		if(!$($event.currentTarget).hasClass('active'))
		{
			if(!$($event.currentTarget).hasClass('activado'))
  			{
  				if($($event.currentTarget).hasClass('creados'))
  				{
  					$($event.currentTarget).addClass('activado');
	  				$($event.currentTarget).parent().append('<div hidden="true" href="javascrip:void(0);" class="list-group-item row"><input type="button" class="col l5 s12 gestionar" id="gestionar" name="gestionar_recursos" value="Gestionar Recursos" style="text-align: center;margin-top: 10px;"> <input type="button" class="col offset-l2 l5 s12 borrar" id="borrar" name="borrar" value="Borrar Curso" style="text-align: center;margin-top: 10px;"></div>');
	  				$($event.currentTarget).next().show('slow');
	  				
	  				$('.gestionar').on('click',function(id)
					{
						if(id != null)
						{
							window.location="https://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos/"+id.target.parentNode.parentNode.childNodes[1].id;
						}
					});
					
					$('.borrar').on('click',function(id)
					{
						if(id != null)
						{
							window.location="https://fct2016daw.no-ip.org/Proyecto/borrarCurso.php?idcurso="+id.target.parentNode.parentNode.childNodes[1].id;
						}
						
					});
  				}
  				else if($($event.currentTarget).hasClass('inscritos'))
  				{
  					$($event.currentTarget).addClass('activado');
	  				$($event.currentTarget).parent().append('<div hidden="true" href="javascrip:void(0);" class="list-group-item row"><input type="button" class="col l5 s12 ir_curso" id="ir_curso" name="ir_curso" value="Ir al curso" style="text-align: center;margin-top: 10px;"> <input type="button" class="col offset-l2 l5 s12 baja" id="baja" name="baja" value="Darse de baja" style="text-align: center;margin-top: 10px;"></div>');
	  				$($event.currentTarget).next().show('slow');
					
	  				$('.baja').on('click',function(id)
					{
						if(id != null)
						{
							window.location="https://fct2016daw.no-ip.org/Proyecto/borrarInscripcion.php?idcurso="+id.target.parentNode.parentNode.childNodes[1].id;
						}
						
					});

					$('.ir_curso').on('click',function(id)
					{
						if(id != null)
						{
							window.location="https://fct2016daw.no-ip.org/Proyecto/#/cursos/"+id.target.parentNode.parentNode.childNodes[1].id;
						}
					});
  				}	
  			}
  			else
  			{
  				$($event.currentTarget).removeClass('activado');
  				$($event.currentTarget).next().hide('slow');
  				setTimeout(function(){
  					$($event.currentTarget).next().remove();
  				},3000);
  			}
		}
	}
	
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
	
	$http.get("https://fct2016daw.no-ip.org/Proyecto/listaCursosCreados.php").then(function (response)
  	{
  		$scope.cursos = response.data;
  		if(Array.isArray($scope.cursos))
  		{
  			$scope.hayCursos = false;
  		}
  		else
  		{
  			$scope.cursos = null; 
  			$scope.hayCursos = true;
  		}
  	});
  	
  	$http.get("https://fct2016daw.no-ip.org/Proyecto/listaCursoInscrito.php").then(function (response)
  	{
  		$scope.cursos_inscritos = response.data;
  		if(Array.isArray($scope.cursos_inscritos))
  		{
  			$scope.hayCursosInscritos = false;
  		}
  		else
  		{
  			$scope.cursos_incritos = null; 
  			$scope.hayCursosInscritos = true;
  		}
  	});
  	
	$('#index-banner').css('display','none');
	
});

app.controller('gestionar_temas_libres', function($scope, $http)
{
	$scope.desplegar = function($event)
	{
		if(!$($event.currentTarget).hasClass('active'))
		{
			if(!$($event.currentTarget).hasClass('activado'))
  			{
  				if($($event.currentTarget).hasClass('creados'))
  				{
  					$($event.currentTarget).addClass('activado');
	  				$($event.currentTarget).parent().append('<div hidden="true" href="javascrip:void(0);" class="list-group-item row"><input type="button" class="col l5 s12 gestionar" id="gestionar" name="gestionar_recursos" value="Gestionar Recursos" style="text-align: center;margin-top: 10px;"> <input type="button" class="col offset-l2 l5 s12 borrar" id="borrar" name="borrar" value="Borrar Tema" style="text-align: center;margin-top: 10px;"></div>');
	  				$($event.currentTarget).next().show('slow');
	  				
	  				$('.gestionar').on('click',function(id)
					{
						if(id != null)
						{
							window.location="https://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres/"+id.target.parentNode.parentNode.childNodes[1].id;
						}
					});
					
					$('.borrar').on('click',function(id)
					{
						if(id != null)
						{
							window.location="https://fct2016daw.no-ip.org/Proyecto/borra_tema.php?idtema="+id.target.parentNode.parentNode.childNodes[1].id;
						}
						
					});
  				}
  				else if($($event.currentTarget).hasClass('inscritos'))
  				{
  					$($event.currentTarget).addClass('activado');
	  				$($event.currentTarget).parent().append('<div hidden="true" href="javascrip:void(0);" class="list-group-item row"><input type="button" class="col l5 s12 ir_tema" id="ir_tema" name="ir_tema" value="Ir al tema" style="text-align: center;margin-top: 10px;"> <input type="button" class="col offset-l2 l5 s12 baja" id="baja" name="baja" value="Darse de baja" style="text-align: center;margin-top: 10px;"></div>');
	  				$($event.currentTarget).next().show('slow');
	  				
	  				$('.baja').on('click',function(id)
					{
						if(id != null)
						{
							window.location="https://fct2016daw.no-ip.org/Proyecto/borrarInscripcion.php?idtema="+id.target.parentNode.parentNode.childNodes[1].id;
						}
						
					});
					
					$('.ir_tema').on('click',function(id)
					{
						if(id != null)
						{
							window.location="https://fct2016daw.no-ip.org/Proyecto/#/temas/"+id.target.parentNode.parentNode.childNodes[1].id;
						}
					});
  				}	
  			}
  			else
  			{
  				$($event.currentTarget).removeClass('activado');
  				$($event.currentTarget).next().hide('slow');
  				setTimeout(function(){
  					$($event.currentTarget).next().remove();
  				},3000);
  			}
		}
	}
	
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
	
	$http.get("https://fct2016daw.no-ip.org/Proyecto/islogin.php?login=true").then(function (response)
  	{
  		if(!response.data.user)
  		{
  			window.location="https://fct2016daw.no-ip.org/Proyecto/";
  		}
  		
  		$http.get("https://fct2016daw.no-ip.org/Proyecto/listaTemas.php").then(function (response)
	  	{
	  		$scope.temas = response.data;
	  		if(Array.isArray($scope.temas))
	  		{
	  			$scope.hayTemas = false;
	  		}
	  		else
	  		{
	  			$scope.temas = null;
	  			$scope.hayTemas = true;
	  		}
	  	});
	  	
	  	$http.get("https://fct2016daw.no-ip.org/Proyecto/listaTemaInscrito.php").then(function (response)
	  	{
	  		$scope.temas_inscritos = response.data;
	  		if(Array.isArray($scope.temas_inscritos))
	  		{
	  			$scope.hayTemasInscritos = false;
	  		}
	  		else
	  		{
	  			$scope.temas_incritos = null; 
	  			$scope.hayTemasInscritos = true;
	  		}
	  	});
  	});
  	
	$('#index-banner').css('display','none');
});

app.controller('crear_temas_libres', ['$scope','$http','$routeParams',function($scope, $http, $routeParams)
{
	if($routeParams.curso)
	{
		$scope.curso = true;
		$scope.idcurso = $routeParams.curso;
	}
	else
	{
		$scope.curso = false;
	}
	
	$http.get("https://fct2016daw.no-ip.org/Proyecto/islogin.php?login=true").then(function (response)
  	{
  		if(!response.data.user && response.data.tipo != 2)
  		{
  			window.location="https://fct2016daw.no-ip.org/Proyecto/";
  		}
  	});
  	
  	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
  	
	$('#index-banner').css('display','none');
}]);

app.controller('equipo', function($scope, $http)
{
	$('#index-banner').css('display','none');
});

app.controller('crear_cursos', function($scope, $http)
{
	$('#index-banner').css('display','none');
	
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
	
});

app.controller('contacto', function($scope, $http)
{
	$('#index-banner').css('display','none');
	
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
});

app.controller('lista_tema', function($scope, $http)
{
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
	
	$('#index-banner').css('display','none');
	
	$http.get("https://fct2016daw.no-ip.org/Proyecto/listaTodosTemas.php").then(function (response)
  	{
  		$scope.temas = response.data;
  		if(!Array.isArray($scope.temas))
  		{
  			$scope.noTemas = true;
  		}
  	});
});

app.controller('user_panel', function($scope, $http)
{
	$('#index-banner').css('display','none');
	$http.get("https://fct2016daw.no-ip.org/Proyecto/islogin.php?login=true").then(function (response)
  	{
  		if(!response.data.user)
  		{
  			window.location="https://fct2016daw.no-ip.org/Proyecto/";
  		}
  	});
  	
  	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
	
	$('#cambiar_clave').on('click', function()
	{
		$('#modal_clave').css('display','block');
		$('#modal_clave').css('max-height','340px');
		$('#modal_clave').css('min-height','265px');
		$('#modal_clave').css('background-color','rgb(234, 234, 234);');
		$('#modal_clave p').css('text-align','center');
		$('#modal_clave').css('border-radius','5px');
		$('<div class="modal-backdrop" style="opacity: 0.5 !important;"></div>').appendTo(document.body);
	});

	$('#cancelar_clave').on('click', function()
	{
		$('#modal_clave').css('display','none');
		$(".modal-backdrop").remove();
	});
	$('#cambiar_datos').on('click', function()
	{
		$('#modal_datos').css('display','block');
		$('#modal_datos').css('max-height','290px');
		$('#modal_datos').css('min-height','235px');
		$('#modal_datos').css('background-color','rgb(234, 234, 234);');
		$('#modal_datos p').css('text-align','center');
		$('#modal_datos').css('border-radius','5px');
		$('<div class="modal-backdrop" style="opacity: 0.5 !important;"></div>').appendTo(document.body);
	});

	$('#cancelar_datos').on('click', function()
	{
		$('#modal_datos').css('display','none');
		$(".modal-backdrop").remove();
	});
	$('#cambiar_imagen').on('click', function()
	{
		$('#modal_imagen').css('display','block');
		$('#modal_imagen').css('max-height','265px');
		$('#modal_imagen').css('min-height','235px');
		$('#modal_imagen').css('background-color','rgb(234, 234, 234);');
		$('#modal_imagen p').css('text-align','center');
		$('#modal_imagen').css('border-radius','5px');
		$('<div class="modal-backdrop" style="opacity: 0.5 !important;"></div>').appendTo(document.body);
	});

	$('#cancelar_imagen').on('click', function()
	{
		$('#modal_imagen').css('display','none');
		$(".modal-backdrop").remove();
	});
	
	
});

app.controller('tema_libre_detalle', ['$scope','$http','$routeParams',function($scope, $http, $routeParams)
{
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
	
	$http.get("https://fct2016daw.no-ip.org/Proyecto/listaTemaSeleccionado.php?idtema="+$routeParams.tema).then(function (response)
  	{
  	   	$scope.tema = response.data.tema;
  	   	$scope.recursos = response.data.tema.recurso;
  	   	$scope.creador = response.data.usuario;
  	   	
  	   	if($scope.recursos.length > 0)
		{
  			$scope.hayRecursos = false;
  		}
  		else
  		{
  			$scope.hayRecursos = true;
  		}
  	   	
  	   	$scope.openFIle = function($event)
		{
			var fileName = $event.target.name;
				
			if(fileName.split('.')[1] == "pdf")
			{
				$('<div class="modal-backdrop" style="opacity: 0.5 !important;"></div>').appendTo(document.body);
				var object = "<div id='dialog' class='card' style='height:95%;border-radius: 15px;position: fixed;z-index: 999999;top: 20px;width:96%;left: 2%;'><center><object data=\"{FileName}\"style='margin: 10px;' type=\"application/pdf\" width=\"" + (window.innerWidth/100)*90 + "px\" height=\"" + (window.innerHeight /100)*80 + "px\">";
				object += "If you are unable to view file, you can download from <a href=\"{FileName}\">here</a>";
				object += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
				object += "</object><input id='close_btn' type='button' value='Cerrar' style='margin: 10px; width: 98%;' /></div>";
				object = object.replace(/{FileName}/g, "recursos/temaslibres/" + $routeParams.tema + "/" + fileName);
				
				$(object).prependTo(document.body);
				
				$('#close_btn').on('click',function()
				{
					$('.modal-backdrop').remove();
					$('#dialog').remove();
				});
			}
			else if(fileName.split('.').length == 1)
			{
				window.location="https://fct2016daw.no-ip.org/Proyecto/#/" + $routeParams.tema + "/" + fileName;
			}
			else
			{
				window.location="https://fct2016daw.no-ip.org/Proyecto/recursos/temaslibres/" + $routeParams.tema + "/" + fileName;
			}
		}
	});
}]);

app.controller('curso_detalle', ['$scope','$http','$routeParams',function($scope, $http, $routeParams)
{
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
	
	$http.get("https://fct2016daw.no-ip.org/Proyecto/listaCursoSeleccionado.php?idcurso="+$routeParams.curso).then(function (response)
  	{
  	   	$scope.curso = response.data.curso;
  	   	$scope.temas = $scope.curso.temas;
  	   	$scope.creador = response.data.usuario;
  	   	
  	   	if($scope.recursos.length > 0)
		{
  			$scope.hayTemas = false;
  		}
  		else
  		{
  			$scope.hayTemas = true;
  		}
	});
	
	$scope.openTema = function($event)
  	{
  		window.location="https://fct2016daw.no-ip.org/Proyecto/#/temas/"+$event.target.id;
  	}
}]);

app.controller('examen', ['$scope','$http','$routeParams',function($scope, $http, $routeParams)
{
	$('#index-banner').css('display','none');
	
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
	
	$http.get("https://fct2016daw.no-ip.org/Proyecto/listaPreguntaExamen.php?idexamen="+$routeParams.Examen).then(function (response)
  	{
  		$scope.preguntas = response.data;
  		
  		$scope.checked = function($event)
		{
			var checkbox = $('.checkbox');
			var action = "";
			
			if($event.currentTarget.previousElementSibling.checked)
			{
				action = "desactivar"
			}
			else
			{
				action = "activar"
			}
			
			if(action == "desactivar")
			{
				$event.currentTarget.previousElementSibling.removeAttr("checked");
			}
			else
			{
				$event.currentTarget.previousElementSibling.prop('checked',true);;
			}
		}
	});
  	
  	
	
}]);


app.controller('gestionar_temas_libres_detalle', ['$scope','$http','$routeParams',function($scope, $http, $routeParams)
{
	$http.get("https://fct2016daw.no-ip.org/Proyecto/islogin.php?login=true").then(function (response)
  	{
  		if(!response.data.user)
  		{
  			window.location="https://fct2016daw.no-ip.org/Proyecto/";
  		}
  	});
  	
  	$http.get("https://fct2016daw.no-ip.org/Proyecto/listaTemaDetalle.php?idtema="+$routeParams.idTema).then(function (response)
  	{
  	   	$scope.tema = response.data;
  	   	$scope.recursos = response.data.recurso; 
  	   	 	   	
  	   	if($scope.recursos != null)
		{
  			$scope.hayRecursos = false;
  		}
  		else
  		{
  			$scope.hayRecursos = true;
  		}
  	});
  	
  	$scope.addRecurso = function($event)
  	{
  		if(!$($event.currentTarget).hasClass('activado'))
		{
			if(!$('#div_add_recurso').length)
			{
				var html = '<div id="div_add_recurso" class="col l11 s10" hidden="true" href="javascrip:void(0);" style="float: none;margin-left: 25px;">';
				html += '<label for="tipo_rec">Tipo de recurso:</label>';
				html += '<select class="form-control selectpicker" data-style="btn-info" id="tipo_rec">';
				html += '	<option>Seleccione el recurso</option>';
				html += '	<option value="1">Archivo</option>';
				html += '	<option value="2">Examen</option>';
				html += '</select>';
				html += '</div>';
				$($event.currentTarget).addClass('activado');
				$($event.currentTarget).parent().append(html);
				$($event.currentTarget).next().show('slow');
				
				$('#tipo_rec').change(function()
				{
					var opcion = $(this).val();
					switch(opcion)
					{
						case "1":
							if($('#examen').length)
							{
								$('#examen').hide('slow');
							}
							
							if(!$('#div_add_file').length)
							{
								var elemento = '<form id="div_add_file" method="post" action="nuevo_recurso.php" enctype="multipart/form-data">';
								elemento += '		<input class="" type="text" name="idtema" value="'+$routeParams.idTema+'"  style="display: none;"/>';
								elemento += '		<div>';
								elemento += '			<div class="row" style="margin-bottom: 5px;">';
								elemento += '				<input class="col offset-l1 l10 col offset-s1 s10" type="text" name="nombre" placeholder="Nombre" required=""/>';
								elemento += '			</div>';
								elemento += '			<div class="row">';
								elemento += '				<div class="file col offset-l1 l3 col offset-s1 s4" style="padding: 6px;margin-top: 25px;text-align: center;">';
						        elemento += '					<span>Archivo</span>';
						        elemento += '					<input id="archivo" type="file" name="archivo" required="" style="position: absolute;top: 50%;right: 0;left: 89px;bottom: 0;height: 15%;margin: 0;padding: 0;font-size: 20px;cursor: pointer;opacity: 0; ">';
						    	elemento += '				</div>';
						    	elemento += '				<div class="file-path-wrapper col l7 s5">';
						        elemento += '					<input id="archivo_text" class="file-path validate" type="text" disabled="disabled" style="width: 87%;height: 20px;">';
						    	elemento += '				</div>';
						    	elemento += '				<input type="submit" class="col offset-l1 l10 col offset-s1 s10" id="add" name="add" value="Añadir" style="float:none;padding: 10px;text-align: center;margin-top: 10px;">';
						    	elemento += '			</div>';
						    	elemento += '		</div>';
						    	elemento += '	</form>';
								$(this).parent().parent().append(elemento);
								$('#div_add_file').show('slow');
								
								$('#archivo').change(function()
								{
									var filename = $('#archivo').val().replace(/C:\\fakepath\\/i, '')
									$('#archivo_text').val(filename);
								});
								
							}
							else
							{
								$('#div_add_file').show('slow');
							}
								
						break;
						case "2":
							if($('#div_add_file').length)
							{
								$('#div_add_file').hide('slow');
							}
							if(!$('#examen').length)
							{
								$http.get("https://fct2016daw.no-ip.org/Proyecto/plantillas/plantilla1.html").then(function (response)
	  							{
									var plantilla 	 = response.data;
									var elemento 	 = '<form class = "row" id="examen" method="post" action="registraExamen.php" style="color: #039be5;">';
									elemento		+= '	<input type="text" name="idtema" value="'+$routeParams.idTema+'" hidden="true"/>';
									elemento		+= '	<h5>Selecciona la opcion correcta para su almacenamiento</h5>';
									elemento		+= '	<input class="col offset-l1 l10 col offset-s1 s10" type="text" name="nombre" placeholder="Nombre" value="" required="required"/>';
									elemento		+= '	<div id="preguntas">';
									elemento		+= '		<div id="1">';
									elemento		+= '			<h5>Pregunta 1</h5>';
									elemento 		+= 				plantilla;
									elemento		+= '		</div>';
									elemento		+= '	</div>';
									elemento		+= '	<input type="button" class="col offset-l1 l10 col offset-s1 s10" id="add_pregunta" name="add_pregunta" value="Añadir pregunta" style="float:none;padding: 10px;text-align: center;margin-top: 10px;">';
									elemento		+= '	<input type="submit" class="col offset-l1 l10 col offset-s1 s10" id="finalizar" name="finalizar" value="Finalizar" style="float:none;padding: 10px;text-align: center;margin-top: 10px;">';
									elemento 		+= '</form>';
									$('#tipo_rec').parent().parent().append(elemento);
									
									$('#1 input:checkbox').on('click',function()
									{
										$('#1 input:checkbox').not($(this)).prop('required',false);
										if($('#1 input:checkbox:checked').length > 1)
										{
											$('#1 input:checkbox').not($(this)).prop('checked',false);
										}
									});
											
									$('#add_pregunta').on('click',function()
									{
										$http.get("https://fct2016daw.no-ip.org/Proyecto/plantillas/plantilla1.html").then(function (response)
			  							{
			  								var pregunta	 = $('#preguntas').children().last().attr('id');
			  								pregunta = parseInt(pregunta);
			  								pregunta++;
											var plantilla 	 = response.data;
											var elemento	 = '<div id="'+pregunta+'">';
											elemento		+= '	<h5>Pregunta '+pregunta+'</h5>';
											elemento 		+= 		plantilla;
											elemento		+= '</div>';
											$('#preguntas').append(elemento);
											
											$('#'+pregunta+' input:checkbox').on('click',function()
											{
												$('#'+pregunta+' input:checkbox').not($(this)).prop('required',false);
												if($('#'+pregunta+' input:checkbox:checked').length > 1)
												{
													$('#'+pregunta+' input:checkbox').not($(this)).prop('checked',false);
												}
											});
											
										});
									});
									
								});
							}
							else
							{
								$('#examen').show('slow');
							}
						break;
						default:
							if($('#div_add_file').length)
							{
								$('#div_add_file').hide('slow');;
							}
							if($('#examen').length)
							{
								$('#examen').hide('slow');
							}
						break;
					}
				});	
			}
			else
			{
				$($event.currentTarget).addClass('activado');
				$('#tipo_rec option:eq(0)').prop('selected', true)
				$('#div_add_recurso').show('slow');
			}
		}
		else
		{
			$($event.currentTarget).removeClass('activado');
			$($event.currentTarget).next().next().hide('slow');
			$($event.currentTarget).next().hide('slow');
		}
  	}
  	
  	$scope.openFIle = function($event)
	{
			var fileName = $event.target.name;
				
				if(fileName.split('.')[1] == "pdf")
				{
					$('<div class="modal-backdrop" style="opacity: 0.5 !important;"></div>').appendTo(document.body);
					var object = "<div id='dialog' class='card' style='height:95%;border-radius: 15px;position: fixed;z-index: 999999;top: 20px;width:96%;left: 2%;'><center><object data=\"{FileName}\"style='margin: 10px;' type=\"application/pdf\" width=\"" + (window.innerWidth/100)*90 + "px\" height=\"" + (window.innerHeight /100)*80 + "px\">";
					object += "If you are unable to view file, you can download from <a href=\"{FileName}\">here</a>";
					object += " or download <a target = \"_blank\" href = \"https://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
					object += "</object><input id='close_btn' type='button' value='Cerrar' style='margin: 10px; width: 98%;' /></div>";
					object = object.replace(/{FileName}/g, "recursos/temaslibres/" + $routeParams.idTema + "/" + fileName);
					
					$(object).prependTo(document.body);
					
					$('#close_btn').on('click',function()
					{
						$('.modal-backdrop').remove();
						$('#dialog').remove();
					});
				}
				else if(fileName.split('.').length == 1)
				{
					window.location="https://fct2016daw.no-ip.org/Proyecto/#/" + $routeParams.idTema + "/" + fileName;
				}
				else
				{
					window.location="https://fct2016daw.no-ip.org/Proyecto/recursos/temaslibres/" + $routeParams.idTema + "/" + fileName;
				}
	}
	
	$scope.checked = function($event)
	{
		var checkbox = $('#recursos .checkbox');
		var c = 0;
		var action = "";
		
		if($event.currentTarget.previousElementSibling.checked)
		{
			action = "desactivar"
		}
		else
		{
			action = "activar"
		}
		
		
		$.each(checkbox,function(index,value)
		{
			if($(this).is( ":checked" ))
			{
				c++;
			}
		});
		
		if(c > 0)
		{
			if(c == 1 && action == "activar")
			{
				$('#borrar_recursos').css("background-color","");
				$('#borrar_recursos').removeAttr("disabled");
			}
			else if(c == 1 && action == "desactivar")
			{
				$('#borrar_recursos').css("background-color","#A9BCF5");
				$('#borrar_recursos').attr("disabled","disabled");
			}
			else
			{
				$('#borrar_recursos').css("background-color","");
				$('#borrar_recursos').removeAttr("disabled");
			}
			
		}
		else
		{
			if(c == 0 && action == "activar")
			{
				$('#borrar_recursos').css("background-color","");
				$('#borrar_recursos').removeAttr("disabled");
			}
			else
			{
				$('#borrar_recursos').css("background-color","#A9BCF5");
				$('#borrar_recursos').attr("disabled","disabled");
			}
		}
		
		if(action == "desactivar")
		{
			$event.currentTarget.previousElementSibling.removeAttr("checked");
		}
		else
		{
			$event.currentTarget.previousElementSibling.attr('checked','checked');
		}
	}
	
	$('#cambiar_datos').on('click', function()
	{
		$('#modal_datos').css('display','block');
		$('#modal_datos').css('width','65%');
		$('#modal_datos').css('max-height','80%');
		$('#modal_datos').css('min-height','235px');
		$('#modal_datos').css('background-color','rgb(234, 234, 234);');
		$('#modal_datos p').css('text-align','center');
		$('#modal_datos').css('border-radius','5px');
		$('<div class="modal-backdrop" style="opacity: 0.5 !important;"></div>').appendTo(document.body);
	});

	$('#cancelar_datos').on('click', function()
	{
		$('#modal_datos').css('display','none');
		$(".modal-backdrop").remove();
	});
	$('#cambiar_imagen').on('click', function()
	{
		$('#modal_imagen').css('display','block');
		$('#modal_imagen').css('max-height','265px');
		$('#modal_imagen').css('min-height','235px');
		$('#modal_imagen').css('background-color','rgb(234, 234, 234);');
		$('#modal_imagen p').css('text-align','center');
		$('#modal_imagen').css('border-radius','5px');
		$('<div class="modal-backdrop" style="opacity: 0.5 !important;"></div>').appendTo(document.body);
	});

	$('#cancelar_imagen').on('click', function()
	{
		$('#modal_imagen').css('display','none');
		$(".modal-backdrop").remove();
	});
	
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove();
		},4000);

	}
	
}]);

app.controller('verificar1', function($scope, $http)
{
	$('#index-banner').css('display','none');
	
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove();
		},4000);

	}
	
	$http.get("https://fct2016daw.no-ip.org/Proyecto/verificar.php").then(function (response)
  	{
  		$scope.tipos = new Array();
		$scope.tipos[0] = new Array('Profesor','img/ico3.png','El usuario del tipo profesor podrá crear temas y cursos en los que podrá inscribirse sus alumnos.','3');
		$scope.tipos[1] = new Array('Alumno','img/ico2.png','El usuario del tipo alumno podrá inscribirse en cursos para realizar sus temas.','2');
		$scope.tipos[2] = new Array('Libre','img/ico1.png','El usuario del tipo libre podrá crear temas por libre que no estan en ningun curso y realizar temas del mismo tipo.','1');
  		if(!response.data.verificar)
  		{
  			window.location="https://fct2016daw.no-ip.org/Proyecto/";
  		}
  	});
  	
			
});

app.controller('verificar2', function($scope, $http)
{
	$('#index-banner').css('display','none');
	
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
	
	$http.get("https://fct2016daw.no-ip.org/Proyecto/verificar.php").then(function (response)
  	{
  		if(!response.data.verificar)
  		{
  			window.location="https://fct2016daw.no-ip.org/Proyecto/";
  		}
  	});
  	
  	$http.get("https://fct2016daw.no-ip.org/Proyecto/verificar.php?datos=true").then(function (response)
  	{
  		$scope.datos_user = response.data;
  		if(response.data.clave != null)
  		{
  			$('#clave').attr('disabled','disabled');
  		}
  		else
  		{
  			$('#clave').removeAttr('disabled');
  		}
  	});
});

app.controller('empezar', function($scope, $http)
{
	$http.get("https://fct2016daw.no-ip.org/Proyecto/islogin.php?login=true").then(function (response)
  	{
  		if(response.data.user)
  		{
  			window.location="https://fct2016daw.no-ip.org/Proyecto/";
  		}
  	});
	//Ocultamos el baner del inicio
	$('#index-banner').css('display','none');
	$(".modal-backdrop").css('diplay','block');
	$('.row').css('margin-bottom','4%');
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
		
	$('.twitter').on('click', function()
	{
		$('#modal_twitter').css('max-height','170px');
		$('#modal_twitter').css('display','block');
		$('#modal_twitter p').css('text-align','center');
		$('#modal_twitter').css('border-radius','5px');
		$('<div class="modal-backdrop" style="opacity: 0.5 !important;"></div>').appendTo(document.body);
	});
	
	$('#cancelar_twitter').on('click', function()
	{
		$('#modal_twitter').css('display','none');
		$(".modal-backdrop").remove();
	});
	
	$('#verifica').css('display','block');
	$('#verifica').css('max-height','170px');
	$('#verifica').css('text-align','center');
	$('#verifica').css('color','white');
	$('#verifica').css('background-color','#2196F3');
	$('#verifica').css('border-radius','15px');
	$('#cerrar_verifica').on('click', function()
	{
	   $('#verifica').css('display','none');
	   $('#verifica').remove();
	   $(".modal-backdrop").remove();
	});

	$('.clave').on('click', function()
	{
		$('#modal_clave').css('display','block');
		$('#modal_clave').css('max-height','170px');
		$('#modal_clave p').css('text-align','center');
		$('#modal_clave').css('border-radius','5px');
		$('<div class="modal-backdrop" style="opacity: 0.5 !important;"></div>').appendTo(document.body);
	});

	$('#cancelar_clave').on('click', function()
	{
		$('#modal_clave').css('display','none');
		$(".modal-backdrop").remove();
	});

	

});

app.controller('registrar', function($scope, $http)
{
	$http.get("https://fct2016daw.no-ip.org/Proyecto/islogin.php?login=true").then(function (response)
  	{
  		if(response.data.user)
  		{
  			window.location="https://fct2016daw.no-ip.org/Proyecto/";
  		}
  	});
	$('#index-banner').css('display','none');
	//mostramos el modal si el registro ha ido bien
	$('.row').css('margin-bottom','2%');
	//mostramos el modal_registrar durante unos segundos
	if($('#modal').length)
	{
		$('#modal').css('max-height','80px');
		$('#modal').css('display','block');
		$('#modal').css('text-align','center');
		$('#modal').css('color','white');
		$('#modal').css('background-color','#2196F3');
		$('#modal').css('border-radius','15px');
		setTimeout(function()
		{
			$('#modal').css('display','none');
			$('#modal').remove();
			$(".modal-backdrop").remove()
		},4000);

	}
	
	$('.twitter').on('click', function()
	{
		$('#modal_twitter').css('display','block');
		$('#modal_twitter').css('max-height','170px');
		$('#modal_twitter p').css('text-align','center');
		$('#modal_twitter').css('border-radius','5px');
		$('<div class="modal-backdrop" style="opacity: 0.5 !important;"></div>').appendTo(document.body);
	});
	
	$('#cancelar_twitter2').on('click', function()
	{
		$('#modal_twitter').css('display','none');
		$(".modal-backdrop").remove();
	});
});

//Configuracion de las distintas rutas
app.config(function($routeProvider)
{

    $routeProvider
    	.when('/',{
            templateUrl : 'vistas/principal.php',
            controller  : 'principal'
       	})
       	.when('/equipo',{
            templateUrl : 'vistas/equipo.php',
            controller  : 'equipo'
       	})
       	.when('/empezar',{
            templateUrl : 'vistas/empezar.php',
            controller  : 'empezar'
       	})
       	.when('/registrar',{
            templateUrl : 'vistas/registrar.php',
            controller  : 'registrar'
       	})
       	.when('/verificar_step1',{
            templateUrl : 'vistas/verificar1.php',
            controller  : 'verificar1'
       	})
       	.when('/verificar_step2',{
            templateUrl : 'vistas/verificar2.php',
            controller  : 'verificar2'
       	})
       	.when('/user_panel',{
            templateUrl : 'vistas/user_panel_datos.php',
            controller  : 'user_panel'
       	})
       	.when('/crear_temas_libres',{
            templateUrl : 'vistas/crear_temas_libres.php',
            controller  : 'crear_temas_libres'
       	})
       	.when('/gestionar_temas_libres',{
            templateUrl : 'vistas/gestionar_temas_libres.php',
            controller  : 'gestionar_temas_libres'
       	})
       	.when('/gestionar_temas_libres/:idTema', {
            templateUrl : 'vistas/gestionar_temas_libres_detalle.php',
            controller  : 'gestionar_temas_libres_detalle'
        })
        .when('/temas', {
            templateUrl : 'vistas/lista_tema.php',
            controller  : 'lista_tema'
        })
        .when('/crear_cursos', {
            templateUrl : 'vistas/crear_cursos.php',
            controller  : 'crear_cursos'
        })
        .when('/gestionar_cursos', {
            templateUrl : 'vistas/gestionar_cursos.php',
            controller  : 'gestionar_cursos'
        })
        .when('/contacto', {
            templateUrl : 'vistas/contacto.php',
            controller  : 'contacto'
        })
        .when('/cursos', {
            templateUrl : 'vistas/lista_cursos.php',
            controller  : 'cursos'
        })
        .when('/gestionar_cursos/:idCurso', {
            templateUrl : 'vistas/gestionar_cursos_detalle.php',
            controller  : 'gestionar_cursos_detalle'
        })
        .when('/gestionar_temas_curso/:idTema', {
            templateUrl : 'vistas/gestionar_temas_libres_detalle.php',
            controller  : 'gestionar_temas_libres_detalle'
        })
        .when('/temas/:tema', {
            templateUrl : 'vistas/tema_libre_detalle.php',
            controller  : 'tema_libre_detalle'
        })
        .when('/crear_temas_curso/:curso',{
            templateUrl : 'vistas/crear_temas_libres.php',
            controller  : 'crear_temas_libres'
       	})
       	.when('/cursos/:curso', {
            templateUrl : 'vistas/curso_detalle.php',
            controller  : 'curso_detalle'
        })
        .when('/:IdTema/:Examen', {
            templateUrl : 'vistas/examen.php',
            controller  : 'examen'
        })
        .when('/resultados', {
            templateUrl : 'vistas/resultados.php',
            controller  : 'resultados'
        })
        .otherwise({
            redirectTo: '/'
        });
});
