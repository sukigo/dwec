<?php
require_once 'clases/BD.php';
$bd=BD::getInstancia();
session_start();
if (isset($_SESSION['user'])){
	if (isset($_REQUEST['idtema'])){
		$_SESSION['registro'] = true;
		if($bd->borrarTema($_REQUEST['idtema'], $_SESSION['user']['idusuario']))
		{
			$_SESSION['mensaje'] = "Se ha borrado correctamente.";
		}
		else
		{
			$_SESSION['mensaje'] = "Ha ocurrido un error, el tema se ha borrado con anterioridad a este.";
		}
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres");
	}

	if ($_REQUEST['temas']){
		$_SESSION['registro'] = true;
		$tema=$_REQUEST['temas'];
		$cuenta=0;
		foreach($tema as $key => $idtema) {
			if ($bd->borrarTema($idtema, $_SESSION['user']['idusuario'])) {
				$_SESSION['mensaje'] = "Se ha borrado correctamente.";
			} else {
				$_SESSION['mensaje'] = "Ha ocurrido un error.";
			}
		}
		$_SESSION['mensaje'] = "Se ha borrado correctamente.";
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos/".$_POST['idcurso']);
	}
}else{

	header("Location: http://fct2016daw.no-ip.org/Proyecto/#/");

}

?>
