<?php
require("clases/BD.php");

session_start();
if(isset($_POST) && $_POST != NULL) {
	if ($_POST['clave1'] == $_POST['clave2']) {
		$bd = BD::getInstancia();
		if ($bd->ponerClaveNueva($_SESSION['correo'], $_POST['claveanterior'], $_POST['clave1'])) {
			$_SESSION['registro'] = true;
			$_SESSION['mensaje'] = "Se ha cambiado correctamente.";			
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/user_panel");
		} else {
			$_SESSION['registro'] = true;
			$_SESSION['mensaje'] = "La clave anterior no es correcta";
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/user_panel");
		}

	} else {

		$_SESSION['registro'] = true;
		$_SESSION['mensaje'] = "Las claves no coinciden.";
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/user_panel");

	}
}
	?>
