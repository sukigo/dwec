<?php
	require_once 'lib/hybridauth/index.php';
	require ("clases/BD.php");
	session_start();
	$bd= BD::getInstancia();
	function generarCodigo($longitud)
	{
		 $cadena = '';
		 $patron = '1234567890abcdefghijklmnopqrstuvwxyz';
		 $max = strlen($patron)-1;
		 for($i=0;$i < $longitud;$i++) $cadena .= $patron{mt_rand(0,$max)};
		 return $cadena;
	}
	if(isset($_POST) && $_POST != NULL)
	{
		$_SESSION['provider'] = $_POST['provider'];
		if (isset($_POST['email'])){
			$_SESSION['correo']=$_POST['email'];		
		}
	}
	elseif(!isset($_SESSION['provider']))
	{
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/empezar");
	}

	try
	{
	   $config = dirname(__FILE__) . '/lib/hybridauth/config.php';
       // initialize Hybrid_Auth class with the config file
	   $hybridauth = new Hybrid_Auth( $config );
	   // try to authenticate with the selected provider
	   $adapter = $hybridauth->authenticate( $_SESSION['provider'] );
	   // then grab the user profile
	   $user_profile = $adapter->getUserProfile();
		if(!isset($_SESSION['correo'])){
			$_SESSION['correo']=$user_profile->email;
		}
	   $usuario= $bd->iniciaSesionSocial($user_profile->displayName, $_SESSION['correo'], $_SESSION['provider'],$user_profile->identifier);
		if ($usuario != NULL) {
			if ($usuario['tipo'] == 1) {
				$_SESSION['user']=$usuario;
				//$_SESSION['registro'] = false;
				//$_SESSION['mensaje'] = 'Se ha iniciado sesion correctamente';
				header("Location: http://fct2016daw.no-ip.org/Proyecto/");
			}
			if($usuario['tipo'] == 2){
				$_SESSION['registro'] = true;
				$_SESSION['mensaje'] = 'Error al iniciar sesion el usuario no esta activo, <form method="POST" action="verificar_olvidado.php">
					<input type="submit" value="verificar">

</form> ';
				//opcion de reenviar mensaje de verificacion
				header("Location: http://fct2016daw.no-ip.org/Proyecto/#/empezar");
			}
			if ($usuario['tipo']== 3){
				$_SESSION['registro'] = true;
				$_SESSION['mensaje'] = 'Error al iniciar sesion';
				header("Location: http://fct2016daw.no-ip.org/Proyecto/#/empezar");
			}
		}else{
			print_r($user_profile->identifier);
			die();
			session_start();
			session_destroy();

			header('Location: http://fct2016daw.no-ip.org/Proyecto/');
		}
		

   }
   catch( Exception $e )
   {
       switch( $e->getCode() )
       {
	  	  case 0 : echo "Unspecified error."; break;
	  	  case 1 : echo "Hybriauth configuration error."; break;
	  	  case 2 : echo "Provider not properly configured."; break;
	  	  case 3 : echo "Unknown or disabled provider."; break;
	  	  case 4 : echo "Missing provider application credentials."; break;
	  	  case 5 : $_SESSION['registro'] = true;
                   $_SESSION['mensaje'] = "Fallo al registrar. "
	  	              . "El usuario ha cancelado el registro.";
					header("Location: http://fct2016daw.no-ip.org/Proyecto/#/empezar");
	  	           break;
	  	  case 6 : $_SESSION['registro'] = true;
                   $_SESSION['mensaje'] = "Ha ocurrido un error, porfavor inicie sesión de nuevo.";
					header("Location: http://fct2016daw.no-ip.org/Proyecto/#/empezar");
	  	           break;
				  		if(is_object($adapter))
						{
							$adapter->logout();
						}
	  	           
	  	           break;
	  	  case 7 : echo "User not connected to the provider.";
	  	           if(is_object($adapter))
					{
						$adapter->logout();
					}
	  	           break;
	  	  case 8 : echo "Provider does not support this feature."; break;
  		}
 
	  	// well, basically your should not display this to the end user, just give him a hint and move on..
	  	echo "<br /><br /><b>Original error message:</b> " . $e->getMessage();
   }
?>

