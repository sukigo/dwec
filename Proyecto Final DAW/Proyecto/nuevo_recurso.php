<?php
require("clases/BD.php");

session_start();

$bd = BD::getInstancia();
$_SESSION['registro']=true;
if(isset($_SESSION['user'])){
if (isset($_REQUEST)){
	$filtra = explode(".", $_FILES['archivo']['name']);
	$extension = end($filtra);
	if ($extension == "pdf" || $extension == "doc" || $extension == "docx" || $extension == "mp4" || $extension == "mp3") {
		if (is_uploaded_file($_FILES['archivo']['tmp_name'])) {

			$dir_subida = '/var/www/html/Proyecto/recursos/temaslibres/' . $_REQUEST['idtema'] . "/";
			mkdir($dir_subida, 0777, true);
			$fichero_subido = $dir_subida . basename($_FILES['archivo']['name']);
			if (move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_subido)) {
				if ($bd->nuevoRecurso($_REQUEST['idtema'], $_REQUEST['nombre'], $_FILES['archivo']['name'], $_SESSION['user']['idusuario'])) {
					$_SESSION['mensaje'] = "Se ha añadido correctamente.";
					header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres/" . $_REQUEST['idtema']);
				} else {
					$_SESSION['mensaje'] = "Ha ocurrido un error con el registro, vuelva a intentarlo.";
					header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres/" . $_REQUEST['idtema']);
				}
			} else {
				$_SESSION['mensaje'] = "Ha ocurrido un error el archivo no ha podido subirse.";
				header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres/" . $_REQUEST['idtema']);

			}

		} else {
			$_SESSION['mensaje'] = "Error, el archivo no se ha subido correctamente, vuelva a intentarlo.";
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres/" . $_REQUEST['idtema']);
		}
	}else{
		$_SESSION['mensaje'] = "Error, solo puedes subir pdf, doc, docx, mp4 y mp3.";
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres/" . $_REQUEST['idtema']);
	}



	}else{
		$_SESSION['mensaje'] = "No hay nada que pasarme?.";
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres/".$_REQUEST['idtema']);
	}
}else{

	$_SESSION['mensaje'] = "Inicia Sesión.";
	header("Location: http://fct2016daw.no-ip.org/Proyecto/#/");


}
?>


