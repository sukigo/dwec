<?php
require_once 'clases/BD.php';
$bd=BD::getInstancia();
session_start();

if (isset($_SESSION['user'])){
	if(isset($_REQUEST['idcurso'])){
		if($curso = $bd->listaCursoSeleccionado($_SESSION['user']['idusuario'], $_REQUEST['idcurso'])){
			echo json_encode($curso);
		}
	}
}


?>