<?php
require("clases/BD.php");

session_start();

if ($_SESSION['user']){
if(isset($_POST) && $_POST != NULL) {
	
		$bd = BD::getInstancia();
		
		if ($usuario= $bd->modificaCurso($_REQUEST['idcurso'], $_SESSION['user']['idusuario'], $_REQUEST['titulo'], $_REQUEST['descripcion'])) {
			$_SESSION['registro'] = true;			
			$_SESSION['mensaje'] = "Se ha modificado correctamente.";
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos/".$_REQUEST['idcurso']);
		} else {
			$_SESSION['registro'] = true;
			$_SESSION['mensaje'] = "Ha ocurrido un error, vuelva a intentarlo.";
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos/".$_REQUEST['idcurso']);
		}
}else{
		$_SESSION['registro'] = true;
		$_SESSION['mensaje'] = "Has pensado pasarme algo?.";
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos");
}
}else{
		$_SESSION['registro'] = true;
		$_SESSION['mensaje'] = "Inicie Sesión";
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/");
}
	?>