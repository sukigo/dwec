<?php

	require_once 'clases/BD.php';
	$bd=BD::getInstancia();
	session_start();
	if ($_POST['email']!= null || $_POST['pass'] != null){
		
		//en este caso se le pasa como parametro el correo y clave del usuario, inciando sesion de manera normal
		$usuario = $bd->iniciaSesionEstandar($_POST['email'], $_POST['pass']);
		$_SESSION['correo']= $_POST['email'];
		//en este otro caso se introduce simplemente el iddeusuario en caso de que este usando facebook twitter o google plus, este verificara si el usuario ha sido activado previamente.

		if ($usuario['tipo'] == 1) {
			$_SESSION['user']=$usuario;
			
			header("Location: http://fct2016daw.no-ip.org/Proyecto/");
		}
		if($usuario['tipo'] == 2){
			$_SESSION['registro'] = true;
			$_SESSION['mensaje'] = 'Error al iniciar sesion el usuario no esta activo, <form method="POST" action="verificar_olvidado.php">
					<input type="submit" value="verificar">

</form> ';
			//opcion de reenviar mensaje de verificacion
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/empezar");
		}
		if ($usuario['tipo']== 3){
			$_SESSION['registro'] = true;
			$_SESSION['mensaje'] = 'Error al iniciar sesion';
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/empezar");
		}

	}

?>

