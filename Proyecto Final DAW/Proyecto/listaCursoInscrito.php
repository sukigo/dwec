<?php
require_once 'clases/BD.php';
$bd=BD::getInstancia();
session_start();
if(isset($_SESSION['user'])){
	if($_SESSION['user']['idtipousuario']==2) 
	{
		if ($curso = $bd->listaCursosInscritos($_SESSION['user']['idusuario']))
		{
			echo json_encode($curso);
		}
	
	}
}else{
	header("Location: http://fct2016daw.no-ip.org/Proyecto/#/");
}

?>

