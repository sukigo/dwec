<?php
	require("clases/BD.php");
	require("lib/phpmailer/PHPMailerAutoload.php");
	session_start();
	function generarCodigo($longitud)
	{
		 $cadena = '';
		 $patron = '1234567890abcdefghijklmnopqrstuvwxyz';
		 $max = strlen($patron)-1;
		 for($i=0;$i < $longitud;$i++) $cadena .= $patron{mt_rand(0,$max)};
		 return $cadena;
	}
	
	if(isset($_POST) && $_POST != NULL)
	{
		if($_POST['pass'] == $_POST['re_pass'])
		{
			$bd=BD::getInstancia();
			
			$mail = new PHPMailer();

		    //Configuracion del servidor SMTP
		    $mail->isSMTP();
			$mail->CharSet = "UTF-8";
		    $mail->Host = "";
		    $mail->Username = "";
		    $mail->Password = '';
		    $mail->SMTPAuth = true;
		    $mail->SMTPSSecure = 'tls';
		    $mail->Port = 587;
		
		    // Remitente   
		    $mail->SetFrom('', 'E-Ducar');
	
		    // Destinatario
		    $mail->addAddress($_POST['email']);
			
		    // Asunto
		    $mail->Subject ='Verificacion de su cuenta.';
		
		    // Contenido
		    $filename = dirname(__FILE__) . '/recursos/index.html';
			
		    if($file = file_get_contents($filename))
		    {
				$pass=generarCodigo(10);
				$file = str_replace('##NOMBRE##', $_POST['email'], $file);
				$file = str_replace('##verificar##', 'http://fct2016daw.no-ip.org/Proyecto/verificar.php?verificacion='.$pass, $file);
				$mail->IsHTML(true);
				$mail->Body = $file;
				if ($bd->registrarUsuario($_POST['email'], $_POST['pass'], $pass))
				{
					if($mail->send()==false)
					{
		        		$_SESSION['registro'] = true;
						$_SESSION['mensaje'] = $mail->ErrorInfo;
			   			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/registrar");
			    	}
			    	else
			    	{
						$_SESSION['registro'] = true;
						$_SESSION['mensaje'] = "Se ha registrado correctamente.<br/>
						Se le ha enviado un correo a su correo electronico, por favor verifique su cuenta.";
			   			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/registrar");
			    	}					
				}
				else
				{
						$_SESSION['registro'] = true;
						$_SESSION['mensaje'] = "El usuario ya está Registrado";
			   			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/registrar");
				}
			}
		}
		else
		{
			$_SESSION['registro'] = true;
			$_SESSION['mensaje'] = "Las claves no coinciden.";
   			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/registrar");
		}
	}
?>
