<?php
	require_once 'clases/BD.php';
	session_start();
	$bd=BD::getInstancia();
	
	if(isset($_REQUEST['verificacion']))
	{
		if($datos_user = $bd->compruebaVerificacion($_REQUEST['verificacion']))
		 {
		 	$_SESSION['verificar'] = true;
			$_SESSION['datos_user'] = $datos_user;
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/verificar_step1");
		 }
		 else
		 {
			$_SESSION['registro'] = true;
			$_SESSION['mensaje'] = "La verificacón ya se completo o el enlace caduco.";
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/registrar");
		 }
	}
	elseif(isset($_REQUEST['datos']))
	{
		if($_SESSION['datos_user'] != null)
		{
			echo json_encode($_SESSION['datos_user']);
		}
	}
	else
	{
		$verificacion = array('verificar' => $_SESSION['verificar']);
		echo json_encode($verificacion);
	}
		 

	//despues de esto mostrar un apartado en el que el usuario nos da los datos para completar el registro

?>
