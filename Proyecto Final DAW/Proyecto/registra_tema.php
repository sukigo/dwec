<?php
require_once 'clases/BD.php';
$bd=BD::getInstancia();
session_start();


if (isset($_SESSION['user'])){
if(isset($_POST))
{
	$tema = array();
	$tema['titulo']=$_POST['titulo'];
	$tema['descripcion']=$_POST['descripcion'];
	$tema['idusuario']=	$_SESSION['user']['idusuario'];
	$tema['tipotema']= $_POST['tipotema'];
	if (isset($_POST['idcurso'])){

		$tema['idcurso']=$_POST['idcurso'];

	}else{
		$tema['idcurso'] = null;
	}
	if(isset($_POST['imagen'])){
            $activo = true;
        }else{
            $activo = false;
        }
	$filtra = explode(".", $_FILES['imagen']['name']);
	$extension = end($filtra);
	if ($extension == "jpg" || $extension == "png" || $extension == "gif" || $extension == "bmp" || $activo ==false) {
		if(is_uploaded_file($_FILES["imagen"]["tmp_name"]))
		{

			$dir_subida = '/var/www/html/Proyecto/img/';
			$fichero_subido = $dir_subida . basename($_FILES['imagen']['name']);
			if (move_uploaded_file($_FILES['imagen']['tmp_name'], $fichero_subido)) {
				$tema['imagen'] = 'img/'.$_FILES['imagen']['name'];
			} else {
				$tema['imagen'] = 'img/icotema.png';
			}

		}else{
			$tema['imagen'] = 'img/icotema.png';
		}

		if($bd->crearTema($tema)){
			$_SESSION['registro']=true;
			$_SESSION['mensaje']='Se ha creado Correctamente';
			if($tema['tipotema'] == 1)
			{
				header('Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres');
			}
			elseif($tema['tipotema'] == 2)
			{
				header('Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos/'.$_POST['idcurso']);
			}
		}else{
			$_SESSION['registro']=true;
			$_SESSION['mensaje']='ha ocurrido un error al crear el tema.';
			header('Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres');
		}
	}else{
		$_SESSION['registro']=true;
		$_SESSION['mensaje']='Error solo imagenes en formatto png, jpg, bmp y gif';
		header('Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres');
	}
}else{
	header('Location: http://fct2016daw.no-ip.org/Proyecto/#/');
}
}
?>

