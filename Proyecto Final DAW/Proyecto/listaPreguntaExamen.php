<?php
	require("clases/BD.php");
	session_start();
	$bd = BD::getInstancia();
	
	if(isset($_REQUEST['idexamen']))
	{
		if($preguntas = $bd->listaPreguntas($_REQUEST['idexamen']))
		{
			echo json_encode($preguntas);
		}
	}
	else
	{
		echo "Es necesario el id del examen";
	}
?>