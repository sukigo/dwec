<div class="wrapper">
	<form method="post" enctype="multipart/form-data" action="datos_verificacion.php">
		<div class="section scrollspy" id="work">
		    <div class="container">
		        <h2 class="header text_b">Datos de Usuario</h2>
		        <div class="row">
		        	<input type="text" class="email col s12" name="nick" value="{{datos_user.nick}}" placeholder="Nick" required="" style=""/>
		        </div>
		        <div class="row">
		        	<input type="text" class="email col s12" name="nombre" value="{{datos_user.nombre}}" placeholder="Nombre" required=""/>
		        </div>
		        <div class="row">
		        	<input type="email" class="email col s5" name="correo" placeholder="Email" value="{{datos_user.correo}}" disabled="disabled"/>
		        	<input id="clave" type="password" class="email col offset-s2 s5" name="clave" placeholder="Contraseña" value="{{datos_user.clave}}" required=""/>
		        </div>
		        <div class="row file-field input-field">
				    <div class="file col s3" style="padding: 12px;margin-top: 27px;text-align: center;">
				        <span>Foto de Perfil</span>
				        <input type="file" name="imagen">
				    </div>
				    <div class="file-path-wrapper">
				        <input class="file-path validate col l12 s12" type="text" disabled="disabled" style="height: 49px;margin-top: 28px;">
				    </div>
			    </div>
		        <div class="row">
		        	<a type="button" class="col s4" href="http://fct2016daw.no-ip.org/Proyecto/#/verificar_step1" style="margin-top: 30px;padding: 10px;text-align: center;">Atras</a>
		            <input type="submit" class="col offset-s4 s4" name="finalizar" value="Finalizar" style="margin-top: 30px;padding: 10px;">
		        </div>
		    </div>
		</div>
	</form>
</div>
