<!--Introduccion y servicios-->		
<!-- ESTA SECCION SOLO DEBE VERSE SI NO ESTA LOGEADO -->
<div id="intro" class="section scrollspy wrapper">
    <div class="container">
        <div class="row">
            <div  class="col s12">
                <h2 class="center header text_h2"> Bienvenido a <span class="span_h2"> E-Ducar  </span>la página web donde prodrás<span class="span_h2"> enseñar</span>, <span class="span_h2"> aprender</span> y <span class="span_h2"> compartir</span> con todo el mundo de forma interactiva y divertida.</h2>
            </div>

            <div  class="col s12 m4 l4">
                <div class="center promo promo-example">
                    <i class="mdi-image-edit"></i>
                    <h5 class="promo-caption">Enseña</h5>
                    <p class="light center">Aquí podrás enseñar lo que tu quieras a quien tu quieras, tanto si eres un profesor que quiere enseñar a sus alumnos mediante la creacion de cursos, como si tienes algo que compartir con el mundo.</p>
                </div>
            </div>
            <div class="col s12 m4 l4">
                <div class="center promo promo-example">
                    <i class="mdi-social-school"></i>
                    <h5 class="promo-caption">Aprende</h5>
                    <p class="light center">Aquí podrás aprender sobre cualquier tema o categoría ya sea mediante cursos o de forma libre, todo ello de forma interactiva y divertida.</p>
                </div>
            </div>
            <div class="col s12 m4 l4">
                <div class="center promo promo-example">
                    <i class="mdi-social-public"></i>
                    <h5 class="promo-caption">Comparte</h5>
                    <p class="light center">Los cursos y temas que crees podrán ser compartidos en redes sociales, podran ser comentados y puntuados, de esta manera podrá aceder a una información y unos cursos más fiables.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Boton empezar final-->
<?php
	session_start();

	if(!isset($_SESSION['user']))
	{
		echo 	'<div class="row">
					<div class="col s12">
							<a class="btn-large col offset-s2 s8" style="background-color: #2196F3 " href="#/empezar">
								Empezar en E-Ducar!
								<i class="fa fa-graduation-cap" aria-hidden="true"></i>
							</a>
					</div>
				</div>';
	}
?>
<style>
	a:focus, a:hover {
    color: white;
    text-decoration: underline;
}
</style>