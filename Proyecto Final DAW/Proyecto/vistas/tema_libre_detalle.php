<div ng-data="tema">
	<header class="main-header">
		    <!-- Header Navbar: style can be found in header.less -->
		    <nav class="navbar navbar-static-top" style="margin: 0 auto; text-align: center;background-color: #1aa3ff;overflow: hidden;">
		      <!-- Sidebar toggle button-->
		      <a href="javascript:void(0);" class="sidebar-toggle" data-toggle="offcanvas" role="button" style="font-size: 35px;position: relative;top: -15px;left: 35px;">
				 <span class="sr-only">Toggle navigation</span>
			  </a>
		      <h2 style="margin: 0 auto;">{{tema.titulo}}</h2>
		    </nav>
	</header>
	  	<!-- Font Awesome -->
	  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	  	<!-- Ionicons -->
	  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	  	<!-- Theme style -->
	  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	  	<!-- AdminLTE Skins. Choose a skin from the css/skins
	    	   folder instead of downloading all of them to reduce the load. -->
	  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	  	<!-- iCheck -->
	  	<link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
	  	<!-- Morris chart -->
	  	<link rel="stylesheet" href="plugins/morris/morris.css">
	  	<!-- jvectormap -->
	  	<link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	  	<!-- Date Picker -->
	  	<link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
	  	<!-- Daterange picker -->
	  	<link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
	  	<!-- bootstrap wysihtml5 - text editor -->
	  	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	
	<div class="wrapper">
	  <!-- Left side column. contains the logo and sidebar -->
	  <aside class="main-sidebar">
	    <!-- sidebar: style can be found in sidebar.less -->
	    <section class="sidebar">
	      <!-- sidebar menu: : style can be found in sidebar.less -->
	      <ul class="sidebar-menu">
	        <li class="header" ng-data="creador">Datos Creador</li>
	        <li>
	        	<div class="user-panel">
		        	<div class="pull-left image">
		          		<img src="{{creador.imagenusuario}}" class="img-circle" alt="User Image">
		        	</div>
		        	<div class="pull-left info">
		          		<p>{{creador.nick}}</p>
		        	</div>
		    	</div>
		    </li>
	      </ul>
	    </section>
	    <!-- /.sidebar -->
	  </aside>
	
	  <!-- Content Wrapper. Contains page content -->
	  <div class="content-wrapper" style="background-color: white;">
	  	<a href="javascript:void(0);" class="sidebar-toggle" data-toggle="offcanvas" role="button">
	        <span class="sr-only">Toggle navigation</span>
	    </a>
	    <!-- Main content -->
	    <section class="content" style="background-color: white; min-height: 400px;">
			<div ng-data="tema" class="row" style="margin: 20px auto;">
				<div class="col l3 offset-m3 m10 s12">
					 <img src="{{tema.imagen}}" class="img" alt="tema Image" style="max-width: 320px; max-height: 420px;">
				</div>
				<div class="col offset-l2 l6 offset-m3 m10 s12" style="margin-top: 15px">
					<div class="row">
						<span class="col l2 s3" style="color: #3c8dbc;">Titulo: </span>
						<input type="text" class="email col l5 offset-s1 s9" name="titulo" placeholder="Email" value="{{tema.titulo}}"  disabled="disabled" style="border: 0px;background-color: white;color: black;"/>
					</div>
					<div class="row">
						<span class="col l2 s3" style="color: #3c8dbc;">Descripción: </span>
					</div>
					<div class="row">
						<textarea class="col l10 offset-s1 s9" name="descripcion" placeholder="Descripcion"   disabled="disabled" style="text-align:left; min-width:300px; min-height: 140px;border: 0px;background-color: white;color: black;">
							{{tema.descripcion}}
						</textarea>
					</div>
				</div>
			    <div class="row">
			    	<div class="list-group col l11 s10" style="margin-left: 25px;">
					  <a href="javascript:void(0);" class="list-group-item active" style="background-color: #2196F3;">
					    Recursos : 
					  </a>
					  <div  ng-if="hayRecursos" class="list-group-item">
					  	<label style="text-align: center;">El tema no dispone de ningun recurso aún.</label>
					  </div>
					  <div id="recursos" ng-repeat="recurso in recursos" class="list-group-item">
					  	<div class="material-switch" style="background-color: white !important;">
                            <a href="" ng-click="openFIle($event);" name="{{recurso.recurso}}">{{recurso.nombre}}<label style="float: right;">{{recurso.fecha}}</label></a>
                            <div id="dialog"></div>
	                    </div>
					  </div>
					</div>
			    </div>
			</div>
	    </section>
	    <!-- /.content -->
	  </div>
	</div>
</div>
<!-- ./wrapper -->
<style type="text/css">
	html{
		font-size: 15px !important;
	}
	
</style>