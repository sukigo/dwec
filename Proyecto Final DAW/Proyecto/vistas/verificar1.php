<form method="post"  enctype="multipart/form-data" action="datos_verificacion.php">
	<div class="section scrollspy" id="work">
	    <div class="container">
	        <h2 class="header text_b">Tipo de usuario</h2>
	        <div class="row">
	            <div class="col s12 m4 l4" ng-repeat="tipo in tipos">
	                <div class="card">
	                    <div class="card-image waves-effect waves-block waves-light">
	                        <img class="activator" src="{{tipo[1]}}">
	                    </div>
	                    <div class="card-content">
	                        <span class="card-title activator grey-text text-darken-4">{{tipo[0]}}<i class="mdi-navigation-more-vert right"></i></span>
	                    </div>
	                    <div class="card-reveal">
	                        <span class="card-title grey-text text-darken-4">{{tipo[0]}}<i class="mdi-navigation-close right"></i></span>
	                        <p>{{tipo[2]}}</p>
	                    </div>
	                    <input type="radio" name="tipo" value="{{tipo[3]}}" style="visibility: visible !important;position: relative !important;left: 50% !important;width:2em;height:4em;" required="required"/>
	                </div>
	            </div>
	            <input type="submit" class="col offset-s8 s4" value="Siguiente" style="margin-top: 30px;padding: 10px;">
	        </div>
	    </div>
	</div>
</form>
