<div class="section scrollspy" id="work">
    <div class="container">
        <h2 class="header text_b">Cursos</h2>
        <div class="row">
        	<div ng-if="noCursos"><h4 style="text-align: center;color: #2196F3;min-height: 200px;">Aún no disponemos de cursos activos.</h4></div>
            <div class="col s12 m4 l4" ng-repeat="curso in cursos">
                <div class="card"  style="height: 400px;">
                    <div class="card-image waves-effect waves-block waves-light">
                        <img class="activator" ng-src="{{curso.imagen}}" style="height: 270px;">
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4">{{curso.titulo}}<i class="mdi-navigation-more-vert right"></i></span>
                        <p ng-if="curso.inscrito == true"><a href="http://fct2016daw.no-ip.org/Proyecto/inscribirseCurso.php?idcurso={{curso.idcurso}}">Inscribirse</a></p>
                        <p ng-if="curso.inscrito == false"><a href="http://fct2016daw.no-ip.org/Proyecto/#/cursos/{{curso.idcurso}}">Ir al curso</a></p>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4">{{curso.titulo}} <i class="mdi-navigation-close right"></i></span>
                        <p>{{curso.descripcion}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>