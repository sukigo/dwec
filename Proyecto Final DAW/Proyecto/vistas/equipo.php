	<!--Team-->
		<div class="section scrollspy wrapper" id="team">
		    <div class="container">
		        <h2 class="header text_b"> Equipo </h2>
		        <div class="row">
		            <div class="col s12 m3">
		                <div class="card card-avatar">
		                    <div class="waves-effect waves-block waves-light">
		                        <img class="activator" src="img/aldo.jpg">
		                    </div>
		                    <div class="card-content">
		                        <span class="card-title activator grey-text text-darken-4">Aldo<br/>
		                            <small><em><a class="red-text text-darken-1" href="">Developer</a></em></small></span>
		                        <p>
		                            <a class="blue-text text-lighten-2" href="https://www.facebook.com/aldo.guerrerogarcia.5">
		                                <i class="fa fa-facebook-square"></i>
		                            </a>
		                            <a class="blue-text text-lighten-2" href="https://twitter.com/sukigo">
		                                <i class="fa fa-twitter-square"></i>
		                            </a>
		                            <a class="blue-text text-lighten-2" href="https://plus.google.com/u/1/103363384727955026567/posts">
		                                <i class="fa fa-google-plus-square"></i>
		                            </a>
		                        </p>
		                    </div>
		                </div>
		            </div>
		            <div class="col s12 m3">
		                <div class="card card-avatar">
		                    <div class="waves-effect waves-block waves-light">
		                        <img class="activator" src="img/iban.jpg">
		                    </div>
		                    <div class="card-content">
		                        <span class="card-title activator grey-text text-darken-4">Iban<br/>
		                            <small><em><a class="red-text text-darken-1" href="">Developer</a></em></small></span>
		                        <p>
		                            
		                            <a class="blue-text text-lighten-2" href="https://plus.google.com/u/0/108601293130498123773">
		                                <i class="fa fa-google-plus-square"></i>
		                            </a>

		                        </p>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
