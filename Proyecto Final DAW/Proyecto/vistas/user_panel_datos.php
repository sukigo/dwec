<?php 
	session_start(); 
?>

<header class="main-header">
	    <!-- Header Navbar: style can be found in header.less -->
	    <nav class="navbar navbar-static-top" style="margin: 0 auto; text-align: center;background-color: #1aa3ff;overflow: hidden;">
	      <!-- Sidebar toggle button-->
	      <a href="javascript:void(0);" class="sidebar-toggle" data-toggle="offcanvas" role="button" style="font-size: 35px;position: relative;top: -15px;left: 35px;">
			 <span class="sr-only">Toggle navigation</span>
		  </a>
	      <h2 style="margin: 0 auto;">Panel de Usuario</h2>
	    </nav>
</header>
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
    	   folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  	<!-- iCheck -->
  	<link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  	<!-- Morris chart -->
  	<link rel="stylesheet" href="plugins/morris/morris.css">
  	<!-- jvectormap -->
  	<link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  	<!-- Date Picker -->
  	<link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  	<!-- Daterange picker -->
  	<link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
  	<!-- bootstrap wysihtml5 - text editor -->
  	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

<div class="wrapper">
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Menú</li>
        <li class="treeview">
          <a href="javascript:void(0);">
            <i class="fa fa-user"></i> <span>Datos Usuario</span></i>
          </a>
        </li>
        <?php
        	if($_SESSION['user']['idtipousuario'] == 1)
			{
				//temas libres
				echo '<li class="treeview">
				          <a href="javascript:void(0);">
				            <i class="fa fa-book"></i> <span>Temas Libres</span>  <i class="fa fa-angle-left pull-right"></i>
				            <ul class="treeview-menu">
					            <li class="active"><a href="http://fct2016daw.no-ip.org/Proyecto/#/crear_temas_libres"><i class="fa fa-circle-o"></i> Crear</a></li>
					            <li><a href="http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres"><i class="fa fa-circle-o"></i> Gestionar</a></li>
					        </ul>
				          </a>
				      </li>';
			}
			elseif($_SESSION['user']['idtipousuario'] == 2) 
			{
				//cursos
				echo '<li class="treeview">
				          <a href="javascript:void(0);">
				            <i class="fa fa-graduation-cap"></i> <span>Cursos</span>  <i class="fa fa-angle-left pull-right"></i>
				            <ul class="treeview-menu">
					            <li class="active"><a href="http://fct2016daw.no-ip.org/Proyecto/#/cursos"><i class="fa fa-circle-o"></i> Inscribirse</a></li>
					            <li><a href="http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos"><i class="fa fa-circle-o"></i> Gestionar</a></li>
					        </ul>
				          </a>
				      </li>';
				//temas libres
				echo '<li class="treeview">
				          <a href="javascript:void(0);">
				            <i class="fa fa-book"></i> <span>Temas Libres</span>  <i class="fa fa-angle-left pull-right"></i>
				            <ul class="treeview-menu">
					            <li class="active"><a href="http://fct2016daw.no-ip.org/Proyecto/#/temas"><i class="fa fa-circle-o"></i> Inscribirse</a></li>
					            <li><a href="http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres"><i class="fa fa-circle-o"></i> Gestionar</a></li>
					        </ul>
				          </a>
				      </li>';
			}
			elseif($_SESSION['user']['idtipousuario'] == 3)
			{
				//cursos
				echo '<li class="treeview">
				          <a href="javascript:void(0);">
				            <i class="fa fa-graduation-cap"></i> <span>Cursos</span>  <i class="fa fa-angle-left pull-right"></i>
				            <ul class="treeview-menu">
					            <li class="active"><a href="http://fct2016daw.no-ip.org/Proyecto/#/crear_cursos"><i class="fa fa-circle-o"></i> Crear</a></li>
					            <li><a href="http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos"><i class="fa fa-circle-o"></i> Gestionar</a></li>
					        </ul>
				          </a>
				      </li>';
				//temas libres   
				echo '<li class="treeview">
				          <a href="javascript:void(0);">
				            <i class="fa fa-book"></i> <span>Temas Libres</span>  <i class="fa fa-angle-left pull-right"></i>
				            <ul class="treeview-menu">
					            <li class="active"><a href="http://fct2016daw.no-ip.org/Proyecto/#/crear_temas_libres"><i class="fa fa-circle-o"></i> Crear</a></li>
					            <li><a href="http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres"><i class="fa fa-circle-o"></i> Gestionar</a></li>
					        </ul>
				          </a>
				      </li>';
			}
		?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="background-color: white;">
  	<a href="javascript:void(0);" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Main content -->
    <section class="content" >
		<div class="row" style="margin: 20px auto;">
			<div class="col l3 offset-m2 m10 s12">
				 <img <?php echo 'src="'.$_SESSION['user']['imagen'].'"'; ?> class="img" alt="User Image" style="max-width: 320px; max-height: 420px;">
			</div>
			<div class="col offset-l2 l6 offset-m2 m10 s12" style="margin-top: 15px">
				<div class="row">
					<span class="col l2 s3" style="color: #3c8dbc;">Nick: </span>
					<input type="text" class="email col l5 offset-s1 s9" name="nick" placeholder="Email"<?php echo 'value="'.$_SESSION['user']['nick'].'"'; ?>  disabled="disabled" style="border: 0px;background-color: white;color: black;"/>
				</div>
				<div class="row">
					<span class="col l2 s3" style="color: #3c8dbc;">Nombre: </span>
					<input type="text" class="email col l5 offset-s1 s9" name="nombre" placeholder="Email"<?php echo 'value="'.$_SESSION['user']['nombre'].'"'; ?>  disabled="disabled" style="border: 0px;background-color: white;color: black;"/>
				</div>
				<div class="row">
					<span class="col l2 s3" style="color: #3c8dbc;">E-Mail: </span>
					<input type="text" class="email col l5 offset-s1 s9" name="correo" placeholder="Email"<?php echo 'value="'.$_SESSION['user']['correo'].'"'; ?>  disabled="disabled" style="border: 0px;background-color: white;color: black;"/>
				</div>
				 
			</div>
			<div class="row">
	        	<input type="button" class="col l11 s10" id="cambiar_imagen" name="cambiar_imagen" value="Cambiar Foto" style="padding: 10px;text-align: center;margin-left: 25px;margin-top: 10px;">
		    </div>	
			<div class="row">
	        	<input type="button" class="col l11 s10" id="cambiar_datos" name="cambiar_datos" value="Modificar Datos" style="padding: 10px;text-align: center;margin-left: 25px;margin-top: 10px;">
		    </div>	
		    <div class="row">
	        	<input type="button" class="col l11 s10" id="cambiar_clave" name="cambiar_clave" value="Cambiar Contraseña" style="padding: 10px;text-align: center;margin-left: 25px;margin-top: 10px;">
		    </div>
		</div>
    </section>
    <!-- /.content -->
  </div>
</div>
<!-- ./wrapper -->

<!--modales -->
<div id="modal_clave" class="modal modal__bg" role="dialog" aria-hidden="true">
	<div class="modal__dialog">
		<div class="modal__content row">
			<form id="clave_tw" action="cambiar_clave.php" method="post">
				<h3 style="text-align: center;margin-top: 20px;color: #2196F3;">Cambiar contraseña</h3>
				<div class="row">
					<input class="col offset-s1 s10" type="password" name="claveanterior" placeholder="Contraseña Actual" required="" style="margin: 0 0 0 8.33333%;"/>
				</div>
				<div class="row">
					<input class="col offset-s1 s4" type="password" name="clave1" placeholder="Nueva Contraseña" required="" style="margin: 0 0 0 8.33333%;"/>
					<input class="col offset-s2 s4" type="password" name="clave2" placeholder="Verificar contraseña" required="" style="margin: 0 0 0 16.33333%;"/>
				</div>
				
				<div class="row">
			    	<input type="submit" class="col offset-s1 s4" value="Confirmar">
			    	<input type="button" id="cancelar_clave" class="col offset-s2 s4" value="Cancelar">
				
			    </div>
			</form>
		</div>
	</div>
</div>
<div id="modal_imagen" class="card modal modal__bg" role="dialog" aria-hidden="true">
	<div class="fondo_opaco"></div>
	<div class="modal__dialog">
		<div class="modal__content">
			<form id="imagen_tw" enctype="multipart/form-data" action="cambia_imagen.php" method="post">
				<h3 style="text-align: center;margin-top: 20px;color: #2196F3;">Cambio de Imagen</h3>
				<div class="row file-field input-field">
				    <div class="file col offset-l1 l3 s4" style="padding: 6px;margin-top: 25px;text-align: center;">
				        <span>Nueva Imagen</span>
				        <input type="file" name="imagen" required="">
				    </div>
				    <div class="file-path-wrapper col l7 s6">
				        <input class="file-path validate" type="text" disabled="disabled" style="width: 87%;height: 20px;">
				    </div>
			    </div>
			    <div class="row">
			    	<input type="submit" class="col offset-s1 s4" value="Confirmar">
			    	<input type="button" id="cancelar_imagen" class="col offset-s2 s4" value="Cancelar">
			    </div>
					
			</form>
		</div>
	</div>
</div>
<div id="modal_datos" class="modal modal__bg" role="dialog" aria-hidden="true">
	<div class="fondo_opaco"></div>
	<div class="modal__dialog">
		<div class="modal__content row">
			<form id="datos_tw" action="cambia_datos.php" method="post">
				<h3 style="text-align: center;margin-top: 20px;color: #2196F3;">Modificar Datos</h3>
				<div class="row">
					<input class="col offset-l1 l4 s12" type="text" name="nick" placeholder="Nick" <?php echo 'value="'.$_SESSION['user']['nick'].'"' ?>  style="margin-top: 10px;" required=""/>
					<input class="col offset-l2 l4 s12" type="text" name="nombre" placeholder="Nombre" <?php echo 'value="'.$_SESSION['user']['nombre'].'"' ?> style="margin-top: 10px;" required=""/>
				</div>
				<div class="row">
			    	<input type="submit" class="col offset-s1 s4" value="Confirmar">
			    	<input type="button" id="cancelar_datos" class="col offset-s2 s4" value="Cancelar">
			    </div>
			</form>
		</div>
	</div>
</div>

<style type="text/css">
	html{
		font-size: 15px !important;
	}
	
</style>
