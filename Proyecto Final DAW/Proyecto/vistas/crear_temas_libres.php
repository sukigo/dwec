<?php 
	session_start(); 
?>

<header class="main-header">
	    <!-- Header Navbar: style can be found in header.less -->
	    <nav class="navbar navbar-static-top" style="margin: 0 auto; text-align: center;background-color: #1aa3ff;overflow: hidden;">
	      <!-- Sidebar toggle button-->
	      <a href="javascript:void(0);" class="sidebar-toggle" data-toggle="offcanvas" role="button" style="font-size: 35px;position: relative;top: -15px;left: 35px;">
			 <span class="sr-only">Toggle navigation</span>
		  </a>
	      <h2 style="margin: 0 auto;">Panel de Usuario</h2>
	    </nav>
</header>
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  	<!-- Theme style -->
  	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  	<!-- AdminLTE Skins. Choose a skin from the css/skins
    	   folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  	<!-- iCheck -->
  	<link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  	<!-- Morris chart -->
  	<link rel="stylesheet" href="plugins/morris/morris.css">
  	<!-- jvectormap -->
  	<link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  	<!-- Date Picker -->
  	<link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  	<!-- Daterange picker -->
  	<link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
  	<!-- bootstrap wysihtml5 - text editor -->
  	<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

<div class="wrapper">
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Menú</li>
        <li class="treeview">
          <a href="http://fct2016daw.no-ip.org/Proyecto/#/user_panel">
            <i class="fa fa-user"></i> <span>Datos Usuario</span></i>
          </a>
        </li>
         <?php
        	if($_SESSION['user']['idtipousuario'] == 1)
			{
				//temas libres
				echo '<li class="treeview">
				          <a href="javascript:void(0);">
				            <i class="fa fa-book"></i> <span>Temas Libres</span>  <i class="fa fa-angle-left pull-right"></i>
				            <ul class="treeview-menu">
					            <li class="active"><a href="http://fct2016daw.no-ip.org/Proyecto/#/crear_temas_libres"><i class="fa fa-circle-o"></i> Crear</a></li>
					            <li><a href="http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres"><i class="fa fa-circle-o"></i> Gestionar</a></li>
					        </ul>
				          </a>
				      </li>';
			}
			elseif($_SESSION['user']['idtipousuario'] == 2) 
			{
				//cursos
				echo '<li class="treeview">
				          <a href="javascript:void(0);">
				            <i class="fa fa-graduation-cap"></i> <span>Cursos</span>  <i class="fa fa-angle-left pull-right"></i>
				            <ul class="treeview-menu">
					            <li class="active"><a href="http://fct2016daw.no-ip.org/Proyecto/#/cursos"><i class="fa fa-circle-o"></i> Inscribirse</a></li>
					            <li><a href="http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos"><i class="fa fa-circle-o"></i> Gestionar</a></li>
					        </ul>
				          </a>
				      </li>';
				//temas libres
				echo '<li class="treeview">
				          <a href="javascript:void(0);">
				            <i class="fa fa-book"></i> <span>Temas Libres</span>  <i class="fa fa-angle-left pull-right"></i>
				            <ul class="treeview-menu">
					            <li class="active"><a href="http://fct2016daw.no-ip.org/Proyecto/#/temas"><i class="fa fa-circle-o"></i> Inscribirse</a></li>
					            <li><a href="http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres"><i class="fa fa-circle-o"></i> Gestionar</a></li>
					        </ul>
				          </a>
				      </li>';
			}
			elseif($_SESSION['user']['idtipousuario'] == 3)
			{
				//cursos
				echo '<li class="treeview">
				          <a href="javascript:void(0);">
				            <i class="fa fa-graduation-cap"></i> <span>Cursos</span>  <i class="fa fa-angle-left pull-right"></i>
				            <ul class="treeview-menu">
					            <li class="active"><a href="http://fct2016daw.no-ip.org/Proyecto/#/crear_cursos"><i class="fa fa-circle-o"></i> Crear</a></li>
					            <li><a href="http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos"><i class="fa fa-circle-o"></i> Gestionar</a></li>
					        </ul>
				          </a>
				      </li>';
				//temas libres   
				echo '<li class="treeview">
				          <a href="javascript:void(0);">
				            <i class="fa fa-book"></i> <span>Temas Libres</span>  <i class="fa fa-angle-left pull-right"></i>
				            <ul class="treeview-menu">
					            <li class="active"><a href="http://fct2016daw.no-ip.org/Proyecto/#/crear_temas_libres"><i class="fa fa-circle-o"></i> Crear</a></li>
					            <li><a href="http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres"><i class="fa fa-circle-o"></i> Gestionar</a></li>
					        </ul>
				          </a>
				      </li>';
			}
		?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="background-color: white;">
  	<a href="javascript:void(0);" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Main content -->
    <section class="content" style="background-color: white;">
    	<form method="post" action="registra_tema.php" enctype="multipart/form-data">
    		<input ng-if="curso" type="text" name="idcurso" value="{{idcurso}}" hidden="hidden" />
    		<input ng-if="curso" type="text" name="tipotema" value="2" hidden="hidden" />
    		<input ng-if="curso == false" type="text" name="tipotema" value="1" hidden="hidden" />
    		<h4 class="col offset-l4 l7 offset-s1 s12" style="color: #2196F3;">Crear Tema</h4>
			<div class="row">
				<label class="col offset-l1 l2 offset-s1 s2" style="font-size: 14px;margin-top: 25px;color: #2196F3;">Titulo: </label>
				<input type="text" class="email col l7 s6 " name="titulo" placeholder="Titulo" required="required"/>
			</div>
			<div class="row file-field input-field">
			    <div class="file col offset-l1 l2 offset-s1 s3" style="padding: 6px;margin-top: 25px;text-align: center;">
			        <span>Imagen</span>
			        <input type="file" name="imagen">
			    </div>
			    <div class="file-path-wrapper col l9 s5">
			        <input class="file-path validate" type="text" disabled="disabled" style="width: 70%;height: 20px;margin-top: 25px">
			    </div>
		    </div>
	    	<div class="row">
	    		<textarea class="form-control col offset-l1 l9 offset-s1 s8" name="descripcion" placeholder="Descripción...." style="min-height: 200px;border-radius: 10px;" required="required"></textarea>
		    </div>
		    <div class="row">
		    	<input type="submit" class="col offset-l1 l9 offset-s1 s8" id="finalizar" name="finalizar" value="Finalizar" style="padding: 10px;text-align: center;margin-top: 10px;">
		    </div>
    	</form>
    </section>
    <!-- /.content -->
  </div>
</div>
<!-- ./wrapper -->
<style type="text/css">
	html{
		font-size: 15px !important;
	}
	
</style>
