<div class="body_login wrapper">
	<div class="login-form">
		<div class="top-form">
			<h2>Registrar</h2>
			<ul class="top-sc-icons">
				<li>
					<form id="social_fb" action="registro_social.php" method="post">
						<button type="submit" class="facebook">
							<input type="text" name="provider" value="Facebook" style="display: none;"/>
							<img src="img/fb.png" />							
						</button>
					</form>
				</li>
				<li>
					<button type="button" class="twitter" name="Twitter">
						<img src="img/tw.png" />						
					</button>
				</li>
				<li>
					<form id="social_gp" action="registro_social.php" method="post">
						<button type="submit" class="gp" name="Google">
							<input type="text" name="provider" value="Google" style="display: none;"/>
							<img src="img/gp.png" /> 
						</button>
					</form>
				</li>
			</ul>
			<div class="clear"> </div>
		</div>
		<div class="bottom-form">
			<div class="form-top">
				<form action="registro.php" method="post">
					<input type="email" class="email" name="email" placeholder="Email" required=""/>
					<input type="password" class="password" name="pass" placeholder="Contraseña" required="" style=" margin: 18px 0 0 0 !important; "/>
					<input type="password" class="password" name="re_pass" placeholder="Verificar Contraseña" required=""/>
					<input type="submit" value="Registrar" style="margin-top: 15px;">
				</form>
			</div>
			<div class="sign-up">
				<p>Ya estas registrado?<a href="#/empezar">Inicia Sesion</a></p>
			</div>
		</div>
	</div>
</div>

<div id="modal_twitter" class="modal modal__bg" role="dialog" aria-hidden="true">
		<div class="modal__dialog">
			<div class="modal__content row">
				<form id="social_tw" action="registro_social.php" method="post">
					<input type="text" name="provider" value="Twitter" style="display: none;"/>
					<h6 style="text-align: center;margin-top: 20px;color: #2196F3;">Por favor introduzca un email para enviarle el correo de verificacion de la cuenta:</h6>
					<input type="email" class="email col offset-s1 s10" name="email" placeholder="Email" required="" style="margin-bottom: 20px;"/>
					<input type="submit" class="col offset-s1 s4" value="Confirmar">
					<input type="button" id="cancelar_twitter2" class="col offset-s2 s4" value="Cancelar">
				</form>
			</div>
		</div>
	</div>