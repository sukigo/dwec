<form method="post" action="checkear_examen.php">
	<center>
		<div ng-repeat="pregunta in preguntas" class="card" style="margin: 0 auto !important;text-align: center !important;width: 50%!important;">
			<input type="text" name="idpregunta[]" value="{{pregunta.idpregunta}}" hidden="true"/>
			<input type="text" name="solucion[]" value="{{pregunta.solucion}}" hidden="true"/>
			<div class="row">
				<h6 class="col offset-l3 l2 s10" style="margin-top: 34px !important;color: #0040FF;">Pregunta :</h6>
				<input class="col l4 offset-s2 s10" type="text" value="{{pregunta.pregunta}}" disabled="" style="border: 0;background-color: white;color: #2196F3;"/>
			</div>
			<div class="row">
				<input class="checkbox" type="checkbox" name="seleccionada[]" id="{{pregunta.idpregunta}}_1" value="1" style="visibility: hidden;"/>
	            <label class="col offset-l4 l1 offset-s1 s1" ng-click="checked($event);" for="{{pregunta.idpregunta}}_1" style="top: 30px;"></label>
				<input class="col offset-l1 l4 offset-s2 s4" type="text" disabled="" value="{{pregunta.opcion1}}" style="border: 0;background-color: white;color: #2196F3;"/>
			</div>
			<div class="row">
					<input class="checkbox" type="checkbox" name="seleccionada[]" id="{{pregunta.idpregunta}}_2" value="2" style="visibility: hidden;"/>
	                <label class="col offset-l4 l1 offset-s1 s1" ng-click="checked($event);" for="{{pregunta.idpregunta}}_2" style="top: 30px;"></label>
					<input class="col offset-l1 l4 offset-s2 s4" type="text" disabled="" value="{{pregunta.opcion2}}" style="border: 0;background-color: white;color: #2196F3;"/>
			</div>
			<div class="row">
					<input class="checkbox" type="checkbox" name="seleccionada[]" id="{{pregunta.idpregunta}}_3" value="3" style="visibility: hidden;"/>
	                <label class="col offset-l4 l1 offset-s1 s1" ng-click="checked($event);" for="{{pregunta.idpregunta}}_3" style="top: 30px;"></label>
					<input class="col offset-l1 l4 offset-s2 s4" type="text" disabled="" value="{{pregunta.opcion3}}" style="border: 0;background-color: white;color: #2196F3;"/>
			</div>
		</div>
	</center>
	<input type="submit" class="col offset-l3 l6 col offset-s3 s6" id="finalizar" name="finalizar" value="Finalizar" style="float:none;padding: 10px;text-align: center;margin-top: 10px;">
</form>


	