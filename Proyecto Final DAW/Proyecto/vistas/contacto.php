<div class="container">
  <div class="row header">
    <h2 style="color: #2196F3;">CONTACTANOS &nbsp;</h2>
  </div>
  <div class="">
    <form action="contacta.php" method="POST">
      <ul>
        <li>
        	<div class="row">
	        	<p class="col l5">
	        		<label for="nombre" style="width: 100px;color: #2196F3;">Nombre: </label>
	            	<input type="text" name="nombre" placeholder="Pepe" />
	          	</p>
	          	<p class="col offset-l1 l5">
	            	<label for="apellidos" style="width: 100px;color: #2196F3;">Apellidos: </label>
	            	<input type="text" name="apellidos" placeholder="Martinez Aguado" />      
	          	</p>
         	</div>
        </li>
        
        <li>
        	<div class="row">
          		<p class="col l11">
          	  		<label for="email" style="width: 100px;color: #2196F3;">Email: <span class="req">*</span></label>
            		<input type="email" name="email" placeholder="usr@host" />
          		</p>
          	</div>
        </li>        
        <li>
        	<div class="col l11">
	          		<label for="comments" style="color: #2196F3;">Comentario: </label>
	          		<textarea name="comments" style="background-color:#f9f9f9 ; border-radius: 10px; min-height: 250px;border:1 solid #6E6E6E"></textarea>
          	</div>
        </li>
        
        <li>
        	<div class="col l11">
          		<input class="btn btn-submit" type="submit" value="Enviar" style="line-height: 0px;margin: 10px auto;"/>
          	</div>
        </li>
        
      </ul>
    </form>  
  </div>
</div>
