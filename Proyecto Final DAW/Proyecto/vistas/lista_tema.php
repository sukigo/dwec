<div class="section scrollspy" id="work">
    <div class="container">
        <h2 class="header text_b">Temas Libres</h2>
        <div class="row">
        	<div ng-if="noTemas"><h4 style="text-align: center;color: #2196F3;min-height: 200px;">Aún no disponemos de temas activos.</h4></div>
            <div class="col s12 m4 l4" ng-repeat="tema in temas">
                <div class="card"  style="height: 400px;">
                    <div class="card-image waves-effect waves-block waves-light">
                        <img class="activator" ng-src="{{tema.imagen}}" style="height: 270px;">
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4">{{tema.titulo}}<i class="mdi-navigation-more-vert right"></i></span>
                        <p ng-if="tema.inscrito == true"><a href="http://fct2016daw.no-ip.org/Proyecto/inscribirseTema.php?idtema={{tema.idtema}}">Inscribirse</a></p>
                        <p ng-if="tema.inscrito == false"><a href="http://fct2016daw.no-ip.org/Proyecto/#/temas/{{tema.idtema}}">Ir al tema</a></p>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4">{{tema.titulo}} <i class="mdi-navigation-close right"></i></span>
                        <p>{{tema.descripcion}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>