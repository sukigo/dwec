<?php
	session_start();
	if(isset($_REQUEST['mostrar']))
	{
		echo json_encode($_SESSION['resultados']);
	}
	
	if(isset($_POST) && $_POST != null)
	{
		foreach($_POST['idpregunta'] as $key => $valor)
		{
			$arr_resultado[$key]['idpregunta'] = $valor;
		}
		
		foreach($_POST['solucion'] as $key => $valor)
		{
			$arr_resultado[$key]['solucion'] = $valor;
		}
		
		foreach($_POST['seleccionada'] as $key => $valor)
		{
			$arr_resultado[$key]['seleccionada'] = $valor;
		}
		
		if(isset($arr_resultado))
		{
			$correctas = 0;
			$incorrectas = 0;
			foreach ($arr_resultado as $pregunta => $resultados)
			{
				if($resultados['seleccionada'] == $resultados['solucion'])
				{
					$correctas++;
				}
				else
				{
					$incorrectas++;
				}
			}
			
			$arr_final = array(
				'correctas' => $correctas,
				'incorrectas' => $incorrectas
			);
			$_SESSION['resultados'] = $arr_final;
			header('Location: http://fct2016daw.no-ip.org/Proyecto/#/resultados');
		}
	}
	
		
?>