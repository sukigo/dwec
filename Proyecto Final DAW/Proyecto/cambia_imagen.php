<?php
require("clases/BD.php");

session_start();
$_SESSION['registro'] = true;
$bd = BD::getInstancia();
$filtra = explode(".", $_FILES['imagen']['name']);
$extension = end($filtra);
if ($extension == "jpg" || $extension == "png" || $extension == "gif" || $extension == "bmp") {
	if (is_uploaded_file($_FILES['imagen']['tmp_name'])) {
		$dir_subida = '/var/www/html/Proyecto/img/';
		$fichero_subido = $dir_subida . basename($_FILES['imagen']['name']);
		if (move_uploaded_file($_FILES['imagen']['tmp_name'], $fichero_subido)) {
			$_SESSION['datos_verificacion']['imagen'] = 'img/' . $_FILES['imagen']['name'];
			$usuario = $bd->cambiarImagen($_SESSION['correo'], $_SESSION['datos_verificacion']['imagen']);
			if ($usuario['mensaje'] == 1) {
				$_SESSION['user']['imagen'] = $usuario['imagen'];
				$_SESSION['mensaje'] = "Se ha modificado correctamente.";

			} else {
				$_SESSION['mensaje'] = "Ha ocurrido un error con el registro, vuelva a intentarlo.";

			}
		} else {
			$_SESSION['datos_verificacion']['imagen'] = 'img/ico' . $_SESSION['datos_verificacion']['tipo'] . '.png';
		}

		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/user_panel");
	} else {

		$_SESSION['mensaje'] = "Ha ocurrido un error, no se ha podido subir.";
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/user_panel");
	}
}else{
	$_SESSION['mensaje'] = "Solo puedes subir imagenes en formato png, jpg, bmp y gif.";
	header("Location: http://fct2016daw.no-ip.org/Proyecto/#/user_panel");
}
?>






