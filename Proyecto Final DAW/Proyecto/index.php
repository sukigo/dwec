<?php
	session_start();
	require_once 'lib/hybridauth/index.php';
?>

<!DOCTYPE html>
<html ng-app="E-Ducar">
<head>
    <meta charset="utf-8"/> 
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <meta name="theme-color" content="#2196F3">
    <!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	
    <title>E-Ducar</title>
    
 	<!-- AngularJS -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.js"></script>
	
	<!--google charts-->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
	
    <!-- CSS  -->
    <link href="min/plugin-min.css" type="text/css" rel="stylesheet">
    <link href="min/custom-min.css" type="text/css" rel="stylesheet">
    <link href="css/style.css" type="text/css" rel="stylesheet">
</head>
<body id="top" class="scrollspy">
	<div id="cabecera">
		<!--Navigation-->
		 <div class="navbar-fixed">
		    <nav id="nav_f" class="default_color" role="navigation">
		        <div class="container">
		            <div class="nav-wrapper">
		            <a href="#/" id="logo-container" class="brand-logo">E-Ducar</a>
		                <ul class="right hide-on-med-and-down">
		                	<?php
		                		//Realizamos cambios en la barra de navegacion dependiendo del estado del usuario
			                	if(isset($_SESSION['user']))
								{
									echo '<li><a href="#/user_panel">'.$_SESSION['user']['nick'].'</a></li>';
								}
								else
								{
									echo '<li><a href="#/empezar">Empezar</a></li>';
								}
							
		                    ?>
		                    <li><a href="#/temas">Temas</a></li>
		                    <li><a href="#/cursos">Cursos</a></li>
		                    <li><a href="#/equipo">Equipo</a></li>
		                    <li><a href="#/contacto">Contacto</a></li>
		                    <?php
		                		//Realizamos cambios en la barra de navegacion dependiendo del estado del usuario
			                	if(isset($_SESSION['user']))
								{
									echo '<li><a href="logout.php"><i class="fa fa-power-off white-text" aria-hidden="true"></i></a></li>';
								}
		                    ?>
		                </ul>
		                <ul id="nav-mobile" class="side-nav">
		                    <?php
		                		//Realizamos cambios en la barra de navegacion para otros dispositivos dependiendo del estado del usuario
			                	if(isset($_SESSION['user']))
								{
									echo '<li><a href="#/user_panel">'.$_SESSION['user']['nick'].'</a></li>';
								}
								else
								{
									echo '<li><a href="#/empezar">Empezar</a></li>';
								}
		                    ?>
		                    <li><a href="#/temas">Temas</a></li>
		                    <li><a href="#/cursos">Cursos</a></li>
		                    <li><a href="#/equipo">Equipo</a></li>
		                    <li><a href="#/contacto">Contacto</a></li>
		                    <?php
		                		//Realizamos cambios en la barra de navegacion dependiendo del estado del usuario
			                	if(isset($_SESSION['user']))
								{
									echo '<li><a href="logout.php">Cerrar Sesion</a></li>';
								}
		                    ?>
		                </ul>
		            <a href="javascript:void(0);" data-activates="nav-mobile" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
		            </div>
		        </div>
		    </nav>
		</div>
	</div>

	<!--Banner-->
	<div class="section no-pad-bot" id="index-banner" style="display: none !important;">
	    <div class="container">
	        <h1 class="text_h center header cd-headline letters type">
	            <span>Me gusta </span> 
	            <span class="cd-words-wrapper waiting">
	                <b class="is-visible">E-Ducar</b>
	                <b>aprender</b>
	                <b>enseñar</b>
	                <b>compartir</b>
	            </span>
	        </h1>
	    </div>
	</div>
	
	<!--Dialog-->
	<?php
		if(isset($_SESSION['registro']))
		{
			echo '<div class="modal" id="modal" style="overflow: hidden;">
				    <div class="inner-modal">
				      <div class="content">
				        <p style="margin: 15px auto;">'.$_SESSION['mensaje'].'</p>
				      </div>
				    </div>
				  </div>';
			unset($_SESSION['mensaje']);
		}
		
	?>
	
	
	<!--Cuerpo-->
	<div id="cuerpo" class="row" ng-view autoscroll="true">
		
	</div>
	
	
	<!--Footer-->
	<footer id="contact" class="page-footer default_color scrollspy">
	    <div class="container">  
	        <div class="row">
	            <div>
	                <ul style="display: inline">
	                    <li class="col offset-s3 s3">
	                        <a class="white-text" href="https://www.facebook.com">
	                            <i class="small fa fa-facebook-square white-text"></i> Facebook
	                        </a>
	                    </li>
	                    <li class="col s3">
	                        <a class="white-text" href="https://www.twitter.com">
	                            <i class="small fa fa-twitter-square white-text"></i> Twitter
	                        </a>
	                    </li>
	                    <li class="col  s3">
	                        <a class="white-text" href="https://plus.google.com">
	                            <i class="small fa fa-google-plus-square white-text"></i> Google+
	                        </a>
	                    </li>	                    
	                </ul>
	            </div>
	        </div>
	    </div>
	    <div class="footer-copyright default_color">
	        <div class="container" style="text-align: right">
	            Realizado por Ibán Jesús Romera Román y Aldo Guerrero García.
	        </div>
	    </div>
	</footer>
	<style type="text/css">
		.navbar-fixed a:hover {
			color:white;
		    text-decoration: underline;
		}
		.navbar-fixed a:focus{
			color:white;
			text-decoration: none;
		}
	</style>
	<?php
		if(isset($_SESSION['registro']))
		{
			echo '<div class="modal-backdrop" style="opacity: 0.5 !important;"></div>';
			unset($_SESSION['registro']);
		}
	?>
    <!--  Scripts-->
    <script src="min/plugin-min.js"></script>
    <script src="min/custom-min.js"></script>
    <!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.6 -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- Morris.js charts -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	<script src="plugins/morris/morris.min.js"></script>
	<!-- Sparkline -->
	<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- jQuery Knob Chart -->
	<script src="plugins/knob/jquery.knob.js"></script>
	<!-- daterangepicker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
	<script src="plugins/daterangepicker/daterangepicker.js"></script>
	<!-- datepicker -->
	<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
	<!-- Slimscroll -->
	<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="dist/js/app.min.js"></script>
	<!--Angular JS-->
    <script src="js/angular.js"></script>
</body>
</html>
