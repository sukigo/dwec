<?php
require("clases/BD.php");

session_start();

$bd = BD::getInstancia();
if (isset($_SESSION['user'])) {
	if ($_SESSION['user']['idtipousuario']==3) {
		$filtra = explode(".", $_FILES['imagen']['name']);
		$extension = end($filtra);
		if ($extension == "jpg" || $extension == "png" || $extension == "gif" || $extension == "bmp") {
			if (is_uploaded_file($_FILES['imagen']['tmp_name'])) {
				$dir_subida = '/var/www/html/Proyecto/img/';
				$fichero_subido = $dir_subida . basename($_FILES['imagen']['name']);

				if (move_uploaded_file($_FILES['imagen']['tmp_name'], $fichero_subido)) {
					$imagen = 'img/' . $_FILES['imagen']['name'];
				} else {
					$imagen = 'img/icocurso.png';
				}
				if ($bd->modificaImagenCurso($_REQUEST['idcurso'], $_SESSION['user']['idusuario'], $imagen)) {
					$_SESSION['registro'] = true;
					$_SESSION['mensaje'] = "Se ha modificado correctamente.";

				} else {
					$_SESSION['registro'] = true;
					$_SESSION['mensaje'] = "Ha ocurrido un error con el registro, vuelva a intentarlo.";

				}
				header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos/" . $_REQUEST['idcurso']);
			} else {
				$_SESSION['registro'] = true;
				$_SESSION['mensaje'] = "Ha ocurrido un error, no se ha podido subir.";
				header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos/" . $_REQUEST['idcurso']);
			}
		}else{
			$_SESSION['registro'] = true;
			$_SESSION['mensaje'] = "Solo se pueden subir imagenes en formato png, jpg, bmp y gif.";
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos/" . $_REQUEST['idcurso']);
		}
		}else{
			$_SESSION['registro'] = true;
			$_SESSION['mensaje'] = "Solo profesor.";
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/user_panel");
		}
	}else{

		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/");
	}
	?>
