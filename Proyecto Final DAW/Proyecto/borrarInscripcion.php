
<?php
require("clases/BD.php");

session_start();

$bd = BD::getInstancia();
$_SESSION['registro']=true;
if(isset($_SESSION['user'])){
	if ($_SESSION['user']['idtipousuario']!=3){
		if(isset($_REQUEST['idtema']))
		{

			if ($bd->bajaTemaUsuario($_SESSION['user']['idusuario'], $_REQUEST['idtema']))
			{
				$_SESSION['mensaje'] = "Se ha dado de baja correctamente.";
				header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres");
			}
			else
			{
				$_SESSION['mensaje'] = "El usuario ya se dio de baja anteriormente";
				header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres");
			}

		}
		elseif(isset($_REQUEST['idcurso']))
		{
			if ($bd->bajaCursoUsuario($_SESSION['user']['idusuario'], $_REQUEST['idcurso']))
			{
				$_SESSION['mensaje'] = "Se ha dado de baja correctamente.";
				header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos");
			}
			else
			{
				$_SESSION['mensaje'] = "El usuario ya se dio de baja anteriormente";
				header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos");
			}
		}
		else
		{
			$_SESSION['mensaje'] = "No hay nada que pasarme?.";
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres");
		}
	}else{

		$_SESSION['mensaje'] = "Tu usuario no puede usar este fichero";
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres");

	}
}else{

	$_SESSION['mensaje'] = "Inicia Sesión.";
	header("Location: http://fct2016daw.no-ip.org/Proyecto/#/");


}
?>


