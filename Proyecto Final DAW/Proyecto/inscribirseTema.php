
<?php
require("clases/BD.php");

session_start();

$bd = BD::getInstancia();
$_SESSION['registro']=true;
if(isset($_SESSION['user'])){
	if ($_SESSION['user']['idtipousuario']!=3){
		if (isset($_REQUEST)){

			if ($bd->inscribirseTema($_SESSION['user']['idusuario'], $_REQUEST['idtema']))
			{
				$_SESSION['mensaje'] = "Se ha Inscrito correctamente.";
				header("Location: http://fct2016daw.no-ip.org/Proyecto/#/temas");
			}
			else
			{
				$_SESSION['mensaje'] = "Ya estas inscrito en este tema.";
				header("Location: http://fct2016daw.no-ip.org/Proyecto/#/temas");
			}

		}else{
			$_SESSION['mensaje'] = "No hay nada que pasarme?.";
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/temas");
		}
	}else{

		$_SESSION['mensaje'] = "Solo pueden inscribirse a temas los alumnos y usuarios libres";
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/temas");

	}
}else{

	$_SESSION['mensaje'] = "Inicia Sesión.";
	header("Location: http://fct2016daw.no-ip.org/Proyecto/#/temas");


}
?>


