<?php
require_once 'clases/BD.php';
$bd=BD::getInstancia();
session_start();
if (isset($_SESSION['user'])){
	$_SESSION['registro'] = true;
	if ($_SESSION['user']['idtipousuario']==3) {
		if (isset($_REQUEST['idcurso'])) {
			
			if ($bd->borrarCurso($_REQUEST['idcurso'], $_SESSION['user']['idusuario'])) {
				$_SESSION['mensaje'] = "Se ha borrado correctamente.";
			} else {
				$_SESSION['mensaje'] = "Ha ocurrido un error, el curso se ha borrado con anterioridad a este.";
			}
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos");
		}else{
			$_SESSION['mensaje'] = "Te olvidas pasarme algo?";
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/user_panel");
		}
	}else{
		$_SESSION['mensaje'] = "Solo profesores pueden borrar";
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/user_panel");
	}

}else{

	header("Location: http://fct2016daw.no-ip.org/Proyecto/#/");

}

?>
