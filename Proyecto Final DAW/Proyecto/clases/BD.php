<?php
Class BD extends MySQLi{

	private $servidor='';
	private $usuario='';
	private $password='';
	private $base_datos='Formacion';
	private $link;
	private $stmt;
	private $stmt2;
	static $_instancia;

	private function BD(){
		$this->conectar();
	}

	private function __clone(){ }

	public static function getInstancia(){
		if (!(self::$_instancia instanceof self)){
			self::$_instancia=new self();
		}
		return self::$_instancia;
	}


	private function conectar(){
		$this->link=mysqli_connect($this->servidor, $this->usuario, $this->password, $this->base_datos);

		@mysqli_query("SET NAMES 'utf8'");
	}

	public function iniciaSesionEstandar($correo, $clave){
		$usuario = array();
		$this->stmt=mysqli_query($this->link, "SELECT count(*) as valido FROM usuario where correo='$correo' and clave=PASSWORD('$clave')");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']==1){
					$this->stmt=mysqli_query($this->link, "SELECT * FROM usuario where correo='$correo' and activa=true");
					$contador=mysqli_num_rows($this->stmt);

					if($contador != 0){
						if($ro = mysqli_fetch_array($this->stmt)){
							$usuario['idusuario']=$ro['idusuario'];
							$usuario['nick']= $ro['nick'];
							$usuario['nombre'] = $ro['nombre'];
							$usuario['correo']= $correo;
							$usuario['idtipousuario']= $ro['idtipousuario'];
							$usuario['imagen']= $ro['imagen'];
							$usuario['tipo'] = 1;
						}

					}else{
						$usuario['tipo'] = 2;
					}
				}else{
					$usuario['tipo']= 3;
				}
			}
		}
		return $usuario;
	}
				public function iniciaSesionSocial($nombre, $correo ,$tipo ,$identificador){
		$this->stmt=mysqli_query($this->link, "SELECT $tipo as tipo, idusuario, nick, idtipousuario, imagen, nombre FROM usuario where correo='$correo'");
		$contador=mysqli_num_rows($this->stmt);
		$usuario = array();
		if($contador != 0){
				if ($ro = mysqli_fetch_array($this->stmt)) {
					if ($ro['nombre'] == null || $ro['nombre'] == "") {
						$usuario['nombre'] = $nombre;
					} else {
						$usuario['nombre'] = $ro['nombre'];
					}
					$usuario['nick'] = $ro['nick'];
					$usuario['correo'] = $correo;
					$usuario['idtipousuario'] = $ro['idtipousuario'];
					$usuario['imagen'] = $ro['imagen'];
					$usuario['tipo'] = 1;
					$usuario['idusuario'] = $ro['idusuario'];
                    if ($ro['tipo'] == null) {
                        mysqli_query($this->link, "Update usuario set $tipo=PASSWORD('$identificador') where correo = '$correo'");
                    } else {
                        if (!$this->compruebaIdentificadorSocial($correo, $identificador, $tipo)) {
                            $usuario['tipo'] = 3;
                        }
                    }
					$this->stmt3 = mysqli_query($this->link, "select count(*) as tipo from  usuario where correo='$correo' and activa=true");
					$contador = mysqli_num_rows($this->stmt3);
					if ($contador != 0) {
						if ($ro3 = mysqli_fetch_array($this->stmt3)) {
							if ($ro3['tipo'] == 0) {
								$usuario['tipo'] = 2;
							}
						}
					}
					

				}
			}else{
                $usuario['tipo']=3;
        }
				
		
		
		return $usuario;
		
	}
	public function compruebaIdentificadorSocial($correo, $identificador, $tipo)
	{
		$this->stmt2 = mysqli_query($this->link, "select count(*) as tipo from  usuario where correo='$correo' and $tipo=PASSWORD('$identificador')");
		$contador2 = mysqli_num_rows($this->stmt2);
		if ($contador2 != 0) {
			if ($ro2 = mysqli_fetch_array($this->stmt2)) {
					if ($ro2['tipo']!= 0){
						return true;
					}else{
						return false;
					}
			}
		}
	}
//funcion que se llamara cuando se proceda a ver si la verificacion es valida
	public function compruebaVerificacion($pass){
		$usuario = array();
		$this->stmt=mysqli_query($this->link, "select count(*) as valido, nombre, nick, imagen, correo, clave, idusuario from  usuario where verificacion=PASSWORD('$pass')");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']==1){
					$usuario['imagen'] = $ro['imagen'];
					$usuario['nombre'] = $ro['nombre'];
					$usuario['nick'] = $ro['nick'];
					$usuario['idusuario'] = $ro['idusuario'];
					$usuario['correo'] = $ro['correo'];
					$usuario['clave'] = $ro['clave'];

					return $usuario;
				}else{
					return false;
				}

			}

		}

	}
//funcion que activara el usuario
	public function activacionUsuario($idusuario){
		if (mysqli_query($this->link,"Update usuario set verificacion=null, activa=true, fecha_verificacion=null where idusuario=$idusuario")){
			return true;
		}else{
			return false;
		}
	}


//registra usuario por medios sociales
	public function registrarUsuarioSocial($usuario, $email,$proveedor,$token, $pass){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from  usuario where correo='$email'");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']==0){
					mysqli_query($this->link,"Insert into usuario (nombre, correo,$proveedor, verificacion, fecha_verificacion) values('$usuario', '$email',PASSWORD('$token'), PASSWORD('$pass'), now())");
					return true;
				}else{
					return false;
				}
			}
		}
	}
	public function volverVerificar($email, $pass){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from  usuario where correo='$email'");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					mysqli_query($this->link,"Update usuario set verificacion=PASSWORD('$pass'), fecha_verificacion=now() where correo='$email'");
					return true;
				}else{
					return false;
				}
			}
		}
	}


	public function cambiarImagen($correo, $imagen){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from  usuario where correo='$correo' and activa=true");
		$contador=mysqli_num_rows($this->stmt);
		$usuario= array();
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					mysqli_query($this->link,"Update usuario set imagen='$imagen' where correo = '$correo'");
					$usuario['mensaje']=1;
					$usuario['imagen']=$imagen;
				}else{
					$usuario['mensaje']= 0;
				}

			}

		}
		return $usuario;
	}

//esta se usa solo cuando se verifica (dado que el usuario no tiene clave anterior)
	public function ponerClave($correo, $clave){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from  usuario where correo='$correo' and activa=true");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					mysqli_query($this->link,"Update usuario set clave=PASSWORD('$clave') where correo = '$correo'");
					return true;
				}else{
					return false;
				}
			}

		}

	}


	public function listaTemas($idusuario, $tipotema){
		$this->stmt=mysqli_query($this->link, "select idtema, tipotema, idusuario, titulo, descripcion, imagen, activo, date(fecha_creacion) as fecha_creacion from  tema where idusuario=$idusuario and tipotema=$tipotema");
		$contador=mysqli_num_rows($this->stmt);
		$tema = array();
		$cuenta=0;
		if($contador != 0){
			while($ro = mysqli_fetch_array($this->stmt)){
				$tema[$cuenta]= array();
				$tema[$cuenta]['idtema']= $ro['idtema'];
				$tema[$cuenta]['tipotema']= $ro['tipotema'];
				$tema[$cuenta]['idusuario']= $ro['idusuario'];
				$tema[$cuenta]['titulo']= $ro['titulo'];
				$tema[$cuenta]['descripcion']= $ro['descripcion'];
				$tema[$cuenta]['imagen']= $ro['imagen'];
				$tema[$cuenta]['activo']= $ro['activo'];
				$tema[$cuenta]['fecha_creacion']= $ro['fecha_creacion'];
				$cuenta++;

			}

			return $tema;
		}


	}
//borrar recursos
	public function borrarRecursos($idusuaro, $idtema, $recursos){
		$this->stmt=mysqli_query($this->link, "select r.idrecurso, r.idtema, r.nombre, r.recurso, r.fecha, r.activo from recurso_tema as r, tema as t where t.idtema=$idtema and r.idtema=t.idtema and idusuario=$idusuaro");
		$contador=mysqli_num_rows($this->stmt);
		$cuenta=0;
		if($contador != 0){
			while(sizeof($recursos)>$cuenta){
				if($stm = mysqli_query($this->link, "select rt.recurso, count(rt.recurso) as valido from examen as e, recurso_tema as rt where rt.idtema=$idtema and e.idexamen=rt.recurso"))
				{
					$contador3=mysqli_num_rows($stm);
					if($contador3 != 0)
					{
						if($ro2 = mysqli_fetch_array($stm))
						{
							if($ro2['valido'] != 0)
							{
								mysqli_query($this->link,"DELETE FROM examen where idexamen = ".$ro2['recurso']);
								mysqli_query($this->link,"DELETE FROM pregunta where idexamen = ".$ro2['recurso']);
							}
						}	
					}
				}
				mysqli_query($this->link,"delete from recurso_tema where idtema=$idtema and idrecurso=$recursos[$cuenta] ");
				$cuenta++;
			}
			$this->stmt2=mysqli_query($this->link, "select count(*) as valido from recurso_tema where idtema=$idtema");
			$contador2=mysqli_num_rows($this->stmt2);
			if($contador2 != 0)
			{
				if($ro = mysqli_fetch_array($this->stmt2))
				{
					if($ro['valido'] == 0)
					{
						mysqli_query($this->link,"UPDATE tema SET activo=false where idtema=$idtema ");
						mysqli_query($this->link,"DELETE FROM tema_inscrito where idtema=$idtema ");
					}
				}
			}
			return true;
		}else{
			return false;
		}
	}
	public function listaPorTema($idtema, $idusuario){
		$this->stmt=mysqli_query($this->link, "select idtema, idusuario, tipotema, titulo, descripcion, imagen, activo, date(fecha_creacion) as fecha_creacion from tema where idtema=$idtema and idusuario=$idusuario");
		$contador=mysqli_num_rows($this->stmt);
		$tema= array();
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				$tema['idtema']=$ro['idtema'];
				$tema['idusuario']=$ro['idusuario'];
				$tema['tipotema']=$ro['tipotema'];
				$tema['titulo']=$ro['titulo'];
				$tema['descripcion']=$ro['descripcion'];
				$tema['imagen']=$ro['imagen'];
				$tema['activo']=$ro['activo'];
				$tema['fecha_creacion']=$ro['fecha_creacion'];
				$this->stmt=mysqli_query($this->link, "select idrecurso, idtema, nombre, recurso, date(fecha) as fecha, activo from recurso_tema where idtema=$idtema");
				$contador=mysqli_num_rows($this->stmt);
				if($contador != 0){
					$recurso = array();
					$cuenta=0;
					while($ro = mysqli_fetch_array($this->stmt)){
						$recurso[$cuenta]= array();
						$recurso[$cuenta]['idrecurso'] = $ro['idrecurso'];
						$recurso[$cuenta]['idtema'] = $ro['idtema'];
						$recurso[$cuenta]['nombre'] = $ro['nombre'];
						$recurso[$cuenta]['recurso'] = $ro['recurso'];
						$recurso[$cuenta]['fecha'] = $ro['fecha'];
						$recurso[$cuenta]['activo'] = $ro['activo'];
						$cuenta++;
					}
					$tema['recurso'] = $recurso;
				}else{
					$tema['recurso'] = null;
				}
			}
		}


		return $tema;
	}
//modifica imagen del tema
	public function modificaImagenTema($idtema, $idusuario, $imagen){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from tema where idtema=$idtema and idusuario=$idusuario");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					mysqli_query($this->link,"update tema set imagen='$imagen' where idtema = $idtema");
					return true;
				}else{
					return false;
				}

			}

		}

	}
	//modifica imagen curso
	public function modificaImagenCurso($idcurso, $idusuario, $imagen){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from curso where idcurso=$idcurso and idusuario=$idusuario");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					mysqli_query($this->link,"update curso set imagen='$imagen' where idcurso = $idcurso");
					return true;
				}else{
					return false;
				}

			}

		}

	}
//modifica tema por el id
	public function modificaTema($idtema, $idusuario, $titulo, $descripcion){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from tema where idtema=$idtema and idusuario=$idusuario");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					mysqli_query($this->link,"update tema set titulo='$titulo', descripcion='$descripcion' where idtema = $idtema");
					return true;
				}else{
					return false;
				}

			}

		}

	}
	
	//modifica tema por el id
	public function modificaCurso($idcurso, $idusuario, $titulo, $descripcion){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from curso where idcurso=$idcurso and idusuario=$idusuario");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					mysqli_query($this->link,"update curso set titulo='$titulo', descripcion='$descripcion' where idcurso = $idcurso");
					return true;
				}else{
					return false;
				}

			}

		}

	}
	
	//borra curso
	public function borrarCurso($idcurso, $idusuario){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from curso where idcurso=$idcurso and idusuario=$idusuario");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					mysqli_query($this->link,"delete from curso where idcurso=$idcurso and idusuario=$idusuario");
					mysqli_query($this->link,"delete from curso_inscrito where idcurso=$idcurso");
					$this->stmt=mysqli_query($this->link, "select idtema from linea_tema_curso where idcurso=$idcurso");
					$contador=mysqli_num_rows($this->stmt);
					if($contador != 0){
						while($ro = mysqli_fetch_array($this->stmt)){
							mysqli_query($this->link,"delete from tema where idtema=".$ro['idtema']." and idusuario=$idusuario and tipotema=3");
							mysqli_query($this->link,"delete from recurso_tema where idtema=".$ro['idtema']);
							mysqli_query($this->link,"delete from recurso_plantilla where idtema=".$ro['idtema']);
							mysqli_query($this->link,"delete from tema_inscrito where idtema=".$ro['idtema']);

						}


					}
					return true;
				}else{
					return false;
				}

			}

		}
	}
	public function listaCurso($idcurso, $idtipousuario, $idusuario){
		$this->stmt=mysqli_query($this->link, "select idcurso, titulo, descripcion, imagen, date(fecha_creacion) as fecha from curso where idcurso=$idcurso and idusuario=$idusuario");
		$contador=mysqli_num_rows($this->stmt);
		$curso= array();
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				$curso['idcurso']=$ro['idcurso'];
				$curso['titulo']=$ro['titulo'];
				$curso['descripcion']=$ro['descripcion'];
				$curso['imagen']=$ro['imagen'];
				$curso['fecha']=$ro['fecha'];
				//select idtema, titulo, descripcion, imagen, date(fecha_creacion) as fecha from recurso_tema where idtema=$idtema
				$this->stmt=mysqli_query($this->link, "select * from linea_tema_curso where idcurso=$idcurso");
				$contador=mysqli_num_rows($this->stmt);
				$cuenta=0;
				if($contador != 0){
					$curso['tema']=array();
					$tema=array();
					while($ro = mysqli_fetch_array($this->stmt)){
						$this->stmt2=mysqli_query($this->link, "select idtema, titulo, date(fecha_creacion) as fecha  from tema where idtema=".$ro['idtema']);
						$contador2=mysqli_num_rows($this->stmt2);
						if($contador2 != 0){
							while($ro2 = mysqli_fetch_array($this->stmt2)){
								$tema[$cuenta]=array();
								$tema[$cuenta]['idtema']=$ro2['idtema'];
								$tema[$cuenta]['titulo']=$ro2['titulo'];
								$tema[$cuenta]['fecha']=$ro2['fecha'];
								$cuenta++;
							}
						}
					}
					$curso['tema'] = $tema;
				}else{
					$curso['tema'] = null;
				}
			}
		}


		return $curso;
	}

//crear examen
	public function crearExamen($idtema,$nombre,$arr_preguntas)
	{
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from tema where idtema=$idtema");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0)
				{
					try
					{
						mysqli_query($this->link,"INSERT INTO examen (idtema,nombre,fecha) values($idtema,'$nombre', now())");
						$idexamen = mysqli_insert_id($this->link);
						foreach($arr_preguntas as $key => $pregunta)
						{
							$sql =  "INSERT INTO pregunta (idexamen,pregunta,opcion1,opcion2,opcion3,solucion) values($idexamen,'".$pregunta['pregunta']."','".$pregunta['opcion1']."','".$pregunta['opcion2']."','".$pregunta['opcion3']."',".$pregunta['correcta'].")";
							mysqli_query($this->link,$sql);
						}
						
						if(mysqli_query($this->link,"INSERT INTO recurso_tema (idtema, nombre, recurso, fecha, activo) values($idtema, '$nombre', '$idexamen', now(), true)"))
						{
							if(mysqli_query($this->link,"UPDATE tema SET activo = true where idtema = $idtema"))
							{
								return true;
							}
							else
							{
								return false;
							}
						}
						else
						{
							return false;
						}
					}
					catch (Exception $e)
					{ 
					  echo $e->errorMessage();
					  return false;
					} 
				}else{
					return false;
				}
			}
		}
	}
//comprobar si es tema libre o tema de un curso
	public function isLibre($idtema)
	{
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from tema where idtema=$idtema and tipotema=1");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}

//listar preguntas examen
	public function listaPreguntas($idexamen)
	{
		$this->stmt=mysqli_query($this->link, "select * from pregunta where idexamen=$idexamen");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0)
		{
			$key = 0;
			while($ro = mysqli_fetch_array($this->stmt))
			{
				$pregunta[$key]['idpregunta'] 	= $ro['idpregunta'];
				$pregunta[$key]['idexamen'] 	= $ro['idexamen'];
				$pregunta[$key]['pregunta'] 	= $ro['pregunta'];
				$pregunta[$key]['opcion1'] 		= $ro['opcion1'];
				$pregunta[$key]['opcion2'] 		= $ro['opcion2'];
				$pregunta[$key]['opcion3'] 		= $ro['opcion3'];
				$pregunta[$key]['solucion'] 	= $ro['solucion'];
				$key++;
			}
			if($pregunta)
			{
				return	$pregunta;
			}
			else
			{
				return false;
			}
			
		}
	}

// borra el tema
	public function borrarTema($idtema, $idusuario){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from tema where idtema=$idtema and idusuario=$idusuario");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					try
					{
						mysqli_query($this->link,"delete from tema where idtema = $idtema");
						$this->stmt2=mysqli_query($this->link, "select idcurso from linea_tema_curso where idtema=$idtema");
						$contador2=mysqli_num_rows($this->stmt2);
						if($contador2 != 0){
							if($ro2 = mysqli_fetch_array($this->stmt2)){
								mysqli_query($this->link,"UPDATE curso SET activo = false WHERE idcurso=".$ro2['idcurso']);
								mysqli_query($this->link,"delete from linea_tema_curso where idtema = $idtema");
								mysqli_query($this->link,"delete from recurso_tema where idtema = $idtema");
							}
						}
						return true;
					} 
					catch (Exception $e)
					{ 
					  echo $e->errorMessage();
					  return false;
					} 
				}else{
					return false;
				}

			}

		}

	}


	public function listaTemasInscritos($idusuario){
		$this->stmt=mysqli_query($this->link, "select t.idtema, t.idusuario, t.tipotema, t.titulo, t.descripcion, t.imagen, date(t.fecha_creacion) as fecha_creacion from tema_inscrito as ti, tema as t where ti.idtema=t.idtema and t.activo =true and ti.idusuario=$idusuario and t.activo=true");
		$contador=mysqli_num_rows($this->stmt);
		$tema = array();
		$cuenta = 0;
		if($contador != 0){
			while($ro = mysqli_fetch_array($this->stmt)){
				$tema[$cuenta]=array();
				$tema[$cuenta]['idtema']=$ro['idtema'];
				$tema[$cuenta]['idusuario']=$ro['idusuario'];
				$tema[$cuenta]['tipotema']=$ro['tipotema'];
				$tema[$cuenta]['titulo']=$ro['titulo'];
				$tema[$cuenta]['descripcion']=$ro['descripcion'];
				$tema[$cuenta]['imagen']=$ro['imagen'];
				$tema[$cuenta]['fecha_creacion']=$ro['fecha_creacion'];
				$cuenta++;
			}

		}
		return $tema;
	}
//inscribirsetema
	public function inscribirseTema($idusuario, $idtema){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from  tema_inscrito  where idusuario=$idusuario and idtema=$idtema");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']==0){
					mysqli_query($this->link,"insert into tema_inscrito (idtema, idusuario, fecha) values($idtema, $idusuario, now())");
					return true;
				}else{
					return false;
				}

			}

		}
	}
//baja del tema
	public function bajaTemaUsuario($idusuario, $idtema){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from  tema_inscrito  where idusuario=$idusuario and idtema=$idtema");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					mysqli_query($this->link,"delete from tema_inscrito where idtema=$idtema and idusuario=$idusuario");
					return true;
				}else{
					return false;
				}

			}

		}
	}
	
	//baja del curso
	public function bajaCursoUsuario($idusuario, $idcurso){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from  curso_inscrito  where idusuario=$idusuario and idcurso=$idcurso");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					mysqli_query($this->link,"delete from curso_inscrito where idcurso=$idcurso and idusuario=$idusuario");
					return true;
				}else{
					return false;
				}

			}

		}
	}
	
	public function cambiarDatos($correo, $nombre, $nick){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from  usuario where correo='$correo' and activa=true");
		$contador=mysqli_num_rows($this->stmt);
		$usuario = array();
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					mysqli_query($this->link,"Update usuario set nombre='$nombre', nick='$nick' where correo = '$correo'");
					$usuario['mensaje']=1;
					$usuario['nombre']= $nombre;
					$usuario['nick']= $nick;
				}else{
					$usuario['mensaje']=0;
				}
				return $usuario;
			}

		}

	}
	//crea recurso
	public function nuevoRecurso($idtema, $nombre, $archivo, $idusuario){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from tema where idtema=$idtema and activo = false");
		$contador=mysqli_num_rows($this->stmt);
		$usuario = array();
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					mysqli_query($this->link,"update tema set activo=true where idtema=$idtema");			
				}
			}
			
		}
		if(mysqli_query($this->link,"insert into recurso_tema (idtema, nombre, recurso, fecha, activo) values($idtema, '$nombre', '$archivo', now(), true)")){
			return true;
		}else{
			return false;
		}
	}
//lista tema seleccionado por el usuario
	public function listaTemaSeleccionado($idusuario, $idtema){
		$this->stmt=mysqli_query($this->link, "select u.nick, u.imagen as imagenusuario, t.titulo, t.descripcion, t.imagen, date(t.fecha_creacion)as fecha_creacion from usuario as u, tema as t where t.idusuario=u.idusuario and t.idtema=$idtema");
		$contador=mysqli_num_rows($this->stmt);
		$tema = array();
		$cuenta = 0;
		if($contador != 0) {
			if ($ro = mysqli_fetch_array($this->stmt)) {
				$tema['usuario'] = array();
				$tema['usuario']['nick']=$ro['nick'];
				$tema['usuario']['imagenusuario']=$ro['imagenusuario'];
				$tema['tema']= array();
				$tema['tema']['titulo'] = $ro['titulo'];
				$tema['tema']['descripcion'] = $ro['descripcion'];
				$tema['tema']['imagen'] = $ro['imagen'];
				$tema['tema']['fecha'] = $ro['fecha_creacion'];
				$this->stmt2 = mysqli_query($this->link, "SELECT idrecurso, idtema, nombre, recurso, date(fecha) as fecha, activo FROM recurso_tema WHERE idtema=$idtema");
				$contador2 = mysqli_num_rows($this->stmt2);
				$recurso= array();
				if ($contador2 != 0) {
					
					while ($ro2 = mysqli_fetch_array($this->stmt2)) {
						$recurso[$cuenta]= array();
						$recurso[$cuenta]['idrecurso']=$ro2['idrecurso'];
						$recurso[$cuenta]['idtema']=$ro2['idtema'];
						$recurso[$cuenta]['nombre']=$ro2['nombre'];
						$recurso[$cuenta]['recurso']=$ro2['recurso'];
						$recurso[$cuenta]['fecha']=$ro2['fecha'];
						$recurso[$cuenta]['activo']=$ro2['activo'];
						$cuenta++;
					}
				}
				$tema['tema']['recurso']=$recurso;
			}

		}
		return $tema;

	}

//lista el curso seleccionado
public function listaCursoSeleccionado($idusuario, $idcurso){
		$this->stmt=mysqli_query($this->link, "select u.nick, u.imagen as imagenusuario, c.titulo, c.descripcion, c.imagen, date(c.fecha_creacion)as fecha_creacion from usuario as u, curso as c where c.idusuario=u.idusuario and c.idcurso=$idcurso and activo=true");
		$contador=mysqli_num_rows($this->stmt);
		$curso = array();
		$cuenta = 0;
		if($contador != 0) {
			if ($ro = mysqli_fetch_array($this->stmt)) {
				$curso['usuario'] = array();
				$curso['usuario']['nick']=$ro['nick'];
				$curso['usuario']['imagenusuario']=$ro['imagenusuario'];
				$curso['curso']= array();
				$curso['curso']['titulo'] = $ro['titulo'];
				$curso['curso']['descripcion'] = $ro['descripcion'];
				$curso['curso']['imagen'] = $ro['imagen'];
				$curso['curso']['fecha'] = $ro['fecha_creacion'];
				$this->stmt2 = mysqli_query($this->link, "SELECT t.idtema, t.titulo, date(t.fecha_creacion) as fecha, t.activo FROM tema as t, linea_tema_curso as ltc WHERE ltc.idcurso = $idcurso and t.idtema = ltc.idtema");
				$contador2 = mysqli_num_rows($this->stmt2);
				$temas= array();
				if ($contador2 != 0) {
					
					while ($ro2 = mysqli_fetch_array($this->stmt2)) {
						$temas[$cuenta]= array();
						$temas[$cuenta]['idtema']=$ro2['idtema'];
						$temas[$cuenta]['titulo']=$ro2['titulo'];
						$temas[$cuenta]['fecha']=$ro2['fecha'];
						$temas[$cuenta]['activo']=$ro2['activo'];
						$cuenta++;
					}
				}
				$curso['curso']['temas']=$temas;
			}

		}
		return $curso;

	}

//lista todos los temas por usuario
	public function listaTodosTemasUsuario($idusuario, $idtipousuario, $tipotema){
		$this->stmt=mysqli_query($this->link, "select idtema, imagen, titulo, descripcion, date(fecha_creacion) as fecha from  tema where tipotema=$tipotema and activo=true");
		$contador=mysqli_num_rows($this->stmt);
		$tema = array();
		$cuenta = 0;
		if($contador != 0) {
			while ($ro = mysqli_fetch_array($this->stmt)) {
				$tema[$cuenta] = array();
				$tema[$cuenta]['idtema'] = $ro['idtema'];
				$tema[$cuenta]['imagen'] = $ro['imagen'];
				$tema[$cuenta]['titulo'] = $ro['titulo'];
				$tema[$cuenta]['descripcion'] = $ro['descripcion'];
				$tema[$cuenta]['fecha'] = $ro['fecha'];
				$this->stmt2 = mysqli_query($this->link, "select count(*) as valido from  tema_inscrito where idtema=" . $ro['idtema'] . " and idusuario=$idusuario ");
				$contador2 = mysqli_num_rows($this->stmt2);
				if ($contador2 != 0) {
					if ($ro2 = mysqli_fetch_array($this->stmt2)) {
						if ($ro2['valido'] != 0) {
							$tema[$cuenta]['inscrito'] = false;
						}
						else
						{
							$tema[$cuenta]['inscrito'] = true;
						}
					}

				}
				if($idtipousuario == 3)
				{
					unset($tema[$cuenta]['inscrito']);
				}
				$cuenta++;
			}
			return $tema;
		}
	}
//lista todos los temas
	public function listaTodosTemas(){
		$this->stmt=mysqli_query($this->link, "select idtema, imagen, titulo, descripcion, date(fecha_creacion) as fecha from  tema where tipotema=1 and activo=true");
		$contador=mysqli_num_rows($this->stmt);
		$tema = array();
		$cuenta = 0;
		if($contador != 0){
			while($ro = mysqli_fetch_array($this->stmt)){
				$tema[$cuenta] =array();
				$tema[$cuenta]['idtema']= $ro['idtema'];
				$tema[$cuenta]['imagen']= $ro['imagen'];
				$tema[$cuenta]['titulo']=$ro['titulo'];
				$tema[$cuenta]['descripcion']=$ro['descripcion'];
				$tema[$cuenta]['fecha']=$ro['fecha'];
				$cuenta++;
			}

		}
		return $tema;

	}


//esta se usa para cambiar la clave por medio de el panel de usuario
	public function ponerClaveNueva($correo, $claveantigua, $clavenueva){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from  usuario where correo='$correo' and activa=true and clave=PASSWORD('$claveantigua')");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					mysqli_query($this->link,"Update usuario set clave=PASSWORD('$clavenueva') where correo = '$correo'");
					return true;
				}else{
					return false;
				}
			}

		}

	}
//registrar el usuario por el medio clasico
	public function registrarUsuario($email, $clave, $pass){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from  usuario where correo='$email'");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']==0){
					mysqli_query($this->link,"Insert into usuario (correo, clave, verificacion, fecha_verificacion) values('$email',PASSWORD('$clave'), PASSWORD('$pass'), now())");
					return true;
				}else{
					return false;
				}
			}

		}
	}



//crear tema
	public function crearTema($tema){
		mysqli_query($this->link, "Insert into tema (idusuario, tipotema, titulo, descripcion, imagen, activo, fecha_creacion) values( " . $tema['idusuario'] . ", " . $tema['tipotema'] . ", '" . $tema['titulo'] . "', '" . $tema['descripcion'] . "', '" . $tema['imagen'] . "', false, now())");
		if($tema['idcurso'] != null) {
			$idtema=mysqli_insert_id($this->link);
			$sql = "insert into linea_tema_curso (idcurso, idtema) values ( ".$tema['idcurso']." , $idtema ) ;";
			mysqli_query($this->link, $sql);
			mysqli_query($this->link, "UPDATE curso SET activo = true WHERE idcurso = ".$tema['idcurso']);
		}
		return true;
	}
	//crea curso
	public function crearCurso($curso){
		mysqli_query($this->link,"Insert into curso (idusuario, titulo, descripcion, imagen, activo, fecha_creacion) values( ".$curso['idusuario'].", '".$curso['titulo']."', '".$curso['descripcion']."', '".$curso['imagen']."', false, now())");
		return true;
	}
	//lista todos los cursos
	public function listaTodosCursos($idtipousuario = null, $idusuario){
		$this->stmt=mysqli_query($this->link, "select idcurso, imagen, titulo, descripcion, date(fecha_creacion) as fecha from  curso where  activo=true");
		$contador=mysqli_num_rows($this->stmt);
		$curso = array();
		$cuenta = 0;
		if($contador != 0){
			while($ro = mysqli_fetch_array($this->stmt)){
				$curso[$cuenta] =array();
				$curso[$cuenta]['idcurso']= $ro['idcurso'];
				$curso[$cuenta]['imagen']= $ro['imagen'];
				$curso[$cuenta]['titulo']=$ro['titulo'];
				$curso[$cuenta]['descripcion']=$ro['descripcion'];
				$curso[$cuenta]['fecha']=$ro['fecha'];
				if($idtipousuario == 1 || $idtipousuario == 3 || $idtipousuario == null)
				{
					unset($curso[$cuenta]['inscrito']);
				}
				else
				{
					$this->stmt2 = mysqli_query($this->link, "SELECT COUNT(*) as valido FROM curso_inscrito where idusuario = $idusuario and idcurso=".$ro['idcurso']);
					$contador2=mysqli_num_rows($this->stmt2);
					if($contador2 != 0)
					{
						if($ro2 = mysqli_fetch_array($this->stmt2))
						{
							if($ro2['valido'] != 0)
							{
								$curso[$cuenta]['inscrito'] = false;
							}
							else
							{
								$curso[$cuenta]['inscrito'] = true;
							}
						}
					}
				}
				$cuenta++;
			}

		}
		return $curso;

	}
	//lista los cursos inscritos
	public function listaCursosInscritos($idusuario){
		$this->stmt=mysqli_query($this->link, "select c.idcurso, c.titulo, date(ci.fecha) as fecha from curso_inscrito as ci, curso as c where ci.idcurso=c.idcurso and c.activo =true and ci.idusuario=$idusuario and c.activo=true");
		$contador=mysqli_num_rows($this->stmt);
		$curso = array();
		$cuenta = 0;
		if($contador != 0){
			while($ro = mysqli_fetch_array($this->stmt)){
				$curso[$cuenta]=array();
				$curso[$cuenta]['idcurso']=$ro['idcurso'];
				$curso[$cuenta]['titulo']=$ro['titulo'];
				$curso[$cuenta]['fecha']=$ro['fecha'];
				$cuenta++;
			}

		}
		return $curso;
	}
	//inscribirse curso
	public function inscribirseCurso($idusuario, $idcurso){
		if (mysqli_query($this->link, "insert into curso_inscrito (idusuario, idcurso, fecha) values($idusuario, $idcurso, now())")){
			return true;
		}else{
			return false;
		}
	}
	//lista los cursos Creados
	public function listaCursosCreados($idusuario){
		$this->stmt=mysqli_query($this->link, "select c.idcurso, c.titulo, date(c.fecha_creacion) as fecha from curso as c where  c.idusuario=$idusuario");
		$contador=mysqli_num_rows($this->stmt);
		$curso = array();
		$cuenta = 0;
		if($contador != 0){
			while($ro = mysqli_fetch_array($this->stmt)){
				$curso[$cuenta]=array();
				$curso[$cuenta]['idcurso']=$ro['idcurso'];
				$curso[$cuenta]['titulo']=$ro['titulo'];
				$curso[$cuenta]['fecha']=$ro['fecha'];
				$cuenta++;
			}

		}
		return $curso;
	}
	public function claveOlvidada($correo, $pass){
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from  usuario where correo='$correo'");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					$this->stmt=mysqli_query($this->link, "select count(*) as valido from  usuario where correo='$correo' and activa=true");
					$contador=mysqli_num_rows($this->stmt);
					if($contador != 0){
						if($ro = mysqli_fetch_array($this->stmt)){
							if ($ro['valido']==1){
								mysqli_query($this->link,"Update usuario set clave=PASSWORD('$pass') where correo = '$correo' and activa=true");
								return 1;
							}else{
								return 2;
							}
						}
					}
				}else{
					return 3;
				}
			}

		}

	}
	public function dameNombre($correo){
		$this->stmt=mysqli_query($this->link, "select nombre from  usuario where correo='$correo' and activa=true");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				return $ro['nombre'];
			}else{
				return '';
			}
		}
	}
	public function actualizarUsuario($array){

		$this->stmt=mysqli_query($this->link, "select count(*) as valido from  usuario where idusuario=".$array['idusuario']);
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']!=0){
					if($array['clave']==null || $array['clave']==""){
						mysqli_query($this->link,"Update usuario set nombre='".$array['nombre']."', nick='".$array['nick']."', imagen='".$array['imagen']."', idtipousuario=".$array['tipo']." where idusuario=".$array['idusuario']." ");
						return true;
					}else{
						mysqli_query($this->link,"Update usuario set nombre='".$array['nombre']."', clave=PASSWORD('".$array['clave']."'), nick='".$array['nick']."', imagen='".$array['imagen']."', idtipousuario=".$array['tipo']." where idusuario=".$array['idusuario']." ");
						return true;
					}
				}else{
					return false;
				}
			}

		}
	}
	public function compruebaVerificacionClave($pass){
		$usuario = array();
		$this->stmt=mysqli_query($this->link, "select count(*) as valido from  usuario where verificacion=PASSWORD('$pass') and now() < ADDDATE(fecha_verificacion, INTERVAL 1 DAY)");
		$contador=mysqli_num_rows($this->stmt);
		if($contador != 0){
			if($ro = mysqli_fetch_array($this->stmt)){
				if ($ro['valido']==1){
					return true;
				}else{
					return false;
				}

			}

		}

	}


}
?>


