<?php
require_once 'clases/BD.php';
$bd=BD::getInstancia();
session_start();
if(isset($_POST['tipo']))
{
    $_SESSION['datos_verificacion']['tipo'] = $_POST['tipo'];
    header('Location: http://fct2016daw.no-ip.org/Proyecto/#/verificar_step2');
}

if(isset($_SESSION['datos_verificacion']['tipo']))
{
    if(isset($_POST['finalizar']))
    {
        $_SESSION['datos_verificacion']['nick'] = $_POST['nick'];
        $_SESSION['datos_verificacion']['nombre'] = $_POST['nombre'];
        $_SESSION['datos_verificacion']['correo'] = $_POST['correo'];
        $_SESSION['datos_verificacion']['clave'] = $_POST['clave'];
        $_SESSION['datos_verificacion']['idusuario'] = $_SESSION['datos_user']['idusuario'];
        if(isset($_POST['imagen'])){
            $activo = true;
        }else{
            $activo = false;
        }
        $filtra = explode(".", $_FILES['imagen']['name']);
        $extension = end($filtra);
        if ($extension == "jpg" || $extension == "png" || $extension == "gif" || $extension == "bmp" || $activo ==false) {
            if(is_uploaded_file($_FILES["imagen"]["tmp_name"]))
            {


                $dir_subida = '/var/www/html/Proyecto/img/';
                $fichero_subido = $dir_subida . basename($_FILES['imagen']['name']);
                if (move_uploaded_file($_FILES['imagen']['tmp_name'], $fichero_subido)) {
                    $_SESSION['datos_verificacion']['imagen'] = 'img/'.$_FILES['imagen']['name'];
                } else {
                    $_SESSION['datos_verificacion']['imagen'] = 'img/ico'.$_SESSION['datos_verificacion']['tipo'].'.png';
                }
            }
            else
            {
                $_SESSION['datos_verificacion']['imagen'] = 'img/ico'.$_SESSION['datos_verificacion']['tipo'].'.png';
            }

            if($bd->actualizarUsuario($_SESSION['datos_verificacion']))
            {
                if($bd->activacionUsuario($_SESSION['datos_verificacion']['idusuario'])){

                    unset($_SESSION['datos_verificacion']);
                    $_SESSION['registro']=true;
                    $_SESSION['mensaje']='Se ha verificado Correctamente';
                    header('Location: http://fct2016daw.no-ip.org/Proyecto/#/empezar');
                }
            }
        }else{
            $_SESSION['registro']=true;
            $_SESSION['mensaje']='Error solo puede subir imagenes png, jpg, bmp y gif';
            header('Location: http://fct2016daw.no-ip.org/Proyecto/#/verificar_step1');
        }
    }
}
else
{
    $_SESSION['registro']=true;
    $_SESSION['mensaje']='Ocurrio un problema al verificar su cuenta, por favor comprube sus datos.';
    header('Location: http://fct2016daw.no-ip.org/Proyecto/#/verificar_step1');
}
?>

