<?php
	require("clases/BD.php");
	session_start();
	$bd = BD::getInstancia();
	
	$idtema = $_POST['idtema'];
	$nombre = $_POST['nombre'];
	$arr_preguntas = array();
	
	foreach($_POST['preguntas'] as $key => $value)
	{
		$arr_preguntas[$key]['pregunta'] = $value;
	}
	
	foreach($_POST['opcion1'] as $key => $value)
	{
		$arr_preguntas[$key]['opcion1'] = $value;
	}
	
	foreach($_POST['opcion2'] as $key => $value)
	{
		$arr_preguntas[$key]['opcion2'] = $value;
	}
	
	foreach($_POST['opcion3'] as $key => $value)
	{
		$arr_preguntas[$key]['opcion3'] = $value;
	}
	
	foreach($_POST['correcta'] as $key => $value)
	{
		$arr_preguntas[$key]['correcta'] = $value;
	}
	
	if($bd->crearExamen($idtema,$nombre,$arr_preguntas))
	{
		$_SESSION['registro'] = true;
		$_SESSION['mensaje'] = "Se ha creado correctamente.";
		if($bd->isLibre($idtema))
		{
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres/".$idtema);
		}
		else
		{
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_curso/".$idtema);
		}
		
	}
	else
	{
		$_SESSION['registro'] = true;
		$_SESSION['mensaje'] = "Hubo un error durante la creaccion.";
		if($bd->isLibre($idtema))
		{
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres/".$idtema);
		}
		else
		{
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_curso/".$idtema);
		}
	}
?>