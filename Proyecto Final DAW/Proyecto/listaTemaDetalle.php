<?php
require_once 'clases/BD.php';
$bd=BD::getInstancia();
session_start();
if(isset($_SESSION['user'])){
	if($_REQUEST['idtema']) {
		$tema = array();
		if ($tema = $bd->listaPorTema($_REQUEST['idtema'], $_SESSION['user']['idusuario'])) {

			echo json_encode($tema);
		}else{
			$_SESSION['registro']=true;
			$_SESSION['mensaje']='No tienes permiso para ver ese tema.';
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres");
		}
	}else{
		$_SESSION['registro']=true;
		$_SESSION['mensaje']='A la espera de un parametro';
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres");
	}
}else{
	header("Location: http://fct2016daw.no-ip.org/Proyecto/#/");
}

?>


