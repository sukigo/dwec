
<?php
require("clases/BD.php");

session_start();

$bd = BD::getInstancia();
$_SESSION['registro']=true;
if(isset($_SESSION['user'])){
		if (isset($_REQUEST['idcurso'])){
			if($_SESSION['user']['idtipousuario']==2){
					if ($bd->inscribirseCurso($_SESSION['user']['idusuario'], $_REQUEST['idcurso']))
					{
						$_SESSION['mensaje'] = "Se ha Inscrito correctamente.";
						header("Location: http://fct2016daw.no-ip.org/Proyecto/#/cursos");
					}
					else
					{
						$_SESSION['mensaje'] = "Ya estas inscrito en este curso.";
						header("Location: http://fct2016daw.no-ip.org/Proyecto/#/cursos");
					}
				}else{
					$_SESSION['mensaje'] = "No hay nada que pasarme?.";
					header("Location: http://fct2016daw.no-ip.org/Proyecto/#/cursos");
				}
		}else{

		$_SESSION['mensaje'] = "Solo pueden inscribirse a cursos los alumnos";
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/cursos");

	}
}else{

	$_SESSION['mensaje'] = "Inicia Sesión.";
	header("Location: http://fct2016daw.no-ip.org/Proyecto/#/");


}
?>


