<?php
	require_once 'lib/hybridauth/index.php';
	require ("lib/phpmailer/PHPMailerAutoload.php");
	require("clases/BD.php"); 
	$bd=BD::getInstancia();
	session_start();
	if(isset($_POST) && $_POST != NULL)
	{
		$_SESSION['provider'] = $_POST['provider'];
		if(isset($_POST['email']))
		{
			$_SESSION['email'] = $_POST['email'];
		}
	}
	elseif(!isset($_SESSION['provider']))
	{
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/registrar");
	}
	
	try
	{
	   	$config = dirname(__FILE__) . '/lib/hybridauth/config.php';
      	 // initialize Hybrid_Auth class with the config file
	   	$hybridauth = new Hybrid_Auth( $config );
	   	// try to authenticate with the selected provider
	  	 $adapter = $hybridauth->authenticate( $_SESSION['provider'] );
	   	// then grab the user profile
	   	$user_profile = $adapter->getUserProfile();
		
		if(!isset($_SESSION['email']))
		{
			$_SESSION['email'] = $user_profile->email;
		}
		
	   	$mail = new PHPMailer();
    
	
	    //Configuracion del servidor SMTP
	    $mail->isSMTP();
			$mail->CharSet = "UTF-8";
	    $mail->Host = "";
	    $mail->Username = "";
	    $mail->Password = '';
	    $mail->SMTPAuth = true;
	    $mail->SMTPSSecure = 'tls';
	    $mail->Port = 587;
	
	    // Remitente   
	    $mail->SetFrom('cuentacorreoeducar@gmail.com', 'E-Ducar');

	    // Destinatario
	    $mail->addAddress($_SESSION['email']);
	    
	    // Asunto
	    $mail->Subject ='Verificacion de su cuenta.';
	
	    // Contenido
	    $filename = dirname(__FILE__) . '/recursos/index.html';
	    if($file = file_get_contents($filename))
	    {
			$pass=generarCodigo(10);
			$file = str_replace('##NOMBRE##', limpiar($user_profile->displayName), $file);
			$file = str_replace('##verificar##', 'http://fct2016daw.no-ip.org/Proyecto/verificar.php?verificacion='.$pass, $file);
			$mail->IsHTML(true);
			$mail->Body = $file;
			if ($bd->registrarUsuarioSocial(limpiar($user_profile->displayName), $_SESSION['email'],$_SESSION['provider'],$user_profile->identifier, $pass))
			{
				if($mail->send()==false)
				{
		        	$_SESSION['registro'] = true;
					$_SESSION['mensaje'] = $mail->ErrorInfo;
					unset($_SESSION['email']);
					unset($_SESSION['provider']);
		   			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/registrar");
		    	}
		    	else
				{
					$_SESSION['registro'] = true;
					$_SESSION['mensaje'] = "Se ha registrado correctamente.<br/>
					Se le ha enviado un correo a su correo electronico, por favor verifique su cuenta.";
					unset($_SESSION['email']);
					unset($_SESSION['provider']);
		   			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/registrar");
		    	}							
			}
			else
			{
				$_SESSION['registro'] = true;
				$_SESSION['mensaje'] = "El usuario ya está Registrado";
				unset($_SESSION['email']);
				unset($_SESSION['provider']);
	   			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/registrar");
			}
		} 
   }
   catch( Exception $e )
   {
       switch( $e->getCode() )
       {
	  	  case 0 : echo "Unspecified error."; break;
	  	  case 1 : echo "Hybriauth configuration error."; break;
	  	  case 2 : echo "Provider not properly configured."; break;
	  	  case 3 : echo "Unknown or disabled provider."; break;
	  	  case 4 : echo "Missing provider application credentials."; break;
	  	  case 5 : $_SESSION['registro'] = true;
                   $_SESSION['mensaje'] = "Fallo al registrar. "
	  	              . "El usuario ha cancelado el registro.";
					header("Location: http://fct2016daw.no-ip.org/Proyecto/#/registrar");
	  	           break;
	  	  case 6 : echo "User profile request failed. Most likely the user is not connected "
	  	              . "to the provider and he should authenticate again.";
				  		if(is_object($adapter))
						{
							$adapter->logout();
						}
	  	           
	  	           break;
	  	  case 7 : echo "User not connected to the provider.";
	  	           if(is_object($adapter))
					{
						$adapter->logout();
					}
	  	           break;
	  	  case 8 : echo "Provider does not support this feature."; break;
  		}
 
	  	// well, basically your should not display this to the end user, just give him a hint and move on..
	  	echo "<br /><br /><b>Original error message:</b> " . $e->getMessage();
   }

	function generarCodigo($longitud)
	{
		 $cadena = '';
		 $patron = '1234567890abcdefghijklmnopqrstuvwxyz';
		 $max = strlen($patron)-1;
		 for($i=0;$i < $longitud;$i++) $cadena .= $patron{mt_rand(0,$max)};
		 return $cadena;
	}
	function limpiar($string)
	{
		$string = trim($string);
		$string = str_replace(
			array('Ã¡', 'Ã©', 'Ã*', 'Ã³', 'Ãº', 'Ã±', 'Â¿'),
			array('&aacute;', '&eacute;', '&iacute;', '&oacute;', '&uacute;', '&ntilde;', '&iquest;'),
		$string);

		return $string;
	}

?>


