<?php

/**
 * HybridAuth
 * http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
 * (c) 2009-2015, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
 */
// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

return
		array(
			"base_url" => "https://fct2016daw.no-ip.org/Proyecto/lib/hybridauth/",
			"providers" => array(
				// openid providers
				"OpenID" => array(
					"enabled" => true
				),
				"Yahoo" => array(
					"enabled" => true,
					"keys" => array("key" => "", "secret" => ""),
				),
				"AOL" => array(
					"enabled" => true
				),
				"Google" => array (
			          "enabled" => true,
			          "keys"    => array ( "id" => "1087619370022-4p0or8hgr694d1rlhk3k6724r0cmc9k8.apps.googleusercontent.com", "secret" => "Bv_38SOOUlsiPXnohZc2c6RU" ),
      				  "scope"           => "https://www.googleapis.com/auth/userinfo.profile ". // optional
                              			   "https://www.googleapis.com/auth/userinfo.email"   ,
			    ),
				"Facebook" => array(
					"enabled" => true,
					"keys" => array("id" => "595289387299460", "secret" => "4f6bca59d13244dde22b2f537cbd9d43"),
					"trustForwarded" => false
				),
				"Twitter" => array(
					"enabled" => true,
					"keys" => array("key" => "An4dT9JkHyUPRKDfWQp8YLAwx", "secret" => "sG2U3Q3CzdWZr1NgmwRGbal7g7EyupgLPJMC3kvgc5A4FRfHsJ")
				),
				// windows live
				"Live" => array(
					"enabled" => true,
					"keys" => array("id" => "", "secret" => "")
				),
				"LinkedIn" => array(
					"enabled" => true,
					"keys" => array("key" => "", "secret" => "")
				),
				"Foursquare" => array(
					"enabled" => true,
					"keys" => array("id" => "", "secret" => "")
				),
			),
			// If you want to enable logging, set 'debug_mode' to true.
			// You can also set it to
			// - "error" To log only error messages. Useful in production
			// - "info" To log info and error messages (ignore debug messages)
			"debug_mode" => false,
			// Path to file writable by the web server. Required if 'debug_mode' is not false
			"debug_file" => "",
);
