<?php
require_once 'clases/BD.php';
$bd=BD::getInstancia();
session_start();
if (isset($_SESSION['user'])){
if ($_SESSION['user']['idtipousuario']==3) {
	if (isset($_POST)) {
		$curso = array();
		$curso['titulo'] = $_POST['titulo'];
		$curso['descripcion'] = $_POST['descripcion'];
		$curso['idusuario'] = $_SESSION['user']['idusuario'];
		if(isset($_POST['imagen'])){
           		 $activo = true;
        	}else{
            		$activo = false;
        	}
		$filtra = explode(".", $_FILES['imagen']['name']);
		$extension = end($filtra);
		if ($extension == "jpg" || $extension == "png" || $extension == "gif" || $extension == "bmp" || $activo ==false) {
			if (is_uploaded_file($_FILES["imagen"]["tmp_name"])) {

				$dir_subida = '/var/www/html/Proyecto/img/';
				$fichero_subido = $dir_subida . basename($_FILES['imagen']['name']);
				if (move_uploaded_file($_FILES['imagen']['tmp_name'], $fichero_subido)) {
					$curso['imagen'] = 'img/' . $_FILES['imagen']['name'];
				} else {
					$curso['imagen'] = 'img/icocurso.png';
				}
			} else {
				$curso['imagen'] = 'img/icocurso.png';
			}

			if ($bd->crearCurso($curso)) {
				$_SESSION['registro'] = true;
				$_SESSION['mensaje'] = 'Se ha creado Correctamente';
				header('Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos');
			} else {
				$_SESSION['registro'] = true;
				$_SESSION['mensaje'] = 'ha ocurrido un error al crear el curso.';
				header('Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos');
			}
		} else {
			$_SESSION['registro'] = true;
			$_SESSION['mensaje'] = 'Error, Solo imagenes y en formato png, bmp, jpg, gif.';
			header('Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos');
		}
	}else{
		$_SESSION['registro'] = true;
		$_SESSION['mensaje'] = 'no tienes que pasarme nada?';
		header('Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_cursos');
	}
	}

}else{

	header('Location: http://fct2016daw.no-ip.org/Proyecto/#/');
}
?>

