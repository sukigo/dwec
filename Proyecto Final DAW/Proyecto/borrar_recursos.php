<?php

	require("clases/BD.php"); 
	$bd=BD::getInstancia();
	session_start();
	$_SESSION['registro']=true;
	if ($_SESSION['user']){
		if ($_SESSION['user']['idtipousuario']==1 or $_SESSION['user']['idtipousuario']==3){
				
			if(isset($_POST) && $_POST != NULL)
			{
					if($bd->borrarRecursos($_SESSION['user']['idusuario'], $_REQUEST['idtema'], $_POST['recursos'])){
						$_SESSION['mensaje']='Se realizo correctamente';						
					}else{
						$_SESSION['mensaje']='Error, vuelva a intentarlo.';						
					}						
			}else{	
				$_SESSION['mensaje']='Error, Me pasas algo?';
			}
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres/".$_REQUEST['idtema']);
		}else{
			$_SESSION['mensaje']='Error, no tienes permiso para tocar esto.';
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/user_panel");
		}	

	}else{
		$_SESSION['mensaje']='Error, Inicia sesión.';
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/");
				
	
	}
?>
