<?php
require_once 'clases/BD.php';
$bd=BD::getInstancia();
session_start();
if(isset($_SESSION['user'])){
	if($_REQUEST['idcurso']) {
		$curso = array();
		if ($curso = $bd->listaCurso($_REQUEST['idcurso'],$_SESSION['user']['idtipousuario'], $_SESSION['user']['idusuario'])) {

			echo json_encode($curso);
		}
	}else{
		$_SESSION['registro']=true;
		$_SESSION['mensaje']='A la espera de un parametro';
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres");
	}

}else{
	header("Location: http://fct2016daw.no-ip.org/Proyecto/#/");
}

?>
