<?php
require("clases/BD.php");

session_start();

$bd = BD::getInstancia();
$_SESSION['registro']=true;
if(isset($_SESSION['user'])){
if (isset($_REQUEST)) {
	$filtra = explode(".", $_FILES['imagen']['name']);
	$extension = end($filtra);
	if ($extension == "jpg" || $extension == "png" || $extension == "gif" || $extension == "bmp") {
		if (is_uploaded_file($_FILES['imagen']['tmp_name'])) {
			$dir_subida = '/var/www/html/Proyecto/img/';
			$fichero_subido = $dir_subida . basename($_FILES['imagen']['name']);
			if (move_uploaded_file($_FILES['imagen']['tmp_name'], $fichero_subido)) {
				$imagen = 'img/' . $_FILES['imagen']['name'];
			} else {
				$imagen = 'img/icotema.png';
			}


			$usuario = $bd->modificaImagenTema($_REQUEST['idtema'], $_SESSION['user']['idusuario'], $imagen);
			if ($usuario) {
				$_SESSION['mensaje'] = "Se ha modificado correctamente.";
				header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres/" . $_REQUEST['idtema']);
			} else {
				$_SESSION['mensaje'] = "Ha ocurrido un error con el registro, vuelva a intentarlo.";
				header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres/" . $_REQUEST['idtema']);
			}
		} else {
			$_SESSION['mensaje'] = "Error, el archivo no se ha subido correctamente, vuelva a intentarlo.";
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres/" . $_REQUEST['idtema']);
		}


	} else {
		$_SESSION['mensaje'] = "Solo imagenes en formato png, jpg, bmp y gif.";
		header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres/" . $_REQUEST['idtema']);
	}
}else{
	$_SESSION['mensaje'] = "No hay nada que pasarme?.";
	header("Location: http://fct2016daw.no-ip.org/Proyecto/#/gestionar_temas_libres/".$_REQUEST['idtema']);
}
}else{

	$_SESSION['mensaje'] = "Inicia Sesión.";
	header("Location: http://fct2016daw.no-ip.org/Proyecto/#/");


}
?>






