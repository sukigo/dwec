<?php
require("clases/BD.php");

session_start();
if(isset($_POST) && $_POST != NULL) {

		$bd = BD::getInstancia();
		$usuario= $bd->cambiarDatos($_SESSION['correo'], $_POST['nombre'], $_POST['nick']);
		if ($usuario['mensaje']==1) {
			$_SESSION['registro'] = true;
			$_SESSION['user']['nombre'] = $usuario['nombre'];
			$_SESSION['user']['nick'] = $usuario['nick'];
			$_SESSION['mensaje'] = "Se ha modificado correctamente.<br> Los cambios seran visibles en el proximo inicio de sesion.";
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/user_panel");
		} else {
			$_SESSION['registro'] = true;
			$_SESSION['mensaje'] = "Ha ocurrido un error, vuelva a intentarlo.";
			header("Location: http://fct2016daw.no-ip.org/Proyecto/#/user_panel");
		}


}
	?>
