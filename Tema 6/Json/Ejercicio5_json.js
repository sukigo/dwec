function init()
{
	num_email = 0;
	loadmsj();
	setInterval(loadmsj,5000);
}

function loadmsj()
{
	var xhttp;
	var c = 0;
	if (window.XMLHttpRequest)
	{
	    xhttp = new XMLHttpRequest();
	}
	else
	{
	    // code for IE6, IE5
	    xhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xhttp.onreadystatechange = function()
	{
	    if (xhttp.readyState == 4 && xhttp.status == 200)
	    {
	    	var json = JSON.parse(xhttp.responseText);
			if(num_email != json.correo.length)
			{
				c = json.correo.length - num_email;
				num_email = json.correo.length;
				var ultimos = json.correo.length - 1;
		    	for (var i=c-1; i >= 0; i--)
		    	{
				  	var email = json.correo[ultimos];
				  	var div = document.createElement("div");
				  	var texto = document.createTextNode(email.direccion+"--"+email.nombre+"--"+email.mensaje)
				  	div.appendChild(texto);
				  	document.getElementById("correo").appendChild(div);
				  	ultimos--;
				};
			}
	    }
  	}
  	
  	xhttp.open("POST", "email.json", true);
  	xhttp.send();
}

window.onload = init;
