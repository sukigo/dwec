function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function comprobar()
{
 	var xhttp;
	if (window.XMLHttpRequest)
	{
	    xhttp = new XMLHttpRequest();
	}
	else
	{
	    // code for IE6, IE5
	    xhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xhttp.onreadystatechange = function()
	{
		if(xhttp.readyState > 0 && xhttp.readyState < 4)
		{
			document.getElementById("resultado").innerHTML = "Comprobando...";
		}
		
	    if(xhttp.readyState == 4 && xhttp.status == 200)
	    {
	    	if(xhttp.responseText.search("No disponible") != -1)
	    	{
	    		if(!getCookie("user"))
	    		{
	    		setCookie("user",document.getElementById("user").value,"0.1");	    			
	    		}
	    	}
	    	document.getElementById("resultado").innerHTML = xhttp.responseText;
	    }
  	}
  	
  	var user = "";
  	
  	if(document.getElementById("user").value != "")
  	{
  		user = document.getElementById("user").value;
  	}
  	
  	if(getCookie("user"))
  	{
  		if(getCookie("user")+"_1" == user)
  		{
  			document.cookie = 'user=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  			xhttp.open("POST", "Ejercicio4.php?user_old=disponible", true);
  		}
  		if(getCookie("user")+"_2" == user)
  		{
  			document.cookie = 'user=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  			xhttp.open("POST", "Ejercicio4.php?user_old=disponible", true);
  		}
  		if(getCookie("user") == user)
  		{
  			xhttp.open("POST", "Ejercicio4.php?user_old="+user, true);
  		}
  		else
  		{
  			xhttp.open("POST", "Ejercicio4.php?user="+user, true);
  		}
  	}
  	else
  	{
  		xhttp.open("POST", "Ejercicio4.php?user="+user, true);
  	}
  	xhttp.send();
}
