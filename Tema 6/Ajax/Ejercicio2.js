function init()
{
	var url = document.URL;
	document.getElementById("url").value = url;
}

function mostrar()
{	
	var xhttp;
	if (window.XMLHttpRequest)
	{
	    xhttp = new XMLHttpRequest();
	}
	else
	{
	    // code for IE6, IE5
	    xhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xhttp.onreadystatechange = function()
	{
	    if (xhttp.readyState == 4 && xhttp.status == 200)
	    {
			var div = document.createElement("div");
			var titulo1 = document.createElement("h2");
			var text = document.createTextNode("Contenidos del Archivo");
			titulo1.appendChild(text);
			text = document.createTextNode(xhttp.responseText);
			div.appendChild(text);
			document.getElementsByTagName("body")[0].appendChild(div);
	    }
  	}

	var url = document.getElementById("url").value;
  	xhttp.open("GET", url, true);
  	xhttp.send();
}

window.onload = init;