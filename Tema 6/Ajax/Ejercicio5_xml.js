function init()
{
	num_email = 0;
	loadmsj();
	setInterval(loadmsj,5000);
}

function loadmsj()
{
	var xhttp;
	var c = 0;
	if (window.XMLHttpRequest)
	{
	    xhttp = new XMLHttpRequest();
	}
	else
	{
	    // code for IE6, IE5
	    xhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xhttp.onreadystatechange = function()
	{
	    if (xhttp.readyState == 4 && xhttp.status == 200)
	    {
	    	var xml = xhttp.responseXML;
			var emails = xml.getElementsByTagName("email");
			if(num_email != emails.length)
			{
				c = emails.length - num_email;
				num_email = emails.length;
				var ultimos = emails.length - 1;
		    	for (var i=c-1; i >= 0; i--)
		    	{
				  	document.getElementById("correo").appendChild(emails[ultimos]);
				  	ultimos--;
				};
			}
	    }
  	}
  	
  	xhttp.open("POST", "email.xml", true);
  	xhttp.send();
}

window.onload = init;
