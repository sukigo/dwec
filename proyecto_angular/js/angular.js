var app = angular.module('myApp', ['ngRoute','ngAnimate']);
var interval = null;
app.controller('todas', function($scope, $http)
{
	$scope.mostrar =true;
	$scope.url = "#";
	$http.get("http://www.rtve.es/api/noticias.json").then(function (response)
  	{
  		$scope.ruta = "<p><a href='#/'>Inicio</a> > <a href='#/todas'>Ultimas noticias</a></p>";
  		$scope.categoria = "Ultima Noticas";
  		$scope.noticias = response.data.page.items;
  		$scope.mostrar = false;
  	});
	interval = null;
	interval = setInterval(function()
	{
		$scope.mostrar =true;
		$http.get("http://www.rtve.es/api/noticias.json").then(function (response)
	  	{
	  		$scope.ruta = "<p><a href='#/'>Inicio</a> > <a href='#/todas'>Ultimas noticias</a></p>";
	  		$scope.categoria = "Ultima Noticas";
	  		$scope.noticias = response.data.page.items;
	  		$scope.mostrar = false;
	  	});
	},500000);
});

app.controller('principal', function($scope, $http)
{
	$scope.url = "#";
	$scope.mostrar =true;
  	$http.get("http://www.rtve.es/api/noticias.json").then(function (response)
  	{
  		$scope.noticias = new Array();
  		$scope.categoria1 = "Ultimas Noticias";
  		for(var i = 0; i < 6; i++)
  		{
  			$scope.noticias.push(response.data.page.items[i]);
  		}
  	    $scope.mostrar = false;
  	});
  	
  	$http.get("http://api.rtve.es/api/imagenes/mas-vistos.json").then(function (response)
  	{
  		$scope.imagenes = new Array();
  		$scope.categoria2 = "Imagenes";
  	    for(var i = 0; i < 6; i++)
  		{
  			$scope.imagenes.push(response.data.page.items[i]);
  		}
  	    $scope.mostrar = false;
  	});
  	
  	$http.get("http://api.rtve.es/api/videos/mas-vistos.json").then(function (response)
  	{
  		$scope.videos = new Array();
  		$scope.categoria3 = "Videos";
  	    for(var i = 0; i < 6; i++)
  		{
  			$scope.videos.push(response.data.page.items[i]);
  		}
  	    $scope.mostrar = false;
  	});
  	
	interval = null;
	interval = setInterval(function()
	{
		$scope.mostrar =true;
	  	$http.get("http://www.rtve.es/api/noticias.json").then(function (response)
	  	{
	  		$scope.noticias = new Array();
	  		$scope.categoria1 = "Ultimas Noticias";
	  		for(var i = 0; i < 6; i++)
	  		{
	  			$scope.noticias.push(response.data.page.items[i]);
	  		}
	  	    $scope.mostrar = false;
	  	});
	  	
	  	$http.get("http://api.rtve.es/api/imagenes/mas-vistos.json").then(function (response)
	  	{
	  		$scope.imagenes = new Array();
	  		$scope.categoria2 = "Imagenes";
	  	    for(var i = 0; i < 6; i++)
	  		{
	  			$scope.imagenes.push(response.data.page.items[i]);
	  		}
	  	    $scope.mostrar = false;
	  	});
	  	
	  	$http.get("http://api.rtve.es/api/videos/mas-vistos.json").then(function (response)
	  	{
	  		$scope.videos = new Array();
	  		$scope.categoria3 = "Videos";
	  	    for(var i = 0; i < 6; i++)
	  		{
	  			$scope.videos.push(response.data.page.items[i]);
	  		}
	  	    $scope.mostrar = false;
	  	});
	},500000);
  	
});

app.controller('deportes', function($scope, $http)
{
	$scope.mostrar =true;
	$scope.url = "#";
  	$http.get("http://www.rtve.es/api/tematicas/816/noticias.json").then(function (response)
  	{
  		$scope.ruta = "<p><a href='#/'>Inicio</a> > <a href='#/Deportes'>Deportes</a></p>";
  		$scope.categoria = "Deportes";
  	    $scope.noticias = response.data.page.items;
  	    $scope.mostrar = false;
  	});
  	
	interval = null;
	interval = setInterval(function()
	{
		$scope.mostrar =true;
	  	$http.get("http://www.rtve.es/api/tematicas/816/noticias.json").then(function (response)
	  	{
	  		$scope.ruta = "<p><a href='#/'>Inicio</a> > <a href='#/Deportes'>Deportes</a></p>";
	  		$scope.categoria = "Deportes";
	  	    $scope.noticias = response.data.page.items;
	  	    $scope.mostrar = false;
	  	});
  	},500000);
});

app.controller('imagenes', function($scope, $http)
{
	$scope.mostrar =true;
	$scope.url = "#/Imagen";
  	$http.get("http://api.rtve.es/api/imagenes/mas-vistos.json").then(function (response)
  	{
  		$scope.ruta = "<p><a href='#/'>Inicio</a> > <a href='#/Images'>Imagenes</a></p>";
  		$scope.categoria = "Imagenes";
  	    $scope.noticias = response.data.page.items;
  	    $scope.mostrar = false;
  	});
  	
	interval = null;
	interval = setInterval(function()
	{
		$scope.mostrar =true;
	  	$http.get("http://api.rtve.es/api/imagenes/mas-vistos.json").then(function (response)
	  	{
	  		$scope.ruta = "<p><a href='#/'>Inicio</a> > <a href='#/Imagenes'>Imagenes</a></p>";
	  		$scope.categoria = "Imagenes";
	  	    $scope.noticias = response.data.page.items;
	  	    $scope.mostrar = false;
	  	});
  	},500000);
});

app.controller('videos', function($scope, $http)
{
	$scope.mostrar =true;
	$scope.url= "#/Video";
  	$http.get("http://www.rtve.es/api/videos/mas-vistos.json").then(function (response)
  	{
  		$scope.ruta = "<p><a href='#/'>Inicio</a> > <a href='#/Videos'>Videos</a></p>";
  		$scope.categoria = "Videos";
  	    $scope.noticias = response.data.page.items;
  	    $scope.mostrar = false;
  	});
  	
	interval = null;
	interval = setInterval(function()
	{
		$scope.mostrar =true;
	  	$http.get("http://www.rtve.es/api/videos/mas-vistos.json").then(function (response)
	  	{
	  		$scope.ruta = "<p><a href='#/'>Inicio</a> > <a href='#/Videos'>Videos</a></p>";
	  		$scope.categoria = "Videos";
	  	    $scope.noticias = response.data.page.items;
	  	    $scope.mostrar = false;
	  	});
  	},500000);
});

app.controller('tiempo', function($scope, $http)
{
	$scope.mostrar =true;
	$scope.url = "#";
 	$http.get("http://www.rtve.es/api/tematicas/821/noticias.json").then(function (response)
  	{
  		$scope.ruta = "<p><a href='#/'>Inicio</a> > <a href='#/Tiempo'>El tiempo</a></p>";
  		$scope.categoria = "El Tiempo";
  		$scope.noticias = response.data.page.items;
  		$scope.mostrar = false;
  	});
	interval = null;
	interval = setInterval(function()
	{
		$scope.mostrar =true;
	 	$http.get("http://www.rtve.es/api/tematicas/821/noticias.json").then(function (response)
	  	{
	  		$scope.ruta = "<p><a href='#/'>Inicio</a> > <a href='#/Tiempo'>El tiempo</a></p>";
	  		$scope.categoria = "El Tiempo";
	  		$scope.noticias = response.data.page.items;
	  		$scope.mostrar = false;
	  	});
  	},500000);
});

app.controller('cultura', function($scope, $http)
{
	$scope.mostrar =true;
	$scope.url = "#";
  	$http.get("http://www.rtve.es/api/tematicas/827/noticias.json").then(function (response)
  	{
  		$scope.ruta = "<p><a href='#/'>Inicio</a> > <a href='#/Cultura'>Cultura</a></p>";
  	  	$scope.categoria = "Cultura";
      	$scope.noticias = response.data.page.items;
      	$scope.mostrar = false;
	});
	
	interval = null;
	interval = setInterval(function()
	{
		$scope.mostrar =true;
	  	$http.get("http://www.rtve.es/api/tematicas/827/noticias.json").then(function (response)
	  	{
	  		$scope.ruta = "<p><a href='#/'>Inicio</a> > <a href='#/Cultura'>Cultura</a></p>";
	  	  	$scope.categoria = "Cultura";
	      	$scope.noticias = response.data.page.items;
	      	$scope.mostrar = false;
  		});
  	},500000);
});

app.controller('detalle', ['$scope','$http','$routeParams',function($scope, $http, $routeParams)
{
  	$http.get("http://www.rtve.es/api/noticias/"+$routeParams.idNoticia+".json").then(function (response)
  	{
  	   $scope.ruta = $("#ruta").html()+" > "+response.data.page.items[0].title;
       $scope.noticia = response.data.page.items[0];
  	});
}]);

app.controller('detalleVideo', ['$scope','$http','$routeParams',function($scope, $http, $routeParams)
{
  	$http.get("http://www.rtve.es/api/videos/"+$routeParams.idNoticia+".json").then(function (response)
  	{
  		$scope.embedUrl = "http://www.rtve.es/drmn/embed/video/"+response.data.page.items[0].id;
  		$scope.ruta = "<p><a href='#/'>Inicio</a> > <a href='#/Videos'>Videos</a> > "+response.data.page.items[0].longTitle+" </p>"
       	$scope.noticia = response.data.page.items[0];
  	});
}]);

app.controller('detalleImagen', ['$scope','$http','$routeParams',function($scope, $http, $routeParams)
{
  	$http.get("http://www.rtve.es/api/imagenes/"+$routeParams.idNoticia+".json").then(function (response)
  	{
  	   $scope.ruta = "<p><a href='#/'>Inicio</a> > <a href='#/Imagenes'>Imagenes</a> > "+response.data.page.items[0].longTitle+" </p>"
       $scope.noticia = response.data.page.items[0];
  	});
}]);

app.directive('loading',function()
{
	return{
	    template: '<center><img src="img/loading.gif"/></center>'
	};
});

app.filter('trustAsResourceUrl', ['$sce', function($sce) {
    return function(val) {
        return $sce.trustAsResourceUrl(val);
    };
}]);

app.filter('html', ['$sce', function($sce)
{
        return function(val)
        {
            return $sce.trustAsHtml(val);
        };
}]);

app.filter('img', ['$sce', function($sce)
{
        return function(val)
        {
        	if(val == null)
        	{
        		return "img/rtve.png";
        	}
            else
            {
            	return val;
            }
        };
}]);

app.config(function($routeProvider) {

    $routeProvider
    	.when('/', {
            templateUrl : 'vistas/principal.html',
            controller  : 'principal'
        })
        .when('/Imagenes', {
            templateUrl : 'vistas/noticias.html',
            controller  : 'imagenes'
        })
        .when('/Videos', {
            templateUrl : 'vistas/noticias.html',
            controller  : 'videos'
        })
        .when('/todas', {
            templateUrl : 'vistas/noticias.html',
            controller  : 'todas'
        })
        .when('/Tiempo', {
            templateUrl : 'vistas/noticias.html',
            controller  : 'tiempo'
        })
        .when('/Deportes', {
            templateUrl : 'vistas/noticias.html',
            controller  : 'deportes'
        })
        .when('/Cultura', {
            templateUrl : 'vistas/noticias.html',
            controller  : 'cultura'
        })
        .when('/:idNoticia', {
            templateUrl : 'vistas/noticiasDetalle.html',
            controller  : 'detalle'
        })
        .when('/Video/:idNoticia', {
            templateUrl : 'vistas/videoDetalle.html',
            controller  : 'detalleVideo'
        })
        .when('/Imagen/:idNoticia', {
            templateUrl : 'vistas/imagenDetalle.html',
            controller  : 'detalleImagen'
        })
        .otherwise({
            redirectTo: '/'
        });
});